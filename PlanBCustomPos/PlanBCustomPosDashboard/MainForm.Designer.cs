﻿namespace trgposDashboard
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem4 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem5 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem6 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem7 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem8 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup2 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup3 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup4 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup5 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem1 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem2 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem3 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup6 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem4 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem5 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem6 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem7 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem8 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem9 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup7 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem10 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem11 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem12 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem13 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem14 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem15 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem16 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup8 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem17 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem18 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup9 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem19 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup10 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem20 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem21 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem chartSeriesTypeGalleryItem22 = new DevExpress.XtraDashboard.Bars.ChartSeriesTypeGalleryItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dashboardDesigner1 = new DevExpress.XtraDashboard.DashboardDesigner();
            this.dashboardBarController1 = new DevExpress.XtraDashboard.Bars.DashboardBarController();
            this.fileRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.FileRibbonPageGroup();
            this.dashboardRibbonPage1 = new DevExpress.XtraDashboard.Bars.DashboardRibbonPage();
            this.fileNewBarItem1 = new DevExpress.XtraDashboard.Bars.FileNewBarItem();
            this.fileOpenBarItem1 = new DevExpress.XtraDashboard.Bars.FileOpenBarItem();
            this.fileSaveBarItem1 = new DevExpress.XtraDashboard.Bars.FileSaveBarItem();
            this.fileSaveAsBarItem1 = new DevExpress.XtraDashboard.Bars.FileSaveAsBarItem();
            this.historyRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.HistoryRibbonPageGroup();
            this.undoBarItem1 = new DevExpress.XtraDashboard.Bars.UndoBarItem();
            this.redoBarItem1 = new DevExpress.XtraDashboard.Bars.RedoBarItem();
            this.dataSourceRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.DataSourceRibbonPageGroup();
            this.newDataSourceBarItem1 = new DevExpress.XtraDashboard.Bars.NewDataSourceBarItem();
            this.editDataSourceBarItem1 = new DevExpress.XtraDashboard.Bars.EditDataSourceBarItem();
            this.deleteDataSourceBarItem1 = new DevExpress.XtraDashboard.Bars.DeleteDataSourceBarItem();
            this.insertRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.InsertRibbonPageGroup();
            this.insertPivotBarItem1 = new DevExpress.XtraDashboard.Bars.InsertPivotBarItem();
            this.insertGridBarItem1 = new DevExpress.XtraDashboard.Bars.InsertGridBarItem();
            this.insertChartBarItem1 = new DevExpress.XtraDashboard.Bars.InsertChartBarItem();
            this.insertPiesBarItem1 = new DevExpress.XtraDashboard.Bars.InsertPiesBarItem();
            this.insertGaugesBarItem1 = new DevExpress.XtraDashboard.Bars.InsertGaugesBarItem();
            this.insertCardsBarItem1 = new DevExpress.XtraDashboard.Bars.InsertCardsBarItem();
            this.insertImageBarItem1 = new DevExpress.XtraDashboard.Bars.InsertImageBarItem();
            this.insertTextBoxBarItem1 = new DevExpress.XtraDashboard.Bars.InsertTextBoxBarItem();
            this.insertRangeFilterBarItem1 = new DevExpress.XtraDashboard.Bars.InsertRangeFilterBarItem();
            this.itemsRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.ItemsRibbonPageGroup();
            this.duplicateItemBarItem1 = new DevExpress.XtraDashboard.Bars.DuplicateItemBarItem();
            this.deleteItemBarItem1 = new DevExpress.XtraDashboard.Bars.DeleteItemBarItem();
            this.formatRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.FormatRibbonPageGroup();
            this.setCurrencyCultureBarItem1 = new DevExpress.XtraDashboard.Bars.SetCurrencyCultureBarItem();
            this.skinsRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.SkinsRibbonPageGroup();
            this.dashboardSkinsBarItem1 = new DevExpress.XtraDashboard.Bars.DashboardSkinsBarItem();
            this.filteringRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage1 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.pivotToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.PivotToolsRibbonPageCategory();
            this.editFilterBarItem1 = new DevExpress.XtraDashboard.Bars.EditFilterBarItem();
            this.clearFilterBarItem1 = new DevExpress.XtraDashboard.Bars.ClearFilterBarItem();
            this.filteringRibbonPageGroup2 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage2 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.gridToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.GridToolsRibbonPageCategory();
            this.filteringRibbonPageGroup3 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage3 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.chartToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.ChartToolsRibbonPageCategory();
            this.filteringRibbonPageGroup4 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage4 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.piesToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.PiesToolsRibbonPageCategory();
            this.filteringRibbonPageGroup5 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage5 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.gaugesToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.GaugesToolsRibbonPageCategory();
            this.filteringRibbonPageGroup6 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage6 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.cardsToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.CardsToolsRibbonPageCategory();
            this.filteringRibbonPageGroup7 = new DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup();
            this.dataRibbonPage7 = new DevExpress.XtraDashboard.Bars.DataRibbonPage();
            this.rangeFilterToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.RangeFilterToolsRibbonPageCategory();
            this.masterFilterRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.ignoreMasterFiltersBarItem1 = new DevExpress.XtraDashboard.Bars.IgnoreMasterFiltersBarItem();
            this.masterFilterRibbonPageGroup2 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.masterFilterBarItem1 = new DevExpress.XtraDashboard.Bars.MasterFilterBarItem();
            this.crossDataSourceFilteringBarItem1 = new DevExpress.XtraDashboard.Bars.CrossDataSourceFilteringBarItem();
            this.masterFilterRibbonPageGroup3 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.filterByChartSeriesBarItem1 = new DevExpress.XtraDashboard.Bars.FilterByChartSeriesBarItem();
            this.filterByChartArgumentsBarItem1 = new DevExpress.XtraDashboard.Bars.FilterByChartArgumentsBarItem();
            this.masterFilterRibbonPageGroup4 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.filterByPieSeriesBarItem1 = new DevExpress.XtraDashboard.Bars.FilterByPieSeriesBarItem();
            this.filterByPieArgumentsBarItem1 = new DevExpress.XtraDashboard.Bars.FilterByPieArgumentsBarItem();
            this.masterFilterRibbonPageGroup5 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.masterFilterRibbonPageGroup6 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.masterFilterRibbonPageGroup7 = new DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup();
            this.drillDownRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup();
            this.drillDownBarItem1 = new DevExpress.XtraDashboard.Bars.DrillDownBarItem();
            this.drillDownRibbonPageGroup2 = new DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup();
            this.drillDownOnChartSeriesBarItem1 = new DevExpress.XtraDashboard.Bars.DrillDownOnChartSeriesBarItem();
            this.drillDownOnChartArgumentsBarItem1 = new DevExpress.XtraDashboard.Bars.DrillDownOnChartArgumentsBarItem();
            this.drillDownRibbonPageGroup3 = new DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup();
            this.drillDownOnPieSeriesBarItem1 = new DevExpress.XtraDashboard.Bars.DrillDownOnPieSeriesBarItem();
            this.drillDownOnPieArgumentsBarItem1 = new DevExpress.XtraDashboard.Bars.DrillDownOnPieArgumentsBarItem();
            this.drillDownRibbonPageGroup4 = new DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup();
            this.drillDownRibbonPageGroup5 = new DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup();
            this.contentArrangementRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup();
            this.layoutAndStyleRibbonPage1 = new DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage();
            this.contentAutoArrangeBarItem1 = new DevExpress.XtraDashboard.Bars.ContentAutoArrangeBarItem();
            this.contentArrangeInColumnsBarItem1 = new DevExpress.XtraDashboard.Bars.ContentArrangeInColumnsBarItem();
            this.contentArrangeInRowsBarItem1 = new DevExpress.XtraDashboard.Bars.ContentArrangeInRowsBarItem();
            this.contentArrangementCountBarItem1 = new DevExpress.XtraDashboard.Bars.ContentArrangementCountBarItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.contentArrangementRibbonPageGroup2 = new DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup();
            this.layoutAndStyleRibbonPage2 = new DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage();
            this.contentArrangementRibbonPageGroup3 = new DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup();
            this.layoutAndStyleRibbonPage3 = new DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage();
            this.gridCellsRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.GridCellsRibbonPageGroup();
            this.layoutAndStyleRibbonPage4 = new DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage();
            this.gridHorizontalLinesBarItem1 = new DevExpress.XtraDashboard.Bars.GridHorizontalLinesBarItem();
            this.gridVerticalLinesBarItem1 = new DevExpress.XtraDashboard.Bars.GridVerticalLinesBarItem();
            this.gridMergeCellsBarItem1 = new DevExpress.XtraDashboard.Bars.GridMergeCellsBarItem();
            this.gridBandedRowsBarItem1 = new DevExpress.XtraDashboard.Bars.GridBandedRowsBarItem();
            this.gridColumnHeadersBarItem1 = new DevExpress.XtraDashboard.Bars.GridColumnHeadersBarItem();
            this.chartLayoutPageGroup1 = new DevExpress.XtraDashboard.Bars.ChartLayoutPageGroup();
            this.chartLayoutAndStyleRibbonPage1 = new DevExpress.XtraDashboard.Bars.ChartLayoutAndStyleRibbonPage();
            this.chartRotateBarItem1 = new DevExpress.XtraDashboard.Bars.ChartRotateBarItem();
            this.chartShowLegendBarItem1 = new DevExpress.XtraDashboard.Bars.ChartShowLegendBarItem();
            this.chartYAxisSettingsBarItem1 = new DevExpress.XtraDashboard.Bars.ChartYAxisSettingsBarItem();
            this.chartStylePageGroup1 = new DevExpress.XtraDashboard.Bars.ChartStylePageGroup();
            this.galleryChartSeriesTypeItem1 = new DevExpress.XtraDashboard.Bars.GalleryChartSeriesTypeItem();
            this.pieLabelsRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.PieLabelsRibbonPageGroup();
            this.pieLabelsDataLabelsBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsBarItem();
            this.pieLabelsDataLabelsNoneBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsNoneBarItem();
            this.pieLabelsDataLabelArgumentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelArgumentBarItem();
            this.pieLabelsDataLabelsValueBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsValueBarItem();
            this.pieLabelsDataLabelsArgumentAndValueBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentAndValueBarItem();
            this.pieLabelsDataLabelsPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsPercentBarItem();
            this.pieLabelsDataLabelsValueAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsValueAndPercentBarItem();
            this.pieLabelsDataLabelsArgumentAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentAndPercentBarItem();
            this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentValueAndPercentBarItem();
            this.pieTooltipsBarItem1 = new DevExpress.XtraDashboard.Bars.PieTooltipsBarItem();
            this.pieLabelsTooltipsNoneBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsNoneBarItem();
            this.pieLabelsTooltipsArgumentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentBarItem();
            this.pieLabelsTooltipsValueBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsValueBarItem();
            this.pieLabelsTooltipsArgumentAndValueBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentAndValueBarItem();
            this.pieLabelsTooltipsPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsPercentBarItem();
            this.pieLabelsTooltipsValueAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsValueAndPercentBarItem();
            this.pieLabelsTooltipsArgumentAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentAndPercentBarItem();
            this.pieLabelsTooltipsArgumentValueAndPercentBarItem1 = new DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentValueAndPercentBarItem();
            this.pieStyleRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.PieStyleRibbonPageGroup();
            this.pieStylePieBarItem1 = new DevExpress.XtraDashboard.Bars.PieStylePieBarItem();
            this.pieStyleDonutBarItem1 = new DevExpress.XtraDashboard.Bars.PieStyleDonutBarItem();
            this.gaugeStyleRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.GaugeStyleRibbonPageGroup();
            this.gaugeStyleFullCircularBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleFullCircularBarItem();
            this.gaugeStyleHalfCircularBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleHalfCircularBarItem();
            this.gaugeStyleLeftQuarterCircularBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleLeftQuarterCircularBarItem();
            this.gaugeStyleRightQuarterCircularBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleRightQuarterCircularBarItem();
            this.gaugeStyleThreeForthCircularBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleThreeForthCircularBarItem();
            this.gaugeStyleLinearHorizontalBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleLinearHorizontalBarItem();
            this.gaugeStyleLinearVerticalBarItem1 = new DevExpress.XtraDashboard.Bars.GaugeStyleLinearVerticalBarItem();
            this.imageOpenRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.ImageOpenRibbonPageGroup();
            this.imageOptionsRibbonPage1 = new DevExpress.XtraDashboard.Bars.ImageOptionsRibbonPage();
            this.imageToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.ImageToolsRibbonPageCategory();
            this.imageLoadBarItem1 = new DevExpress.XtraDashboard.Bars.ImageLoadBarItem();
            this.imageImportBarItem1 = new DevExpress.XtraDashboard.Bars.ImageImportBarItem();
            this.imageSizeModeRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.ImageSizeModeRibbonPageGroup();
            this.imageSizeModeClipBarItem1 = new DevExpress.XtraDashboard.Bars.ImageSizeModeClipBarItem();
            this.imageSizeModeStretchBarItem1 = new DevExpress.XtraDashboard.Bars.ImageSizeModeStretchBarItem();
            this.imageSizeModeSqueezeBarItem1 = new DevExpress.XtraDashboard.Bars.ImageSizeModeSqueezeBarItem();
            this.imageSizeModeZoomBarItem1 = new DevExpress.XtraDashboard.Bars.ImageSizeModeZoomBarItem();
            this.imageAlignmentRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentRibbonPageGroup();
            this.imageAlignmentTopLeftBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentTopLeftBarItem();
            this.imageAlignmentCenterLeftBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentCenterLeftBarItem();
            this.imageAlignmentBottomLeftBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentBottomLeftBarItem();
            this.imageAlignmentTopCenterBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentTopCenterBarItem();
            this.imageAlignmentCenterCenterBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentCenterCenterBarItem();
            this.imageAlignmentBottomCenterBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentBottomCenterBarItem();
            this.imageAlignmentTopRightBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentTopRightBarItem();
            this.imageAlignmentCenterRightBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentCenterRightBarItem();
            this.imageAlignmentBottomRightBarItem1 = new DevExpress.XtraDashboard.Bars.ImageAlignmentBottomRightBarItem();
            this.textBoxSettingsRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.TextBoxSettingsRibbonPageGroup();
            this.textBoxFormatRibbonPage1 = new DevExpress.XtraDashboard.Bars.TextBoxFormatRibbonPage();
            this.textBoxToolsRibbonPageCategory1 = new DevExpress.XtraDashboard.Bars.TextBoxToolsRibbonPageCategory();
            this.textBoxEditTextBarItem1 = new DevExpress.XtraDashboard.Bars.TextBoxEditTextBarItem();
            this.rangeFilterSeriesTypeRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.RangeFilterSeriesTypeRibbonPageGroup();
            this.rangeFilterStyleRibbonPage1 = new DevExpress.XtraDashboard.Bars.RangeFilterStyleRibbonPage();
            this.rangeFilterLineSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterLineSeriesTypeBarItem();
            this.rangeFilterStackedLineSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterStackedLineSeriesTypeBarItem();
            this.rangeFilterFullStackedLineSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterFullStackedLineSeriesTypeBarItem();
            this.rangeFilterAreaSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterAreaSeriesTypeBarItem();
            this.rangeFilterStackedAreaSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterStackedAreaSeriesTypeBarItem();
            this.rangeFilterFullStackedAreaSeriesTypeBarItem1 = new DevExpress.XtraDashboard.Bars.RangeFilterFullStackedAreaSeriesTypeBarItem();
            this.quickAccessHistoryRibbonPageGroup1 = new DevExpress.XtraDashboard.Bars.QuickAccessHistoryRibbonPageGroup();
            this.quickAccessUndoBarItem1 = new DevExpress.XtraDashboard.Bars.QuickAccessUndoBarItem();
            this.quickAccessRedoBarItem1 = new DevExpress.XtraDashboard.Bars.QuickAccessRedoBarItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dashboardBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.fileNewBarItem1,
            this.fileOpenBarItem1,
            this.fileSaveBarItem1,
            this.fileSaveAsBarItem1,
            this.undoBarItem1,
            this.redoBarItem1,
            this.newDataSourceBarItem1,
            this.editDataSourceBarItem1,
            this.deleteDataSourceBarItem1,
            this.insertPivotBarItem1,
            this.insertGridBarItem1,
            this.insertChartBarItem1,
            this.insertPiesBarItem1,
            this.insertGaugesBarItem1,
            this.insertCardsBarItem1,
            this.insertImageBarItem1,
            this.insertTextBoxBarItem1,
            this.insertRangeFilterBarItem1,
            this.duplicateItemBarItem1,
            this.deleteItemBarItem1,
            this.setCurrencyCultureBarItem1,
            this.dashboardSkinsBarItem1,
            this.editFilterBarItem1,
            this.clearFilterBarItem1,
            this.ignoreMasterFiltersBarItem1,
            this.masterFilterBarItem1,
            this.crossDataSourceFilteringBarItem1,
            this.filterByChartSeriesBarItem1,
            this.filterByChartArgumentsBarItem1,
            this.filterByPieSeriesBarItem1,
            this.filterByPieArgumentsBarItem1,
            this.drillDownBarItem1,
            this.drillDownOnChartSeriesBarItem1,
            this.drillDownOnChartArgumentsBarItem1,
            this.drillDownOnPieSeriesBarItem1,
            this.drillDownOnPieArgumentsBarItem1,
            this.contentAutoArrangeBarItem1,
            this.contentArrangeInColumnsBarItem1,
            this.contentArrangeInRowsBarItem1,
            this.contentArrangementCountBarItem1,
            this.gridHorizontalLinesBarItem1,
            this.gridVerticalLinesBarItem1,
            this.gridMergeCellsBarItem1,
            this.gridBandedRowsBarItem1,
            this.gridColumnHeadersBarItem1,
            this.chartRotateBarItem1,
            this.chartShowLegendBarItem1,
            this.chartYAxisSettingsBarItem1,
            this.galleryChartSeriesTypeItem1,
            this.pieLabelsDataLabelsBarItem1,
            this.pieLabelsDataLabelsNoneBarItem1,
            this.pieLabelsDataLabelArgumentBarItem1,
            this.pieLabelsDataLabelsValueBarItem1,
            this.pieLabelsDataLabelsArgumentAndValueBarItem1,
            this.pieLabelsDataLabelsPercentBarItem1,
            this.pieLabelsDataLabelsValueAndPercentBarItem1,
            this.pieLabelsDataLabelsArgumentAndPercentBarItem1,
            this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1,
            this.pieTooltipsBarItem1,
            this.pieLabelsTooltipsNoneBarItem1,
            this.pieLabelsTooltipsArgumentBarItem1,
            this.pieLabelsTooltipsValueBarItem1,
            this.pieLabelsTooltipsArgumentAndValueBarItem1,
            this.pieLabelsTooltipsPercentBarItem1,
            this.pieLabelsTooltipsValueAndPercentBarItem1,
            this.pieLabelsTooltipsArgumentAndPercentBarItem1,
            this.pieLabelsTooltipsArgumentValueAndPercentBarItem1,
            this.pieStylePieBarItem1,
            this.pieStyleDonutBarItem1,
            this.gaugeStyleFullCircularBarItem1,
            this.gaugeStyleHalfCircularBarItem1,
            this.gaugeStyleLeftQuarterCircularBarItem1,
            this.gaugeStyleRightQuarterCircularBarItem1,
            this.gaugeStyleThreeForthCircularBarItem1,
            this.gaugeStyleLinearHorizontalBarItem1,
            this.gaugeStyleLinearVerticalBarItem1,
            this.imageLoadBarItem1,
            this.imageImportBarItem1,
            this.imageSizeModeClipBarItem1,
            this.imageSizeModeStretchBarItem1,
            this.imageSizeModeSqueezeBarItem1,
            this.imageSizeModeZoomBarItem1,
            this.imageAlignmentTopLeftBarItem1,
            this.imageAlignmentCenterLeftBarItem1,
            this.imageAlignmentBottomLeftBarItem1,
            this.imageAlignmentTopCenterBarItem1,
            this.imageAlignmentCenterCenterBarItem1,
            this.imageAlignmentBottomCenterBarItem1,
            this.imageAlignmentTopRightBarItem1,
            this.imageAlignmentCenterRightBarItem1,
            this.imageAlignmentBottomRightBarItem1,
            this.textBoxEditTextBarItem1,
            this.rangeFilterLineSeriesTypeBarItem1,
            this.rangeFilterStackedLineSeriesTypeBarItem1,
            this.rangeFilterFullStackedLineSeriesTypeBarItem1,
            this.rangeFilterAreaSeriesTypeBarItem1,
            this.rangeFilterStackedAreaSeriesTypeBarItem1,
            this.rangeFilterFullStackedAreaSeriesTypeBarItem1,
            this.quickAccessUndoBarItem1,
            this.quickAccessRedoBarItem1});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 101;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.pivotToolsRibbonPageCategory1,
            this.gridToolsRibbonPageCategory1,
            this.chartToolsRibbonPageCategory1,
            this.piesToolsRibbonPageCategory1,
            this.gaugesToolsRibbonPageCategory1,
            this.cardsToolsRibbonPageCategory1,
            this.rangeFilterToolsRibbonPageCategory1,
            this.imageToolsRibbonPageCategory1,
            this.textBoxToolsRibbonPageCategory1});
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dashboardRibbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1});
            this.ribbon.Size = new System.Drawing.Size(760, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ItemLinks.Add(this.fileSaveBarItem1);
            this.ribbon.Toolbar.ItemLinks.Add(this.quickAccessUndoBarItem1);
            this.ribbon.Toolbar.ItemLinks.Add(this.quickAccessRedoBarItem1);
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 459);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(760, 27);
            // 
            // dashboardDesigner1
            // 
            this.dashboardDesigner1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dashboardDesigner1.Location = new System.Drawing.Point(0, 146);
            this.dashboardDesigner1.MenuManager = this.ribbon;
            this.dashboardDesigner1.Name = "dashboardDesigner1";
            this.dashboardDesigner1.Size = new System.Drawing.Size(760, 313);
            this.dashboardDesigner1.TabIndex = 2;
            // 
            // dashboardBarController1
            // 
            this.dashboardBarController1.BarItems.Add(this.fileNewBarItem1);
            this.dashboardBarController1.BarItems.Add(this.fileOpenBarItem1);
            this.dashboardBarController1.BarItems.Add(this.fileSaveBarItem1);
            this.dashboardBarController1.BarItems.Add(this.fileSaveAsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.undoBarItem1);
            this.dashboardBarController1.BarItems.Add(this.redoBarItem1);
            this.dashboardBarController1.BarItems.Add(this.newDataSourceBarItem1);
            this.dashboardBarController1.BarItems.Add(this.editDataSourceBarItem1);
            this.dashboardBarController1.BarItems.Add(this.deleteDataSourceBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertPivotBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertGridBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertChartBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertPiesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertGaugesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertCardsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertImageBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertTextBoxBarItem1);
            this.dashboardBarController1.BarItems.Add(this.insertRangeFilterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.duplicateItemBarItem1);
            this.dashboardBarController1.BarItems.Add(this.deleteItemBarItem1);
            this.dashboardBarController1.BarItems.Add(this.setCurrencyCultureBarItem1);
            this.dashboardBarController1.BarItems.Add(this.dashboardSkinsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.editFilterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.clearFilterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.ignoreMasterFiltersBarItem1);
            this.dashboardBarController1.BarItems.Add(this.masterFilterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.crossDataSourceFilteringBarItem1);
            this.dashboardBarController1.BarItems.Add(this.filterByChartSeriesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.filterByChartArgumentsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.filterByPieSeriesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.filterByPieArgumentsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.drillDownBarItem1);
            this.dashboardBarController1.BarItems.Add(this.drillDownOnChartSeriesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.drillDownOnChartArgumentsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.drillDownOnPieSeriesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.drillDownOnPieArgumentsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.contentAutoArrangeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.contentArrangeInColumnsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.contentArrangeInRowsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.contentArrangementCountBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gridHorizontalLinesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gridVerticalLinesBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gridMergeCellsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gridBandedRowsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gridColumnHeadersBarItem1);
            this.dashboardBarController1.BarItems.Add(this.chartRotateBarItem1);
            this.dashboardBarController1.BarItems.Add(this.chartShowLegendBarItem1);
            this.dashboardBarController1.BarItems.Add(this.chartYAxisSettingsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.galleryChartSeriesTypeItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsNoneBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelArgumentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsValueBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsArgumentAndValueBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsValueAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsArgumentAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieTooltipsBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsNoneBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsArgumentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsValueBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsArgumentAndValueBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsValueAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsArgumentAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieLabelsTooltipsArgumentValueAndPercentBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieStylePieBarItem1);
            this.dashboardBarController1.BarItems.Add(this.pieStyleDonutBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleFullCircularBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleHalfCircularBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleLeftQuarterCircularBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleRightQuarterCircularBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleThreeForthCircularBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleLinearHorizontalBarItem1);
            this.dashboardBarController1.BarItems.Add(this.gaugeStyleLinearVerticalBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageLoadBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageImportBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageSizeModeClipBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageSizeModeStretchBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageSizeModeSqueezeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageSizeModeZoomBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentTopLeftBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentCenterLeftBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentBottomLeftBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentTopCenterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentCenterCenterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentBottomCenterBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentTopRightBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentCenterRightBarItem1);
            this.dashboardBarController1.BarItems.Add(this.imageAlignmentBottomRightBarItem1);
            this.dashboardBarController1.BarItems.Add(this.textBoxEditTextBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterLineSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterStackedLineSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterFullStackedLineSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterAreaSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterStackedAreaSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.rangeFilterFullStackedAreaSeriesTypeBarItem1);
            this.dashboardBarController1.BarItems.Add(this.quickAccessUndoBarItem1);
            this.dashboardBarController1.BarItems.Add(this.quickAccessRedoBarItem1);
            this.dashboardBarController1.Control = this.dashboardDesigner1;
            // 
            // fileRibbonPageGroup1
            // 
            this.fileRibbonPageGroup1.ItemLinks.Add(this.fileNewBarItem1);
            this.fileRibbonPageGroup1.ItemLinks.Add(this.fileOpenBarItem1);
            this.fileRibbonPageGroup1.ItemLinks.Add(this.fileSaveBarItem1);
            this.fileRibbonPageGroup1.ItemLinks.Add(this.fileSaveAsBarItem1);
            this.fileRibbonPageGroup1.Name = "fileRibbonPageGroup1";
            // 
            // dashboardRibbonPage1
            // 
            this.dashboardRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.fileRibbonPageGroup1,
            this.historyRibbonPageGroup1,
            this.dataSourceRibbonPageGroup1,
            this.insertRibbonPageGroup1,
            this.itemsRibbonPageGroup1,
            this.formatRibbonPageGroup1,
            this.skinsRibbonPageGroup1,
            this.quickAccessHistoryRibbonPageGroup1});
            this.dashboardRibbonPage1.Name = "dashboardRibbonPage1";
            // 
            // fileNewBarItem1
            // 
            this.fileNewBarItem1.Id = 1;
            this.fileNewBarItem1.Name = "fileNewBarItem1";
            // 
            // fileOpenBarItem1
            // 
            this.fileOpenBarItem1.Id = 2;
            this.fileOpenBarItem1.Name = "fileOpenBarItem1";
            // 
            // fileSaveBarItem1
            // 
            this.fileSaveBarItem1.Id = 3;
            this.fileSaveBarItem1.Name = "fileSaveBarItem1";
            // 
            // fileSaveAsBarItem1
            // 
            this.fileSaveAsBarItem1.Id = 4;
            this.fileSaveAsBarItem1.Name = "fileSaveAsBarItem1";
            // 
            // historyRibbonPageGroup1
            // 
            this.historyRibbonPageGroup1.ItemLinks.Add(this.undoBarItem1);
            this.historyRibbonPageGroup1.ItemLinks.Add(this.redoBarItem1);
            this.historyRibbonPageGroup1.Name = "historyRibbonPageGroup1";
            // 
            // undoBarItem1
            // 
            this.undoBarItem1.Id = 5;
            this.undoBarItem1.Name = "undoBarItem1";
            // 
            // redoBarItem1
            // 
            this.redoBarItem1.Id = 6;
            this.redoBarItem1.Name = "redoBarItem1";
            // 
            // dataSourceRibbonPageGroup1
            // 
            this.dataSourceRibbonPageGroup1.ItemLinks.Add(this.newDataSourceBarItem1);
            this.dataSourceRibbonPageGroup1.ItemLinks.Add(this.editDataSourceBarItem1);
            this.dataSourceRibbonPageGroup1.ItemLinks.Add(this.deleteDataSourceBarItem1);
            this.dataSourceRibbonPageGroup1.Name = "dataSourceRibbonPageGroup1";
            // 
            // newDataSourceBarItem1
            // 
            this.newDataSourceBarItem1.Id = 7;
            this.newDataSourceBarItem1.Name = "newDataSourceBarItem1";
            // 
            // editDataSourceBarItem1
            // 
            this.editDataSourceBarItem1.Id = 8;
            this.editDataSourceBarItem1.Name = "editDataSourceBarItem1";
            // 
            // deleteDataSourceBarItem1
            // 
            this.deleteDataSourceBarItem1.Id = 9;
            this.deleteDataSourceBarItem1.Name = "deleteDataSourceBarItem1";
            // 
            // insertRibbonPageGroup1
            // 
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertPivotBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertGridBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertChartBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertPiesBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertGaugesBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertCardsBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertImageBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertTextBoxBarItem1);
            this.insertRibbonPageGroup1.ItemLinks.Add(this.insertRangeFilterBarItem1);
            this.insertRibbonPageGroup1.Name = "insertRibbonPageGroup1";
            // 
            // insertPivotBarItem1
            // 
            this.insertPivotBarItem1.Id = 10;
            this.insertPivotBarItem1.Name = "insertPivotBarItem1";
            // 
            // insertGridBarItem1
            // 
            this.insertGridBarItem1.Id = 11;
            this.insertGridBarItem1.Name = "insertGridBarItem1";
            // 
            // insertChartBarItem1
            // 
            this.insertChartBarItem1.Id = 12;
            this.insertChartBarItem1.Name = "insertChartBarItem1";
            // 
            // insertPiesBarItem1
            // 
            this.insertPiesBarItem1.Id = 13;
            this.insertPiesBarItem1.Name = "insertPiesBarItem1";
            // 
            // insertGaugesBarItem1
            // 
            this.insertGaugesBarItem1.Id = 14;
            this.insertGaugesBarItem1.Name = "insertGaugesBarItem1";
            // 
            // insertCardsBarItem1
            // 
            this.insertCardsBarItem1.Id = 15;
            this.insertCardsBarItem1.Name = "insertCardsBarItem1";
            // 
            // insertImageBarItem1
            // 
            this.insertImageBarItem1.Id = 16;
            this.insertImageBarItem1.Name = "insertImageBarItem1";
            // 
            // insertTextBoxBarItem1
            // 
            this.insertTextBoxBarItem1.Id = 17;
            this.insertTextBoxBarItem1.Name = "insertTextBoxBarItem1";
            // 
            // insertRangeFilterBarItem1
            // 
            this.insertRangeFilterBarItem1.Id = 18;
            this.insertRangeFilterBarItem1.Name = "insertRangeFilterBarItem1";
            // 
            // itemsRibbonPageGroup1
            // 
            this.itemsRibbonPageGroup1.ItemLinks.Add(this.duplicateItemBarItem1);
            this.itemsRibbonPageGroup1.ItemLinks.Add(this.deleteItemBarItem1);
            this.itemsRibbonPageGroup1.Name = "itemsRibbonPageGroup1";
            // 
            // duplicateItemBarItem1
            // 
            this.duplicateItemBarItem1.Id = 19;
            this.duplicateItemBarItem1.Name = "duplicateItemBarItem1";
            // 
            // deleteItemBarItem1
            // 
            this.deleteItemBarItem1.Id = 20;
            this.deleteItemBarItem1.Name = "deleteItemBarItem1";
            // 
            // formatRibbonPageGroup1
            // 
            this.formatRibbonPageGroup1.ItemLinks.Add(this.setCurrencyCultureBarItem1);
            this.formatRibbonPageGroup1.Name = "formatRibbonPageGroup1";
            // 
            // setCurrencyCultureBarItem1
            // 
            this.setCurrencyCultureBarItem1.Id = 21;
            this.setCurrencyCultureBarItem1.Name = "setCurrencyCultureBarItem1";
            // 
            // skinsRibbonPageGroup1
            // 
            this.skinsRibbonPageGroup1.ItemLinks.Add(this.dashboardSkinsBarItem1);
            this.skinsRibbonPageGroup1.Name = "skinsRibbonPageGroup1";
            // 
            // dashboardSkinsBarItem1
            // 
            // 
            // 
            // 
            this.dashboardSkinsBarItem1.Gallery.AllowHoverImages = true;
            this.dashboardSkinsBarItem1.Gallery.ColumnCount = 4;
            this.dashboardSkinsBarItem1.Gallery.FixedHoverImageSize = false;
            galleryItemGroup1.Caption = "Standard Skins";
            galleryItem1.Caption = "DevExpress Style";
            galleryItem1.Hint = "DevExpress Style";
            galleryItem1.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem1.HoverImage")));
            galleryItem1.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem1.Image")));
            galleryItem1.Tag = "DevExpress Style";
            galleryItem2.Caption = "DevExpress Dark Style";
            galleryItem2.Hint = "DevExpress Dark Style";
            galleryItem2.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem2.HoverImage")));
            galleryItem2.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem2.Image")));
            galleryItem2.Tag = "DevExpress Dark Style";
            galleryItem3.Caption = "Seven Classic";
            galleryItem3.Hint = "Seven Classic";
            galleryItem3.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem3.HoverImage")));
            galleryItem3.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem3.Image")));
            galleryItem3.Tag = "Seven Classic";
            galleryItem4.Caption = "Office 2010 Black";
            galleryItem4.Hint = "Office 2010 Black";
            galleryItem4.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem4.HoverImage")));
            galleryItem4.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem4.Image")));
            galleryItem4.Tag = "Office 2010 Black";
            galleryItem5.Caption = "Office 2010 Blue";
            galleryItem5.Hint = "Office 2010 Blue";
            galleryItem5.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem5.HoverImage")));
            galleryItem5.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem5.Image")));
            galleryItem5.Tag = "Office 2010 Blue";
            galleryItem6.Caption = "Office 2010 Silver";
            galleryItem6.Hint = "Office 2010 Silver";
            galleryItem6.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem6.HoverImage")));
            galleryItem6.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem6.Image")));
            galleryItem6.Tag = "Office 2010 Silver";
            galleryItem7.Caption = "Office 2013";
            galleryItem7.Checked = true;
            galleryItem7.Hint = "Office 2013";
            galleryItem7.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem7.HoverImage")));
            galleryItem7.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem7.Image")));
            galleryItem7.Tag = "Office 2013";
            galleryItem8.Caption = "VS2010";
            galleryItem8.Hint = "VS2010";
            galleryItem8.HoverImage = ((System.Drawing.Image)(resources.GetObject("galleryItem8.HoverImage")));
            galleryItem8.Image = ((System.Drawing.Image)(resources.GetObject("galleryItem8.Image")));
            galleryItem8.Tag = "VS2010";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3,
            galleryItem4,
            galleryItem5,
            galleryItem6,
            galleryItem7,
            galleryItem8});
            galleryItemGroup2.Caption = "Bonus Skins";
            galleryItemGroup2.Visible = false;
            galleryItemGroup3.Caption = "Theme Skins";
            galleryItemGroup3.Visible = false;
            galleryItemGroup4.Caption = "Custom Skins";
            galleryItemGroup4.Visible = false;
            this.dashboardSkinsBarItem1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1,
            galleryItemGroup2,
            galleryItemGroup3,
            galleryItemGroup4});
            this.dashboardSkinsBarItem1.Gallery.ImageSize = new System.Drawing.Size(32, 16);
            this.dashboardSkinsBarItem1.Gallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            this.dashboardSkinsBarItem1.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.dashboardSkinsBarItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("dashboardSkinsBarItem1.Glyph")));
            this.dashboardSkinsBarItem1.Id = 22;
            this.dashboardSkinsBarItem1.Name = "dashboardSkinsBarItem1";
            // 
            // filteringRibbonPageGroup1
            // 
            this.filteringRibbonPageGroup1.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup1.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup1.Name = "filteringRibbonPageGroup1";
            // 
            // dataRibbonPage1
            // 
            this.dataRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup1,
            this.masterFilterRibbonPageGroup1});
            this.dataRibbonPage1.Name = "dataRibbonPage1";
            this.dataRibbonPage1.Visible = false;
            // 
            // pivotToolsRibbonPageCategory1
            // 
            this.pivotToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.pivotToolsRibbonPageCategory1.Name = "pivotToolsRibbonPageCategory1";
            this.pivotToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage1});
            this.pivotToolsRibbonPageCategory1.Visible = false;
            // 
            // editFilterBarItem1
            // 
            this.editFilterBarItem1.Id = 23;
            this.editFilterBarItem1.Name = "editFilterBarItem1";
            // 
            // clearFilterBarItem1
            // 
            this.clearFilterBarItem1.Id = 24;
            this.clearFilterBarItem1.Name = "clearFilterBarItem1";
            // 
            // filteringRibbonPageGroup2
            // 
            this.filteringRibbonPageGroup2.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup2.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup2.Name = "filteringRibbonPageGroup2";
            // 
            // dataRibbonPage2
            // 
            this.dataRibbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup2,
            this.masterFilterRibbonPageGroup2,
            this.drillDownRibbonPageGroup1});
            this.dataRibbonPage2.Name = "dataRibbonPage2";
            this.dataRibbonPage2.Visible = false;
            // 
            // gridToolsRibbonPageCategory1
            // 
            this.gridToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.gridToolsRibbonPageCategory1.Name = "gridToolsRibbonPageCategory1";
            this.gridToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage2,
            this.layoutAndStyleRibbonPage4});
            this.gridToolsRibbonPageCategory1.Visible = false;
            // 
            // filteringRibbonPageGroup3
            // 
            this.filteringRibbonPageGroup3.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup3.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup3.Name = "filteringRibbonPageGroup3";
            // 
            // dataRibbonPage3
            // 
            this.dataRibbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup3,
            this.masterFilterRibbonPageGroup3,
            this.drillDownRibbonPageGroup2});
            this.dataRibbonPage3.Name = "dataRibbonPage3";
            this.dataRibbonPage3.Visible = false;
            // 
            // chartToolsRibbonPageCategory1
            // 
            this.chartToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.chartToolsRibbonPageCategory1.Name = "chartToolsRibbonPageCategory1";
            this.chartToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage3,
            this.chartLayoutAndStyleRibbonPage1});
            this.chartToolsRibbonPageCategory1.Visible = false;
            // 
            // filteringRibbonPageGroup4
            // 
            this.filteringRibbonPageGroup4.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup4.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup4.Name = "filteringRibbonPageGroup4";
            // 
            // dataRibbonPage4
            // 
            this.dataRibbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup4,
            this.masterFilterRibbonPageGroup4,
            this.drillDownRibbonPageGroup3});
            this.dataRibbonPage4.Name = "dataRibbonPage4";
            this.dataRibbonPage4.Visible = false;
            // 
            // piesToolsRibbonPageCategory1
            // 
            this.piesToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.piesToolsRibbonPageCategory1.Name = "piesToolsRibbonPageCategory1";
            this.piesToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage4,
            this.layoutAndStyleRibbonPage1});
            this.piesToolsRibbonPageCategory1.Visible = false;
            // 
            // filteringRibbonPageGroup5
            // 
            this.filteringRibbonPageGroup5.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup5.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup5.Name = "filteringRibbonPageGroup5";
            // 
            // dataRibbonPage5
            // 
            this.dataRibbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup5,
            this.masterFilterRibbonPageGroup5,
            this.drillDownRibbonPageGroup4});
            this.dataRibbonPage5.Name = "dataRibbonPage5";
            this.dataRibbonPage5.Visible = false;
            // 
            // gaugesToolsRibbonPageCategory1
            // 
            this.gaugesToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.gaugesToolsRibbonPageCategory1.Name = "gaugesToolsRibbonPageCategory1";
            this.gaugesToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage5,
            this.layoutAndStyleRibbonPage2});
            this.gaugesToolsRibbonPageCategory1.Visible = false;
            // 
            // filteringRibbonPageGroup6
            // 
            this.filteringRibbonPageGroup6.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup6.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup6.Name = "filteringRibbonPageGroup6";
            // 
            // dataRibbonPage6
            // 
            this.dataRibbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup6,
            this.masterFilterRibbonPageGroup6,
            this.drillDownRibbonPageGroup5});
            this.dataRibbonPage6.Name = "dataRibbonPage6";
            this.dataRibbonPage6.Visible = false;
            // 
            // cardsToolsRibbonPageCategory1
            // 
            this.cardsToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.cardsToolsRibbonPageCategory1.Name = "cardsToolsRibbonPageCategory1";
            this.cardsToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage6,
            this.layoutAndStyleRibbonPage3});
            this.cardsToolsRibbonPageCategory1.Visible = false;
            // 
            // filteringRibbonPageGroup7
            // 
            this.filteringRibbonPageGroup7.ItemLinks.Add(this.editFilterBarItem1);
            this.filteringRibbonPageGroup7.ItemLinks.Add(this.clearFilterBarItem1);
            this.filteringRibbonPageGroup7.Name = "filteringRibbonPageGroup7";
            // 
            // dataRibbonPage7
            // 
            this.dataRibbonPage7.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.filteringRibbonPageGroup7,
            this.masterFilterRibbonPageGroup7});
            this.dataRibbonPage7.Name = "dataRibbonPage7";
            this.dataRibbonPage7.Visible = false;
            // 
            // rangeFilterToolsRibbonPageCategory1
            // 
            this.rangeFilterToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.rangeFilterToolsRibbonPageCategory1.Name = "rangeFilterToolsRibbonPageCategory1";
            this.rangeFilterToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.dataRibbonPage7,
            this.rangeFilterStyleRibbonPage1});
            this.rangeFilterToolsRibbonPageCategory1.Visible = false;
            // 
            // masterFilterRibbonPageGroup1
            // 
            this.masterFilterRibbonPageGroup1.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup1.Name = "masterFilterRibbonPageGroup1";
            // 
            // ignoreMasterFiltersBarItem1
            // 
            this.ignoreMasterFiltersBarItem1.Id = 25;
            this.ignoreMasterFiltersBarItem1.Name = "ignoreMasterFiltersBarItem1";
            // 
            // masterFilterRibbonPageGroup2
            // 
            this.masterFilterRibbonPageGroup2.ItemLinks.Add(this.masterFilterBarItem1);
            this.masterFilterRibbonPageGroup2.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup2.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup2.Name = "masterFilterRibbonPageGroup2";
            // 
            // masterFilterBarItem1
            // 
            this.masterFilterBarItem1.Id = 26;
            this.masterFilterBarItem1.Name = "masterFilterBarItem1";
            // 
            // crossDataSourceFilteringBarItem1
            // 
            this.crossDataSourceFilteringBarItem1.Id = 27;
            this.crossDataSourceFilteringBarItem1.Name = "crossDataSourceFilteringBarItem1";
            // 
            // masterFilterRibbonPageGroup3
            // 
            this.masterFilterRibbonPageGroup3.ItemLinks.Add(this.filterByChartSeriesBarItem1);
            this.masterFilterRibbonPageGroup3.ItemLinks.Add(this.filterByChartArgumentsBarItem1);
            this.masterFilterRibbonPageGroup3.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup3.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup3.Name = "masterFilterRibbonPageGroup3";
            // 
            // filterByChartSeriesBarItem1
            // 
            this.filterByChartSeriesBarItem1.Id = 28;
            this.filterByChartSeriesBarItem1.Name = "filterByChartSeriesBarItem1";
            // 
            // filterByChartArgumentsBarItem1
            // 
            this.filterByChartArgumentsBarItem1.Id = 29;
            this.filterByChartArgumentsBarItem1.Name = "filterByChartArgumentsBarItem1";
            // 
            // masterFilterRibbonPageGroup4
            // 
            this.masterFilterRibbonPageGroup4.ItemLinks.Add(this.filterByPieSeriesBarItem1);
            this.masterFilterRibbonPageGroup4.ItemLinks.Add(this.filterByPieArgumentsBarItem1);
            this.masterFilterRibbonPageGroup4.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup4.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup4.Name = "masterFilterRibbonPageGroup4";
            // 
            // filterByPieSeriesBarItem1
            // 
            this.filterByPieSeriesBarItem1.Id = 30;
            this.filterByPieSeriesBarItem1.Name = "filterByPieSeriesBarItem1";
            // 
            // filterByPieArgumentsBarItem1
            // 
            this.filterByPieArgumentsBarItem1.Id = 31;
            this.filterByPieArgumentsBarItem1.Name = "filterByPieArgumentsBarItem1";
            // 
            // masterFilterRibbonPageGroup5
            // 
            this.masterFilterRibbonPageGroup5.ItemLinks.Add(this.masterFilterBarItem1);
            this.masterFilterRibbonPageGroup5.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup5.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup5.Name = "masterFilterRibbonPageGroup5";
            // 
            // masterFilterRibbonPageGroup6
            // 
            this.masterFilterRibbonPageGroup6.ItemLinks.Add(this.masterFilterBarItem1);
            this.masterFilterRibbonPageGroup6.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup6.ItemLinks.Add(this.ignoreMasterFiltersBarItem1);
            this.masterFilterRibbonPageGroup6.Name = "masterFilterRibbonPageGroup6";
            // 
            // masterFilterRibbonPageGroup7
            // 
            this.masterFilterRibbonPageGroup7.ItemLinks.Add(this.crossDataSourceFilteringBarItem1);
            this.masterFilterRibbonPageGroup7.Name = "masterFilterRibbonPageGroup7";
            // 
            // drillDownRibbonPageGroup1
            // 
            this.drillDownRibbonPageGroup1.ItemLinks.Add(this.drillDownBarItem1);
            this.drillDownRibbonPageGroup1.Name = "drillDownRibbonPageGroup1";
            // 
            // drillDownBarItem1
            // 
            this.drillDownBarItem1.Id = 32;
            this.drillDownBarItem1.Name = "drillDownBarItem1";
            // 
            // drillDownRibbonPageGroup2
            // 
            this.drillDownRibbonPageGroup2.ItemLinks.Add(this.drillDownOnChartSeriesBarItem1);
            this.drillDownRibbonPageGroup2.ItemLinks.Add(this.drillDownOnChartArgumentsBarItem1);
            this.drillDownRibbonPageGroup2.Name = "drillDownRibbonPageGroup2";
            // 
            // drillDownOnChartSeriesBarItem1
            // 
            this.drillDownOnChartSeriesBarItem1.Id = 33;
            this.drillDownOnChartSeriesBarItem1.Name = "drillDownOnChartSeriesBarItem1";
            // 
            // drillDownOnChartArgumentsBarItem1
            // 
            this.drillDownOnChartArgumentsBarItem1.Id = 34;
            this.drillDownOnChartArgumentsBarItem1.Name = "drillDownOnChartArgumentsBarItem1";
            // 
            // drillDownRibbonPageGroup3
            // 
            this.drillDownRibbonPageGroup3.ItemLinks.Add(this.drillDownOnPieSeriesBarItem1);
            this.drillDownRibbonPageGroup3.ItemLinks.Add(this.drillDownOnPieArgumentsBarItem1);
            this.drillDownRibbonPageGroup3.Name = "drillDownRibbonPageGroup3";
            // 
            // drillDownOnPieSeriesBarItem1
            // 
            this.drillDownOnPieSeriesBarItem1.Id = 35;
            this.drillDownOnPieSeriesBarItem1.Name = "drillDownOnPieSeriesBarItem1";
            // 
            // drillDownOnPieArgumentsBarItem1
            // 
            this.drillDownOnPieArgumentsBarItem1.Id = 36;
            this.drillDownOnPieArgumentsBarItem1.Name = "drillDownOnPieArgumentsBarItem1";
            // 
            // drillDownRibbonPageGroup4
            // 
            this.drillDownRibbonPageGroup4.ItemLinks.Add(this.drillDownBarItem1);
            this.drillDownRibbonPageGroup4.Name = "drillDownRibbonPageGroup4";
            // 
            // drillDownRibbonPageGroup5
            // 
            this.drillDownRibbonPageGroup5.ItemLinks.Add(this.drillDownBarItem1);
            this.drillDownRibbonPageGroup5.Name = "drillDownRibbonPageGroup5";
            // 
            // contentArrangementRibbonPageGroup1
            // 
            this.contentArrangementRibbonPageGroup1.ItemLinks.Add(this.contentAutoArrangeBarItem1);
            this.contentArrangementRibbonPageGroup1.ItemLinks.Add(this.contentArrangeInColumnsBarItem1);
            this.contentArrangementRibbonPageGroup1.ItemLinks.Add(this.contentArrangeInRowsBarItem1);
            this.contentArrangementRibbonPageGroup1.ItemLinks.Add(this.contentArrangementCountBarItem1);
            this.contentArrangementRibbonPageGroup1.Name = "contentArrangementRibbonPageGroup1";
            // 
            // layoutAndStyleRibbonPage1
            // 
            this.layoutAndStyleRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.contentArrangementRibbonPageGroup1,
            this.pieLabelsRibbonPageGroup1,
            this.pieStyleRibbonPageGroup1});
            this.layoutAndStyleRibbonPage1.Name = "layoutAndStyleRibbonPage1";
            this.layoutAndStyleRibbonPage1.Visible = false;
            // 
            // contentAutoArrangeBarItem1
            // 
            this.contentAutoArrangeBarItem1.Id = 37;
            this.contentAutoArrangeBarItem1.Name = "contentAutoArrangeBarItem1";
            // 
            // contentArrangeInColumnsBarItem1
            // 
            this.contentArrangeInColumnsBarItem1.Id = 38;
            this.contentArrangeInColumnsBarItem1.Name = "contentArrangeInColumnsBarItem1";
            // 
            // contentArrangeInRowsBarItem1
            // 
            this.contentArrangeInRowsBarItem1.Id = 39;
            this.contentArrangeInRowsBarItem1.Name = "contentArrangeInRowsBarItem1";
            // 
            // contentArrangementCountBarItem1
            // 
            this.contentArrangementCountBarItem1.Edit = this.repositoryItemSpinEdit1;
            this.contentArrangementCountBarItem1.Id = 40;
            this.contentArrangementCountBarItem1.Name = "contentArrangementCountBarItem1";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit1.IsFloatValue = false;
            this.repositoryItemSpinEdit1.Mask.EditMask = "N00";
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // contentArrangementRibbonPageGroup2
            // 
            this.contentArrangementRibbonPageGroup2.ItemLinks.Add(this.contentAutoArrangeBarItem1);
            this.contentArrangementRibbonPageGroup2.ItemLinks.Add(this.contentArrangeInColumnsBarItem1);
            this.contentArrangementRibbonPageGroup2.ItemLinks.Add(this.contentArrangeInRowsBarItem1);
            this.contentArrangementRibbonPageGroup2.ItemLinks.Add(this.contentArrangementCountBarItem1);
            this.contentArrangementRibbonPageGroup2.Name = "contentArrangementRibbonPageGroup2";
            // 
            // layoutAndStyleRibbonPage2
            // 
            this.layoutAndStyleRibbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.contentArrangementRibbonPageGroup2,
            this.gaugeStyleRibbonPageGroup1});
            this.layoutAndStyleRibbonPage2.Name = "layoutAndStyleRibbonPage2";
            this.layoutAndStyleRibbonPage2.Visible = false;
            // 
            // contentArrangementRibbonPageGroup3
            // 
            this.contentArrangementRibbonPageGroup3.ItemLinks.Add(this.contentAutoArrangeBarItem1);
            this.contentArrangementRibbonPageGroup3.ItemLinks.Add(this.contentArrangeInColumnsBarItem1);
            this.contentArrangementRibbonPageGroup3.ItemLinks.Add(this.contentArrangeInRowsBarItem1);
            this.contentArrangementRibbonPageGroup3.ItemLinks.Add(this.contentArrangementCountBarItem1);
            this.contentArrangementRibbonPageGroup3.Name = "contentArrangementRibbonPageGroup3";
            // 
            // layoutAndStyleRibbonPage3
            // 
            this.layoutAndStyleRibbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.contentArrangementRibbonPageGroup3});
            this.layoutAndStyleRibbonPage3.Name = "layoutAndStyleRibbonPage3";
            this.layoutAndStyleRibbonPage3.Visible = false;
            // 
            // gridCellsRibbonPageGroup1
            // 
            this.gridCellsRibbonPageGroup1.ItemLinks.Add(this.gridHorizontalLinesBarItem1);
            this.gridCellsRibbonPageGroup1.ItemLinks.Add(this.gridVerticalLinesBarItem1);
            this.gridCellsRibbonPageGroup1.ItemLinks.Add(this.gridMergeCellsBarItem1);
            this.gridCellsRibbonPageGroup1.ItemLinks.Add(this.gridBandedRowsBarItem1);
            this.gridCellsRibbonPageGroup1.ItemLinks.Add(this.gridColumnHeadersBarItem1);
            this.gridCellsRibbonPageGroup1.Name = "gridCellsRibbonPageGroup1";
            // 
            // layoutAndStyleRibbonPage4
            // 
            this.layoutAndStyleRibbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.gridCellsRibbonPageGroup1});
            this.layoutAndStyleRibbonPage4.Name = "layoutAndStyleRibbonPage4";
            this.layoutAndStyleRibbonPage4.Visible = false;
            // 
            // gridHorizontalLinesBarItem1
            // 
            this.gridHorizontalLinesBarItem1.Id = 41;
            this.gridHorizontalLinesBarItem1.Name = "gridHorizontalLinesBarItem1";
            // 
            // gridVerticalLinesBarItem1
            // 
            this.gridVerticalLinesBarItem1.Id = 42;
            this.gridVerticalLinesBarItem1.Name = "gridVerticalLinesBarItem1";
            // 
            // gridMergeCellsBarItem1
            // 
            this.gridMergeCellsBarItem1.Id = 43;
            this.gridMergeCellsBarItem1.Name = "gridMergeCellsBarItem1";
            // 
            // gridBandedRowsBarItem1
            // 
            this.gridBandedRowsBarItem1.Id = 44;
            this.gridBandedRowsBarItem1.Name = "gridBandedRowsBarItem1";
            // 
            // gridColumnHeadersBarItem1
            // 
            this.gridColumnHeadersBarItem1.Id = 45;
            this.gridColumnHeadersBarItem1.Name = "gridColumnHeadersBarItem1";
            // 
            // chartLayoutPageGroup1
            // 
            this.chartLayoutPageGroup1.ItemLinks.Add(this.chartRotateBarItem1);
            this.chartLayoutPageGroup1.ItemLinks.Add(this.chartShowLegendBarItem1);
            this.chartLayoutPageGroup1.ItemLinks.Add(this.chartYAxisSettingsBarItem1);
            this.chartLayoutPageGroup1.Name = "chartLayoutPageGroup1";
            // 
            // chartLayoutAndStyleRibbonPage1
            // 
            this.chartLayoutAndStyleRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.chartLayoutPageGroup1,
            this.chartStylePageGroup1});
            this.chartLayoutAndStyleRibbonPage1.Name = "chartLayoutAndStyleRibbonPage1";
            this.chartLayoutAndStyleRibbonPage1.Visible = false;
            // 
            // chartRotateBarItem1
            // 
            this.chartRotateBarItem1.Id = 46;
            this.chartRotateBarItem1.Name = "chartRotateBarItem1";
            // 
            // chartShowLegendBarItem1
            // 
            this.chartShowLegendBarItem1.Id = 47;
            this.chartShowLegendBarItem1.Name = "chartShowLegendBarItem1";
            // 
            // chartYAxisSettingsBarItem1
            // 
            this.chartYAxisSettingsBarItem1.Id = 48;
            this.chartYAxisSettingsBarItem1.Name = "chartYAxisSettingsBarItem1";
            // 
            // chartStylePageGroup1
            // 
            this.chartStylePageGroup1.ItemLinks.Add(this.galleryChartSeriesTypeItem1);
            this.chartStylePageGroup1.Name = "chartStylePageGroup1";
            // 
            // galleryChartSeriesTypeItem1
            // 
            // 
            // 
            // 
            galleryItemGroup5.Caption = "Bar";
            chartSeriesTypeGalleryItem1.Hint = "Bar";
            chartSeriesTypeGalleryItem1.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem1.Image")));
            chartSeriesTypeGalleryItem1.SeriesTypeCaption = "Bar";
            chartSeriesTypeGalleryItem2.Hint = "Stacked Bar";
            chartSeriesTypeGalleryItem2.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem2.Image")));
            chartSeriesTypeGalleryItem2.SeriesTypeCaption = "Stacked Bar";
            chartSeriesTypeGalleryItem3.Hint = "Full-Stacked Bar";
            chartSeriesTypeGalleryItem3.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem3.Image")));
            chartSeriesTypeGalleryItem3.SeriesTypeCaption = "Full-Stacked Bar";
            galleryItemGroup5.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem1,
            chartSeriesTypeGalleryItem2,
            chartSeriesTypeGalleryItem3});
            galleryItemGroup6.Caption = "Point / Line";
            chartSeriesTypeGalleryItem4.Hint = "Point";
            chartSeriesTypeGalleryItem4.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem4.Image")));
            chartSeriesTypeGalleryItem4.SeriesTypeCaption = "Point";
            chartSeriesTypeGalleryItem5.Hint = "Line";
            chartSeriesTypeGalleryItem5.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem5.Image")));
            chartSeriesTypeGalleryItem5.SeriesTypeCaption = "Line";
            chartSeriesTypeGalleryItem6.Hint = "Stacked Line";
            chartSeriesTypeGalleryItem6.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem6.Image")));
            chartSeriesTypeGalleryItem6.SeriesTypeCaption = "Stacked Line";
            chartSeriesTypeGalleryItem7.Hint = "Full-Stacked Line";
            chartSeriesTypeGalleryItem7.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem7.Image")));
            chartSeriesTypeGalleryItem7.SeriesTypeCaption = "Full-Stacked Line";
            chartSeriesTypeGalleryItem8.Hint = "Step Line";
            chartSeriesTypeGalleryItem8.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem8.Image")));
            chartSeriesTypeGalleryItem8.SeriesTypeCaption = "Step Line";
            chartSeriesTypeGalleryItem9.Hint = "Spline";
            chartSeriesTypeGalleryItem9.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem9.Image")));
            chartSeriesTypeGalleryItem9.SeriesTypeCaption = "Spline";
            galleryItemGroup6.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem4,
            chartSeriesTypeGalleryItem5,
            chartSeriesTypeGalleryItem6,
            chartSeriesTypeGalleryItem7,
            chartSeriesTypeGalleryItem8,
            chartSeriesTypeGalleryItem9});
            galleryItemGroup7.Caption = "Area";
            chartSeriesTypeGalleryItem10.Hint = "Area";
            chartSeriesTypeGalleryItem10.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem10.Image")));
            chartSeriesTypeGalleryItem10.SeriesTypeCaption = "Area";
            chartSeriesTypeGalleryItem11.Hint = "Stacked Area";
            chartSeriesTypeGalleryItem11.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem11.Image")));
            chartSeriesTypeGalleryItem11.SeriesTypeCaption = "Stacked Area";
            chartSeriesTypeGalleryItem12.Hint = "Full-Stacked Area";
            chartSeriesTypeGalleryItem12.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem12.Image")));
            chartSeriesTypeGalleryItem12.SeriesTypeCaption = "Full-Stacked Area";
            chartSeriesTypeGalleryItem13.Hint = "Step Area";
            chartSeriesTypeGalleryItem13.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem13.Image")));
            chartSeriesTypeGalleryItem13.SeriesTypeCaption = "Step Area";
            chartSeriesTypeGalleryItem14.Hint = "Spline Area";
            chartSeriesTypeGalleryItem14.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem14.Image")));
            chartSeriesTypeGalleryItem14.SeriesTypeCaption = "Spline Area";
            chartSeriesTypeGalleryItem15.Hint = "Stacked Spline Area";
            chartSeriesTypeGalleryItem15.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem15.Image")));
            chartSeriesTypeGalleryItem15.SeriesTypeCaption = "Stacked Spline Area";
            chartSeriesTypeGalleryItem16.Hint = "Full-Stacked Spline Area";
            chartSeriesTypeGalleryItem16.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem16.Image")));
            chartSeriesTypeGalleryItem16.SeriesTypeCaption = "Full-Stacked Spline Area";
            galleryItemGroup7.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem10,
            chartSeriesTypeGalleryItem11,
            chartSeriesTypeGalleryItem12,
            chartSeriesTypeGalleryItem13,
            chartSeriesTypeGalleryItem14,
            chartSeriesTypeGalleryItem15,
            chartSeriesTypeGalleryItem16});
            galleryItemGroup8.Caption = "Range";
            chartSeriesTypeGalleryItem17.Hint = "Range Bar Side-by-Side";
            chartSeriesTypeGalleryItem17.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem17.Image")));
            chartSeriesTypeGalleryItem17.SeriesTypeCaption = "Range Bar Side-by-Side";
            chartSeriesTypeGalleryItem18.Hint = "Range Area";
            chartSeriesTypeGalleryItem18.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem18.Image")));
            chartSeriesTypeGalleryItem18.SeriesTypeCaption = "Range Area";
            galleryItemGroup8.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem17,
            chartSeriesTypeGalleryItem18});
            galleryItemGroup9.Caption = "Bubble";
            chartSeriesTypeGalleryItem19.Hint = "Bubble";
            chartSeriesTypeGalleryItem19.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem19.Image")));
            chartSeriesTypeGalleryItem19.SeriesTypeCaption = "Bubble";
            galleryItemGroup9.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem19});
            galleryItemGroup10.Caption = "Financial";
            chartSeriesTypeGalleryItem20.Hint = "High-Low-Close";
            chartSeriesTypeGalleryItem20.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem20.Image")));
            chartSeriesTypeGalleryItem20.SeriesTypeCaption = "High-Low-Close";
            chartSeriesTypeGalleryItem21.Hint = "Open-High-Low-Close (Candle Stick)";
            chartSeriesTypeGalleryItem21.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem21.Image")));
            chartSeriesTypeGalleryItem21.SeriesTypeCaption = "Open-High-Low-Close (Candle Stick)";
            chartSeriesTypeGalleryItem22.Hint = "Open-High-Low-Close (Stock)";
            chartSeriesTypeGalleryItem22.Image = ((System.Drawing.Image)(resources.GetObject("chartSeriesTypeGalleryItem22.Image")));
            chartSeriesTypeGalleryItem22.SeriesTypeCaption = "Open-High-Low-Close (Stock)";
            galleryItemGroup10.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            chartSeriesTypeGalleryItem20,
            chartSeriesTypeGalleryItem21,
            chartSeriesTypeGalleryItem22});
            this.galleryChartSeriesTypeItem1.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup5,
            galleryItemGroup6,
            galleryItemGroup7,
            galleryItemGroup8,
            galleryItemGroup9,
            galleryItemGroup10});
            this.galleryChartSeriesTypeItem1.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.galleryChartSeriesTypeItem1.Gallery.RowCount = 8;
            this.galleryChartSeriesTypeItem1.Id = 49;
            this.galleryChartSeriesTypeItem1.Name = "galleryChartSeriesTypeItem1";
            // 
            // pieLabelsRibbonPageGroup1
            // 
            this.pieLabelsRibbonPageGroup1.ItemLinks.Add(this.pieLabelsDataLabelsBarItem1);
            this.pieLabelsRibbonPageGroup1.ItemLinks.Add(this.pieTooltipsBarItem1);
            this.pieLabelsRibbonPageGroup1.Name = "pieLabelsRibbonPageGroup1";
            // 
            // pieLabelsDataLabelsBarItem1
            // 
            this.pieLabelsDataLabelsBarItem1.Id = 50;
            this.pieLabelsDataLabelsBarItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsNoneBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelArgumentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsValueBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsArgumentAndValueBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsValueAndPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsArgumentAndPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1)});
            this.pieLabelsDataLabelsBarItem1.Name = "pieLabelsDataLabelsBarItem1";
            this.pieLabelsDataLabelsBarItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // pieLabelsDataLabelsNoneBarItem1
            // 
            this.pieLabelsDataLabelsNoneBarItem1.Id = 51;
            this.pieLabelsDataLabelsNoneBarItem1.Name = "pieLabelsDataLabelsNoneBarItem1";
            // 
            // pieLabelsDataLabelArgumentBarItem1
            // 
            this.pieLabelsDataLabelArgumentBarItem1.Id = 52;
            this.pieLabelsDataLabelArgumentBarItem1.Name = "pieLabelsDataLabelArgumentBarItem1";
            // 
            // pieLabelsDataLabelsValueBarItem1
            // 
            this.pieLabelsDataLabelsValueBarItem1.Id = 53;
            this.pieLabelsDataLabelsValueBarItem1.Name = "pieLabelsDataLabelsValueBarItem1";
            // 
            // pieLabelsDataLabelsArgumentAndValueBarItem1
            // 
            this.pieLabelsDataLabelsArgumentAndValueBarItem1.Id = 54;
            this.pieLabelsDataLabelsArgumentAndValueBarItem1.Name = "pieLabelsDataLabelsArgumentAndValueBarItem1";
            // 
            // pieLabelsDataLabelsPercentBarItem1
            // 
            this.pieLabelsDataLabelsPercentBarItem1.Id = 55;
            this.pieLabelsDataLabelsPercentBarItem1.Name = "pieLabelsDataLabelsPercentBarItem1";
            // 
            // pieLabelsDataLabelsValueAndPercentBarItem1
            // 
            this.pieLabelsDataLabelsValueAndPercentBarItem1.Id = 56;
            this.pieLabelsDataLabelsValueAndPercentBarItem1.Name = "pieLabelsDataLabelsValueAndPercentBarItem1";
            // 
            // pieLabelsDataLabelsArgumentAndPercentBarItem1
            // 
            this.pieLabelsDataLabelsArgumentAndPercentBarItem1.Id = 57;
            this.pieLabelsDataLabelsArgumentAndPercentBarItem1.Name = "pieLabelsDataLabelsArgumentAndPercentBarItem1";
            // 
            // pieLabelsDataLabelsArgumentValueAndPercentBarItem1
            // 
            this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1.Id = 58;
            this.pieLabelsDataLabelsArgumentValueAndPercentBarItem1.Name = "pieLabelsDataLabelsArgumentValueAndPercentBarItem1";
            // 
            // pieTooltipsBarItem1
            // 
            this.pieTooltipsBarItem1.Id = 59;
            this.pieTooltipsBarItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsNoneBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsArgumentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsValueBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsArgumentAndValueBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsValueAndPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsArgumentAndPercentBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.pieLabelsTooltipsArgumentValueAndPercentBarItem1)});
            this.pieTooltipsBarItem1.Name = "pieTooltipsBarItem1";
            this.pieTooltipsBarItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            // 
            // pieLabelsTooltipsNoneBarItem1
            // 
            this.pieLabelsTooltipsNoneBarItem1.Id = 60;
            this.pieLabelsTooltipsNoneBarItem1.Name = "pieLabelsTooltipsNoneBarItem1";
            // 
            // pieLabelsTooltipsArgumentBarItem1
            // 
            this.pieLabelsTooltipsArgumentBarItem1.Id = 61;
            this.pieLabelsTooltipsArgumentBarItem1.Name = "pieLabelsTooltipsArgumentBarItem1";
            // 
            // pieLabelsTooltipsValueBarItem1
            // 
            this.pieLabelsTooltipsValueBarItem1.Id = 62;
            this.pieLabelsTooltipsValueBarItem1.Name = "pieLabelsTooltipsValueBarItem1";
            // 
            // pieLabelsTooltipsArgumentAndValueBarItem1
            // 
            this.pieLabelsTooltipsArgumentAndValueBarItem1.Id = 63;
            this.pieLabelsTooltipsArgumentAndValueBarItem1.Name = "pieLabelsTooltipsArgumentAndValueBarItem1";
            // 
            // pieLabelsTooltipsPercentBarItem1
            // 
            this.pieLabelsTooltipsPercentBarItem1.Id = 64;
            this.pieLabelsTooltipsPercentBarItem1.Name = "pieLabelsTooltipsPercentBarItem1";
            // 
            // pieLabelsTooltipsValueAndPercentBarItem1
            // 
            this.pieLabelsTooltipsValueAndPercentBarItem1.Id = 65;
            this.pieLabelsTooltipsValueAndPercentBarItem1.Name = "pieLabelsTooltipsValueAndPercentBarItem1";
            // 
            // pieLabelsTooltipsArgumentAndPercentBarItem1
            // 
            this.pieLabelsTooltipsArgumentAndPercentBarItem1.Id = 66;
            this.pieLabelsTooltipsArgumentAndPercentBarItem1.Name = "pieLabelsTooltipsArgumentAndPercentBarItem1";
            // 
            // pieLabelsTooltipsArgumentValueAndPercentBarItem1
            // 
            this.pieLabelsTooltipsArgumentValueAndPercentBarItem1.Id = 67;
            this.pieLabelsTooltipsArgumentValueAndPercentBarItem1.Name = "pieLabelsTooltipsArgumentValueAndPercentBarItem1";
            // 
            // pieStyleRibbonPageGroup1
            // 
            this.pieStyleRibbonPageGroup1.ItemLinks.Add(this.pieStylePieBarItem1);
            this.pieStyleRibbonPageGroup1.ItemLinks.Add(this.pieStyleDonutBarItem1);
            this.pieStyleRibbonPageGroup1.Name = "pieStyleRibbonPageGroup1";
            // 
            // pieStylePieBarItem1
            // 
            this.pieStylePieBarItem1.Id = 68;
            this.pieStylePieBarItem1.Name = "pieStylePieBarItem1";
            // 
            // pieStyleDonutBarItem1
            // 
            this.pieStyleDonutBarItem1.Id = 69;
            this.pieStyleDonutBarItem1.Name = "pieStyleDonutBarItem1";
            // 
            // gaugeStyleRibbonPageGroup1
            // 
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleFullCircularBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleHalfCircularBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleLeftQuarterCircularBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleRightQuarterCircularBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleThreeForthCircularBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleLinearHorizontalBarItem1);
            this.gaugeStyleRibbonPageGroup1.ItemLinks.Add(this.gaugeStyleLinearVerticalBarItem1);
            this.gaugeStyleRibbonPageGroup1.Name = "gaugeStyleRibbonPageGroup1";
            // 
            // gaugeStyleFullCircularBarItem1
            // 
            this.gaugeStyleFullCircularBarItem1.Id = 70;
            this.gaugeStyleFullCircularBarItem1.Name = "gaugeStyleFullCircularBarItem1";
            // 
            // gaugeStyleHalfCircularBarItem1
            // 
            this.gaugeStyleHalfCircularBarItem1.Id = 71;
            this.gaugeStyleHalfCircularBarItem1.Name = "gaugeStyleHalfCircularBarItem1";
            // 
            // gaugeStyleLeftQuarterCircularBarItem1
            // 
            this.gaugeStyleLeftQuarterCircularBarItem1.Id = 72;
            this.gaugeStyleLeftQuarterCircularBarItem1.Name = "gaugeStyleLeftQuarterCircularBarItem1";
            // 
            // gaugeStyleRightQuarterCircularBarItem1
            // 
            this.gaugeStyleRightQuarterCircularBarItem1.Id = 73;
            this.gaugeStyleRightQuarterCircularBarItem1.Name = "gaugeStyleRightQuarterCircularBarItem1";
            // 
            // gaugeStyleThreeForthCircularBarItem1
            // 
            this.gaugeStyleThreeForthCircularBarItem1.Id = 74;
            this.gaugeStyleThreeForthCircularBarItem1.Name = "gaugeStyleThreeForthCircularBarItem1";
            // 
            // gaugeStyleLinearHorizontalBarItem1
            // 
            this.gaugeStyleLinearHorizontalBarItem1.Id = 75;
            this.gaugeStyleLinearHorizontalBarItem1.Name = "gaugeStyleLinearHorizontalBarItem1";
            // 
            // gaugeStyleLinearVerticalBarItem1
            // 
            this.gaugeStyleLinearVerticalBarItem1.Id = 76;
            this.gaugeStyleLinearVerticalBarItem1.Name = "gaugeStyleLinearVerticalBarItem1";
            // 
            // imageOpenRibbonPageGroup1
            // 
            this.imageOpenRibbonPageGroup1.ItemLinks.Add(this.imageLoadBarItem1);
            this.imageOpenRibbonPageGroup1.ItemLinks.Add(this.imageImportBarItem1);
            this.imageOpenRibbonPageGroup1.Name = "imageOpenRibbonPageGroup1";
            // 
            // imageOptionsRibbonPage1
            // 
            this.imageOptionsRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.imageOpenRibbonPageGroup1,
            this.imageSizeModeRibbonPageGroup1,
            this.imageAlignmentRibbonPageGroup1});
            this.imageOptionsRibbonPage1.Name = "imageOptionsRibbonPage1";
            this.imageOptionsRibbonPage1.Visible = false;
            // 
            // imageToolsRibbonPageCategory1
            // 
            this.imageToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.imageToolsRibbonPageCategory1.Name = "imageToolsRibbonPageCategory1";
            this.imageToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.imageOptionsRibbonPage1});
            this.imageToolsRibbonPageCategory1.Visible = false;
            // 
            // imageLoadBarItem1
            // 
            this.imageLoadBarItem1.Id = 77;
            this.imageLoadBarItem1.Name = "imageLoadBarItem1";
            // 
            // imageImportBarItem1
            // 
            this.imageImportBarItem1.Id = 78;
            this.imageImportBarItem1.Name = "imageImportBarItem1";
            // 
            // imageSizeModeRibbonPageGroup1
            // 
            this.imageSizeModeRibbonPageGroup1.ItemLinks.Add(this.imageSizeModeClipBarItem1);
            this.imageSizeModeRibbonPageGroup1.ItemLinks.Add(this.imageSizeModeStretchBarItem1);
            this.imageSizeModeRibbonPageGroup1.ItemLinks.Add(this.imageSizeModeSqueezeBarItem1);
            this.imageSizeModeRibbonPageGroup1.ItemLinks.Add(this.imageSizeModeZoomBarItem1);
            this.imageSizeModeRibbonPageGroup1.Name = "imageSizeModeRibbonPageGroup1";
            // 
            // imageSizeModeClipBarItem1
            // 
            this.imageSizeModeClipBarItem1.Id = 79;
            this.imageSizeModeClipBarItem1.Name = "imageSizeModeClipBarItem1";
            // 
            // imageSizeModeStretchBarItem1
            // 
            this.imageSizeModeStretchBarItem1.Id = 80;
            this.imageSizeModeStretchBarItem1.Name = "imageSizeModeStretchBarItem1";
            // 
            // imageSizeModeSqueezeBarItem1
            // 
            this.imageSizeModeSqueezeBarItem1.Id = 81;
            this.imageSizeModeSqueezeBarItem1.Name = "imageSizeModeSqueezeBarItem1";
            // 
            // imageSizeModeZoomBarItem1
            // 
            this.imageSizeModeZoomBarItem1.Id = 82;
            this.imageSizeModeZoomBarItem1.Name = "imageSizeModeZoomBarItem1";
            // 
            // imageAlignmentRibbonPageGroup1
            // 
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentTopLeftBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentCenterLeftBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentBottomLeftBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentTopCenterBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentCenterCenterBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentBottomCenterBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentTopRightBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentCenterRightBarItem1);
            this.imageAlignmentRibbonPageGroup1.ItemLinks.Add(this.imageAlignmentBottomRightBarItem1);
            this.imageAlignmentRibbonPageGroup1.Name = "imageAlignmentRibbonPageGroup1";
            // 
            // imageAlignmentTopLeftBarItem1
            // 
            this.imageAlignmentTopLeftBarItem1.Id = 83;
            this.imageAlignmentTopLeftBarItem1.Name = "imageAlignmentTopLeftBarItem1";
            this.imageAlignmentTopLeftBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentCenterLeftBarItem1
            // 
            this.imageAlignmentCenterLeftBarItem1.Id = 84;
            this.imageAlignmentCenterLeftBarItem1.Name = "imageAlignmentCenterLeftBarItem1";
            this.imageAlignmentCenterLeftBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentBottomLeftBarItem1
            // 
            this.imageAlignmentBottomLeftBarItem1.Id = 85;
            this.imageAlignmentBottomLeftBarItem1.Name = "imageAlignmentBottomLeftBarItem1";
            this.imageAlignmentBottomLeftBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentTopCenterBarItem1
            // 
            this.imageAlignmentTopCenterBarItem1.Id = 86;
            this.imageAlignmentTopCenterBarItem1.Name = "imageAlignmentTopCenterBarItem1";
            this.imageAlignmentTopCenterBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentCenterCenterBarItem1
            // 
            this.imageAlignmentCenterCenterBarItem1.Id = 87;
            this.imageAlignmentCenterCenterBarItem1.Name = "imageAlignmentCenterCenterBarItem1";
            this.imageAlignmentCenterCenterBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentBottomCenterBarItem1
            // 
            this.imageAlignmentBottomCenterBarItem1.Id = 88;
            this.imageAlignmentBottomCenterBarItem1.Name = "imageAlignmentBottomCenterBarItem1";
            this.imageAlignmentBottomCenterBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentTopRightBarItem1
            // 
            this.imageAlignmentTopRightBarItem1.Id = 89;
            this.imageAlignmentTopRightBarItem1.Name = "imageAlignmentTopRightBarItem1";
            this.imageAlignmentTopRightBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentCenterRightBarItem1
            // 
            this.imageAlignmentCenterRightBarItem1.Id = 90;
            this.imageAlignmentCenterRightBarItem1.Name = "imageAlignmentCenterRightBarItem1";
            this.imageAlignmentCenterRightBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // imageAlignmentBottomRightBarItem1
            // 
            this.imageAlignmentBottomRightBarItem1.Id = 91;
            this.imageAlignmentBottomRightBarItem1.Name = "imageAlignmentBottomRightBarItem1";
            this.imageAlignmentBottomRightBarItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // textBoxSettingsRibbonPageGroup1
            // 
            this.textBoxSettingsRibbonPageGroup1.ItemLinks.Add(this.textBoxEditTextBarItem1);
            this.textBoxSettingsRibbonPageGroup1.Name = "textBoxSettingsRibbonPageGroup1";
            // 
            // textBoxFormatRibbonPage1
            // 
            this.textBoxFormatRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.textBoxSettingsRibbonPageGroup1});
            this.textBoxFormatRibbonPage1.Name = "textBoxFormatRibbonPage1";
            this.textBoxFormatRibbonPage1.Visible = false;
            // 
            // textBoxToolsRibbonPageCategory1
            // 
            this.textBoxToolsRibbonPageCategory1.Control = this.dashboardDesigner1;
            this.textBoxToolsRibbonPageCategory1.Name = "textBoxToolsRibbonPageCategory1";
            this.textBoxToolsRibbonPageCategory1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.textBoxFormatRibbonPage1});
            this.textBoxToolsRibbonPageCategory1.Visible = false;
            // 
            // textBoxEditTextBarItem1
            // 
            this.textBoxEditTextBarItem1.Id = 92;
            this.textBoxEditTextBarItem1.Name = "textBoxEditTextBarItem1";
            // 
            // rangeFilterSeriesTypeRibbonPageGroup1
            // 
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterLineSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterStackedLineSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterFullStackedLineSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterAreaSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterStackedAreaSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.ItemLinks.Add(this.rangeFilterFullStackedAreaSeriesTypeBarItem1);
            this.rangeFilterSeriesTypeRibbonPageGroup1.Name = "rangeFilterSeriesTypeRibbonPageGroup1";
            // 
            // rangeFilterStyleRibbonPage1
            // 
            this.rangeFilterStyleRibbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rangeFilterSeriesTypeRibbonPageGroup1});
            this.rangeFilterStyleRibbonPage1.Name = "rangeFilterStyleRibbonPage1";
            this.rangeFilterStyleRibbonPage1.Visible = false;
            // 
            // rangeFilterLineSeriesTypeBarItem1
            // 
            this.rangeFilterLineSeriesTypeBarItem1.Id = 93;
            this.rangeFilterLineSeriesTypeBarItem1.Name = "rangeFilterLineSeriesTypeBarItem1";
            // 
            // rangeFilterStackedLineSeriesTypeBarItem1
            // 
            this.rangeFilterStackedLineSeriesTypeBarItem1.Id = 94;
            this.rangeFilterStackedLineSeriesTypeBarItem1.Name = "rangeFilterStackedLineSeriesTypeBarItem1";
            // 
            // rangeFilterFullStackedLineSeriesTypeBarItem1
            // 
            this.rangeFilterFullStackedLineSeriesTypeBarItem1.Id = 95;
            this.rangeFilterFullStackedLineSeriesTypeBarItem1.Name = "rangeFilterFullStackedLineSeriesTypeBarItem1";
            // 
            // rangeFilterAreaSeriesTypeBarItem1
            // 
            this.rangeFilterAreaSeriesTypeBarItem1.Id = 96;
            this.rangeFilterAreaSeriesTypeBarItem1.Name = "rangeFilterAreaSeriesTypeBarItem1";
            // 
            // rangeFilterStackedAreaSeriesTypeBarItem1
            // 
            this.rangeFilterStackedAreaSeriesTypeBarItem1.Id = 97;
            this.rangeFilterStackedAreaSeriesTypeBarItem1.Name = "rangeFilterStackedAreaSeriesTypeBarItem1";
            // 
            // rangeFilterFullStackedAreaSeriesTypeBarItem1
            // 
            this.rangeFilterFullStackedAreaSeriesTypeBarItem1.Id = 98;
            this.rangeFilterFullStackedAreaSeriesTypeBarItem1.Name = "rangeFilterFullStackedAreaSeriesTypeBarItem1";
            // 
            // quickAccessHistoryRibbonPageGroup1
            // 
            this.quickAccessHistoryRibbonPageGroup1.ItemLinks.Add(this.quickAccessUndoBarItem1);
            this.quickAccessHistoryRibbonPageGroup1.ItemLinks.Add(this.quickAccessRedoBarItem1);
            this.quickAccessHistoryRibbonPageGroup1.Name = "quickAccessHistoryRibbonPageGroup1";
            this.quickAccessHistoryRibbonPageGroup1.Visible = false;
            // 
            // quickAccessUndoBarItem1
            // 
            this.quickAccessUndoBarItem1.Id = 99;
            this.quickAccessUndoBarItem1.Name = "quickAccessUndoBarItem1";
            // 
            // quickAccessRedoBarItem1
            // 
            this.quickAccessRedoBarItem1.Id = 100;
            this.quickAccessRedoBarItem1.Name = "quickAccessRedoBarItem1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 486);
            this.Controls.Add(this.dashboardDesigner1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "MainForm";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dashboardBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraDashboard.DashboardDesigner dashboardDesigner1;
        private DevExpress.XtraDashboard.Bars.FileNewBarItem fileNewBarItem1;
        private DevExpress.XtraDashboard.Bars.FileOpenBarItem fileOpenBarItem1;
        private DevExpress.XtraDashboard.Bars.FileSaveBarItem fileSaveBarItem1;
        private DevExpress.XtraDashboard.Bars.FileSaveAsBarItem fileSaveAsBarItem1;
        private DevExpress.XtraDashboard.Bars.UndoBarItem undoBarItem1;
        private DevExpress.XtraDashboard.Bars.RedoBarItem redoBarItem1;
        private DevExpress.XtraDashboard.Bars.NewDataSourceBarItem newDataSourceBarItem1;
        private DevExpress.XtraDashboard.Bars.EditDataSourceBarItem editDataSourceBarItem1;
        private DevExpress.XtraDashboard.Bars.DeleteDataSourceBarItem deleteDataSourceBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertPivotBarItem insertPivotBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertGridBarItem insertGridBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertChartBarItem insertChartBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertPiesBarItem insertPiesBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertGaugesBarItem insertGaugesBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertCardsBarItem insertCardsBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertImageBarItem insertImageBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertTextBoxBarItem insertTextBoxBarItem1;
        private DevExpress.XtraDashboard.Bars.InsertRangeFilterBarItem insertRangeFilterBarItem1;
        private DevExpress.XtraDashboard.Bars.DuplicateItemBarItem duplicateItemBarItem1;
        private DevExpress.XtraDashboard.Bars.DeleteItemBarItem deleteItemBarItem1;
        private DevExpress.XtraDashboard.Bars.SetCurrencyCultureBarItem setCurrencyCultureBarItem1;
        private DevExpress.XtraDashboard.Bars.DashboardSkinsBarItem dashboardSkinsBarItem1;
        private DevExpress.XtraDashboard.Bars.EditFilterBarItem editFilterBarItem1;
        private DevExpress.XtraDashboard.Bars.ClearFilterBarItem clearFilterBarItem1;
        private DevExpress.XtraDashboard.Bars.IgnoreMasterFiltersBarItem ignoreMasterFiltersBarItem1;
        private DevExpress.XtraDashboard.Bars.MasterFilterBarItem masterFilterBarItem1;
        private DevExpress.XtraDashboard.Bars.CrossDataSourceFilteringBarItem crossDataSourceFilteringBarItem1;
        private DevExpress.XtraDashboard.Bars.FilterByChartSeriesBarItem filterByChartSeriesBarItem1;
        private DevExpress.XtraDashboard.Bars.FilterByChartArgumentsBarItem filterByChartArgumentsBarItem1;
        private DevExpress.XtraDashboard.Bars.FilterByPieSeriesBarItem filterByPieSeriesBarItem1;
        private DevExpress.XtraDashboard.Bars.FilterByPieArgumentsBarItem filterByPieArgumentsBarItem1;
        private DevExpress.XtraDashboard.Bars.DrillDownBarItem drillDownBarItem1;
        private DevExpress.XtraDashboard.Bars.DrillDownOnChartSeriesBarItem drillDownOnChartSeriesBarItem1;
        private DevExpress.XtraDashboard.Bars.DrillDownOnChartArgumentsBarItem drillDownOnChartArgumentsBarItem1;
        private DevExpress.XtraDashboard.Bars.DrillDownOnPieSeriesBarItem drillDownOnPieSeriesBarItem1;
        private DevExpress.XtraDashboard.Bars.DrillDownOnPieArgumentsBarItem drillDownOnPieArgumentsBarItem1;
        private DevExpress.XtraDashboard.Bars.ContentAutoArrangeBarItem contentAutoArrangeBarItem1;
        private DevExpress.XtraDashboard.Bars.ContentArrangeInColumnsBarItem contentArrangeInColumnsBarItem1;
        private DevExpress.XtraDashboard.Bars.ContentArrangeInRowsBarItem contentArrangeInRowsBarItem1;
        private DevExpress.XtraDashboard.Bars.ContentArrangementCountBarItem contentArrangementCountBarItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraDashboard.Bars.GridHorizontalLinesBarItem gridHorizontalLinesBarItem1;
        private DevExpress.XtraDashboard.Bars.GridVerticalLinesBarItem gridVerticalLinesBarItem1;
        private DevExpress.XtraDashboard.Bars.GridMergeCellsBarItem gridMergeCellsBarItem1;
        private DevExpress.XtraDashboard.Bars.GridBandedRowsBarItem gridBandedRowsBarItem1;
        private DevExpress.XtraDashboard.Bars.GridColumnHeadersBarItem gridColumnHeadersBarItem1;
        private DevExpress.XtraDashboard.Bars.ChartRotateBarItem chartRotateBarItem1;
        private DevExpress.XtraDashboard.Bars.ChartShowLegendBarItem chartShowLegendBarItem1;
        private DevExpress.XtraDashboard.Bars.ChartYAxisSettingsBarItem chartYAxisSettingsBarItem1;
        private DevExpress.XtraDashboard.Bars.GalleryChartSeriesTypeItem galleryChartSeriesTypeItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsBarItem pieLabelsDataLabelsBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsNoneBarItem pieLabelsDataLabelsNoneBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelArgumentBarItem pieLabelsDataLabelArgumentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsValueBarItem pieLabelsDataLabelsValueBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentAndValueBarItem pieLabelsDataLabelsArgumentAndValueBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsPercentBarItem pieLabelsDataLabelsPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsValueAndPercentBarItem pieLabelsDataLabelsValueAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentAndPercentBarItem pieLabelsDataLabelsArgumentAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsDataLabelsArgumentValueAndPercentBarItem pieLabelsDataLabelsArgumentValueAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieTooltipsBarItem pieTooltipsBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsNoneBarItem pieLabelsTooltipsNoneBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentBarItem pieLabelsTooltipsArgumentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsValueBarItem pieLabelsTooltipsValueBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentAndValueBarItem pieLabelsTooltipsArgumentAndValueBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsPercentBarItem pieLabelsTooltipsPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsValueAndPercentBarItem pieLabelsTooltipsValueAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentAndPercentBarItem pieLabelsTooltipsArgumentAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieLabelsTooltipsArgumentValueAndPercentBarItem pieLabelsTooltipsArgumentValueAndPercentBarItem1;
        private DevExpress.XtraDashboard.Bars.PieStylePieBarItem pieStylePieBarItem1;
        private DevExpress.XtraDashboard.Bars.PieStyleDonutBarItem pieStyleDonutBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleFullCircularBarItem gaugeStyleFullCircularBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleHalfCircularBarItem gaugeStyleHalfCircularBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleLeftQuarterCircularBarItem gaugeStyleLeftQuarterCircularBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleRightQuarterCircularBarItem gaugeStyleRightQuarterCircularBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleThreeForthCircularBarItem gaugeStyleThreeForthCircularBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleLinearHorizontalBarItem gaugeStyleLinearHorizontalBarItem1;
        private DevExpress.XtraDashboard.Bars.GaugeStyleLinearVerticalBarItem gaugeStyleLinearVerticalBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageLoadBarItem imageLoadBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageImportBarItem imageImportBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageSizeModeClipBarItem imageSizeModeClipBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageSizeModeStretchBarItem imageSizeModeStretchBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageSizeModeSqueezeBarItem imageSizeModeSqueezeBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageSizeModeZoomBarItem imageSizeModeZoomBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentTopLeftBarItem imageAlignmentTopLeftBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentCenterLeftBarItem imageAlignmentCenterLeftBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentBottomLeftBarItem imageAlignmentBottomLeftBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentTopCenterBarItem imageAlignmentTopCenterBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentCenterCenterBarItem imageAlignmentCenterCenterBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentBottomCenterBarItem imageAlignmentBottomCenterBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentTopRightBarItem imageAlignmentTopRightBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentCenterRightBarItem imageAlignmentCenterRightBarItem1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentBottomRightBarItem imageAlignmentBottomRightBarItem1;
        private DevExpress.XtraDashboard.Bars.TextBoxEditTextBarItem textBoxEditTextBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterLineSeriesTypeBarItem rangeFilterLineSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterStackedLineSeriesTypeBarItem rangeFilterStackedLineSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterFullStackedLineSeriesTypeBarItem rangeFilterFullStackedLineSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterAreaSeriesTypeBarItem rangeFilterAreaSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterStackedAreaSeriesTypeBarItem rangeFilterStackedAreaSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.RangeFilterFullStackedAreaSeriesTypeBarItem rangeFilterFullStackedAreaSeriesTypeBarItem1;
        private DevExpress.XtraDashboard.Bars.QuickAccessUndoBarItem quickAccessUndoBarItem1;
        private DevExpress.XtraDashboard.Bars.QuickAccessRedoBarItem quickAccessRedoBarItem1;
        private DevExpress.XtraDashboard.Bars.PivotToolsRibbonPageCategory pivotToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage1;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.GridToolsRibbonPageCategory gridToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage2;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup2;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup2;
        private DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup drillDownRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage layoutAndStyleRibbonPage4;
        private DevExpress.XtraDashboard.Bars.GridCellsRibbonPageGroup gridCellsRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.ChartToolsRibbonPageCategory chartToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage3;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup3;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup3;
        private DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup drillDownRibbonPageGroup2;
        private DevExpress.XtraDashboard.Bars.ChartLayoutAndStyleRibbonPage chartLayoutAndStyleRibbonPage1;
        private DevExpress.XtraDashboard.Bars.ChartLayoutPageGroup chartLayoutPageGroup1;
        private DevExpress.XtraDashboard.Bars.ChartStylePageGroup chartStylePageGroup1;
        private DevExpress.XtraDashboard.Bars.PiesToolsRibbonPageCategory piesToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage4;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup4;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup4;
        private DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup drillDownRibbonPageGroup3;
        private DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage layoutAndStyleRibbonPage1;
        private DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup contentArrangementRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.PieLabelsRibbonPageGroup pieLabelsRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.PieStyleRibbonPageGroup pieStyleRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.GaugesToolsRibbonPageCategory gaugesToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage5;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup5;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup5;
        private DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup drillDownRibbonPageGroup4;
        private DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage layoutAndStyleRibbonPage2;
        private DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup contentArrangementRibbonPageGroup2;
        private DevExpress.XtraDashboard.Bars.GaugeStyleRibbonPageGroup gaugeStyleRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.CardsToolsRibbonPageCategory cardsToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage6;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup6;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup6;
        private DevExpress.XtraDashboard.Bars.DrillDownRibbonPageGroup drillDownRibbonPageGroup5;
        private DevExpress.XtraDashboard.Bars.LayoutAndStyleRibbonPage layoutAndStyleRibbonPage3;
        private DevExpress.XtraDashboard.Bars.ContentArrangementRibbonPageGroup contentArrangementRibbonPageGroup3;
        private DevExpress.XtraDashboard.Bars.RangeFilterToolsRibbonPageCategory rangeFilterToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.DataRibbonPage dataRibbonPage7;
        private DevExpress.XtraDashboard.Bars.FilteringRibbonPageGroup filteringRibbonPageGroup7;
        private DevExpress.XtraDashboard.Bars.MasterFilterRibbonPageGroup masterFilterRibbonPageGroup7;
        private DevExpress.XtraDashboard.Bars.RangeFilterStyleRibbonPage rangeFilterStyleRibbonPage1;
        private DevExpress.XtraDashboard.Bars.RangeFilterSeriesTypeRibbonPageGroup rangeFilterSeriesTypeRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.ImageToolsRibbonPageCategory imageToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.ImageOptionsRibbonPage imageOptionsRibbonPage1;
        private DevExpress.XtraDashboard.Bars.ImageOpenRibbonPageGroup imageOpenRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.ImageSizeModeRibbonPageGroup imageSizeModeRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.ImageAlignmentRibbonPageGroup imageAlignmentRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.TextBoxToolsRibbonPageCategory textBoxToolsRibbonPageCategory1;
        private DevExpress.XtraDashboard.Bars.TextBoxFormatRibbonPage textBoxFormatRibbonPage1;
        private DevExpress.XtraDashboard.Bars.TextBoxSettingsRibbonPageGroup textBoxSettingsRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.DashboardRibbonPage dashboardRibbonPage1;
        private DevExpress.XtraDashboard.Bars.FileRibbonPageGroup fileRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.HistoryRibbonPageGroup historyRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.DataSourceRibbonPageGroup dataSourceRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.InsertRibbonPageGroup insertRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.ItemsRibbonPageGroup itemsRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.FormatRibbonPageGroup formatRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.SkinsRibbonPageGroup skinsRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.QuickAccessHistoryRibbonPageGroup quickAccessHistoryRibbonPageGroup1;
        private DevExpress.XtraDashboard.Bars.DashboardBarController dashboardBarController1;
    }
}