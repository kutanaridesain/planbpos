USE [master]
GO
/****** Object:  Database [pos]    Script Date: 5/24/2013 11:21:05 AM ******/
CREATE DATABASE [pos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'pos', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\pos.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'pos_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\pos_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [pos] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [pos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [pos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [pos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [pos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [pos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [pos] SET ARITHABORT OFF 
GO
ALTER DATABASE [pos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [pos] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [pos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [pos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [pos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [pos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [pos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [pos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [pos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [pos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [pos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [pos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [pos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [pos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [pos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [pos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [pos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [pos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [pos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [pos] SET  MULTI_USER 
GO
ALTER DATABASE [pos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [pos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [pos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [pos] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [pos]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categori] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BrandId] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Barcode] [nvarchar](50) NULL,
	[Unit] [nvarchar](50) NOT NULL,
	[ItemPerUnit] [int] NOT NULL,
	[PricePerUnit] [money] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductDiscounts]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDiscounts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[DiscountA] [float] NOT NULL,
	[DiscountB] [float] NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [PK_CustomerProductDiscount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Purchase]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purchase](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SupplierId] [int] NOT NULL,
	[PurchaseDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseDetail]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PurchaseId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[PricePerItem] [money] NOT NULL,
	[QuantityInUnit] [int] NOT NULL,
 CONSTRAINT [PK_PurchaseDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sale]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sale](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SaleDate] [datetime] NOT NULL,
	[StoreCode] [nvarchar](10) NOT NULL,
	[SellerCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Sale] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SaleDetails]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaleDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SaleId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[DiscountA] [float] NULL,
	[DiscountB] [float] NULL,
	[DiscountSales] [float] NULL,
	[GrossPrice] [money] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_SaleDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Seller]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seller](
	[Code] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[Telephone] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Seller] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Stores]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stores](
	[Code] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[Telephone] [nvarchar](20) NOT NULL,
	[SellerCode] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Address] [nvarchar](500) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 5/24/2013 11:21:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Brand] ON 

INSERT [dbo].[Brand] ([Id], [Name]) VALUES (1, N'OLAY')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (2, N'CAMAY')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (3, N'SAFEGUARD')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (4, N'HERBAL')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (5, N'HE')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (6, N'HS')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (7, N'H&S')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (8, N'PANT')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (9, N'PAN')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (10, N'PANTENE')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (11, N'REJOICE')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (12, N'REJ')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (13, N'RJC')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (14, N'WELLA')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (15, N'KWELLA')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (16, N'KSTN')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (17, N'GIL')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (18, N'MACH3')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (19, N'VECTOR')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (20, N'GILLETTE')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (21, N'BLUE')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (22, N'TGS')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (23, N'DURACELL')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (24, N'AMBIPUR')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (25, N'AP')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (26, N'AMBI')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (27, N'PAMP')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (28, N'PAMPERS')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (29, N'SECRET')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (30, N'OLD')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (31, N'WHISPER')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (32, N'DOWNY')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (33, N'OB')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (34, N'ORAL')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (35, N'ORAL-B')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (36, N'FORMULA')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (37, N'VICKS')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (1001, N'cgDXCddd')
INSERT [dbo].[Brand] ([Id], [Name]) VALUES (1002, N'coba')
SET IDENTITY_INSERT [dbo].[Brand] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (1, 1, N'3557050', N'OLAY REG. MICRO SCULPTING CREAM 50GR ', N'4902430-242875', N'CR', 1, 155425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (2, 33, N'53215075', N'OB STAGES PASTE - WINNIE 75ML12 ', N'3014260-279349', N'CR', 1, 12400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (3, 33, N'53219075', N'OB STAGES PASTE - CARS 75ML12 ', N'3014260-279035', N'CR', 1, 12400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (4, 27, N'4503068', N'PAMP NEW AB MEDIUM 68S ', N'4 902430-172547', N'CR', 4, 147550.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (5, 27, N'4504060', N'PAMP NEW AB LARGE 60S ', N'4 902430-172608', N'CR', 4, 147550.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (6, 27, N'4513056', N'PAMP AB PANTS MEDIUM 56S', N'4 902430-406192', N'CR', 4, 139150.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (7, 27, N'4514044', N'PAMP AB PANTS LARGE 44S', N'4 902430-406208', N'CR', 4, 139150.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (8, 32, N'5702111800', N'DOWNY ANTI BACTERI  BOTOL1.8L', N'4902430453240', N'CR', 4, 45300.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (9, 32, N'570112000', N'DOWNY SUNRISE FRESH BOTOL 2L', N'4902430993043', N'CR', 4, 47600.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (10, 32, N'57061200', N'DOWNY GARDEN BLOOM BOTOL 2LTR', N'4902430452809', N'CR', 4, 47600.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (11, 32, N'570411800', N'DOWNY ATTRACTION BOTOL 1.8L', N'4902430355186', N'CR', 4, 46110.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (12, 32, N'570311800', N'DOWNY PASSION BOTOL 1.8L', N'4902430355209', N'CR', 4, 46100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (13, 1, N'3550050', N'OLAY REGENERIST SERUM 50ML ', N'4902430-188876', N'CR', 6, 103575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (14, 1, N'3552050', N'OLAY REGENERIST NIGHT 50GR ', N'4902430-188937', N'CR', 6, 103575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (15, 1, N'3553050', N'OLAY REGENERIST UV CREAM 50GR ', N'4902430-231862', N'CR', 6, 103575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (16, 1, N'3101050', N'OLAY TE NORMAL CREAM 50GR ', N'4902430-359764', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (17, 1, N'3102050', N'OLAY TE NORMAL CREAM SPF 15 50GR ', N'4902430-360111', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (18, 1, N'3101020', N'OLAY TE NORMAL CREAM 20GR ', N'4902430-359788', N'CR', 6, 44227.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (19, 1, N'3102020', N'OLAY TE NORMAL CREAM SPF 15 20GR ', N'4902430-360906', N'CR', 6, 44226.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (20, 1, N'3100050', N'OLAY TE GENTLE CREAM 50GR ', N'4902430-359719', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (21, 1, N'3109050', N'OLAY TE GENTLE CREAM SPF 15 50GR ', N'4902430-359733', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (22, 1, N'3103050', N'OLAY TE COOLING ESSENCE 50GR ', N'4902430-359801', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (23, 1, N'3271050', N'OLAY TE TOUCH OF FOUND SPF15 50G', N'4902430-359818', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (24, 1, N'3104050', N'OLAY TE NIGHT CREAM 50GR ', N'4902430-360142', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (25, 1, N'3106050', N'OLAY TE SERUM 50GR ', N'4902430-359825', N'CR', 6, 99000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (26, 1, N'3421050', N'OLAY WR PROTECTIVE CREAM 50GR ', N'4902430-232531', N'CR', 6, 96875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (27, 1, N'3423050', N'OLAY WR NIGHT RESTORING CREAM 50GR ', N'4902430-200059', N'CR', 6, 96875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (28, 1, N'3421075', N'OLAY WR PROTECTIVE LATION 75GR ', N'4902430-232548', N'CR', 6, 96875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (29, 1, N'3427050', N'OLAY WR INTENSIVE BRIGHTENING SERUM 50ML ', N'4902430-200653', N'CR', 6, 96875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (30, 1, N'3424050', N'OLAY WR UV BLOCKER 50GR ', N'4902430-232555', N'CR', 6, 96875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (31, 1, N'3723050', N'OLAY REGENERIST MICRO SCULPTING SERUM 50ML ', N'4902430-293341', N'CR', 6, 155425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (32, 1, N'3711050', N'OLAY WR CREAM ESSENCE SHAPE MEMORY 50GR', N'4902430-291187', N'CR', 6, 155425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (33, 1, N'3715150', N'OLAY WR ESSENCE WATER 150ML', N'4902430-321174', N'CR', 6, 155425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (34, 1, N'3712040', N'OLAY WR SERUM ESSENCE 40ML', N'4902430-292962', N'CR', 6, 155425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (35, 1, N'39910501', N'OLAY TE UV CREAM 50G + CLSR 50G ', N'4902430-467636', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (36, 1, N'39920501', N'OLAY TE UVFF GENTLE CREAM 50G + CLSR 50G ', N'4902430-467643', N'CR', 6, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (37, 9, N'5741670', N'PAN SHP HAIR FALL CONTROL 670ML ', N'4902430-401111', N'CR', 6, 56025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (38, 9, N'5741900', N'PAN SHP HAIR FALL CONTROL 900ML ', N'4902430-404686', N'CR', 6, 60875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (39, 9, N'5711670', N'PAN SHP ANTI DANDRUFF 670ML ', N'4902430-401135', N'CR', 6, 56025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (40, 9, N'5711900', N'PAN SHP ANTI DANDRUFF 900ML ', N'4902430-404716', N'CR', 6, 60875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (41, 9, N'5751670', N'PAN SHP TOTAL CARE 670ML ', N'4902430-399340', N'CR', 6, 56025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (42, 9, N'5751900', N'PAN SHP TOTAL CARE 900ML ', N'4902430-404709', N'CR', 6, 60875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (43, 9, N'57841670', N'PAN SHP NC Smoothness Life 670ML', N'4902430-451574', N'CR', 6, 56100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (44, 9, N'57831670', N'PAN SHP NC Fullness & Life 670ML', N'4902430-401128', N'CR', 6, 56100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (45, 9, N'5761670', N'PAN SHP NOURISHED SHINE 670ML ', N'4902430-399395', N'CR', 6, 56025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (46, 9, N'5761900', N'PAN SHP NOURISHED SHINE 900ML ', N'4902430-404693', N'CR', 6, 60875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (47, 11, N'6219600', N'REJOICE SHP RICH 600ML ', N'4 902430-396554', N'CR', 6, 35426.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (48, 12, N'6211600', N'REJ SHP SOFT AND SMOOTH 600', N'4 902430-432375', N'CR', 6, 35426.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (49, 11, N'6216600', N'REJOICE SHP ANTI FRIZZ 600ML ', N'4 902430-399555', N'CR', 6, 35426.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (50, 11, N'6217600', N'REJOICE SHP 3IN1 600ML ', N'4 902430-396547', N'CR', 6, 35426.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (51, 17, N'519632001', N'GIL MACH3 TUB SEN RAZ+ GIL TGS MST 15ML ', N'4902430-372978', N'CR', 6, 37775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (52, 17, N'519631001', N'GIL MACH3 TURBO SEN + GIL TGS MST 15ML ', N'4902430-372985', N'CR', 6, 100450.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (53, 17, N'51402200', N'GIL SERIES GEL 200ML CAN COOL CLEANSING 1', N'7702018-001767', N'CR', 6, 41525.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (54, 22, N'514314075', N'TGS LOTION MST 75 ML REG STK ID - 1', N'7702018-074877', N'CR', 6, 62275.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (55, 22, N'514313075', N'TGS LOTION MST 75 ML SPF15 STK ID - 1', N'7702018-074754', N'CR', 6, 62275.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (56, 27, N'4503034', N'PAMP NEW AB PANTS MEDIUM 34S ', N'4 902430-287838', N'CR', 6, 93375.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (57, 27, N'4504028', N'PAMP NEW AB PANTS LARGE 28S ', N'4 902430-287821', N'CR', 6, 93375.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (58, 32, N'570131800', N'DOWNY SUNRISE FRESH REFILL 1.8L', N'4902430245012', N'CR', 6, 33700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (59, 32, N'570631800', N'DOWNY GARDEN BLOOM REFILL 1.8L', N'4902430452816', N'CR', 6, 33700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (60, 32, N'570431600', N'DOWNY ATTRACTION REFILL 1.6L', N'4902430354974', N'CR', 6, 34300.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (61, 32, N'570331600', N'DOWNY PASSION REFILL 1.6L', N'4902430355193', N'CR', 6, 34300.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (62, 27, N'4501024', N'PAMP NEW AB NB 24S ', N'4 902430-287845', N'CR', 8, 44900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (63, 27, N'4502022', N'PAMP NEW AB SMALL 22S ', N'4 902430-167765', N'CR', 8, 43900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (64, 27, N'4503020', N'PAMP NEW AB MEDIUM 20S ', N'4 902430-167772', N'CR', 8, 43900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (65, 27, N'4504018', N'PAMP NEW AB LARGE 18S ', N'4 902430-167789', N'CR', 8, 48550.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (66, 27, N'4601120', N'PAMP ORANGE PANTS M20 ', N'4 902430-301022', N'CR', 8, 35900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (67, 27, N'4601220', N'PAMP ORANGE PANTS L20 ', N'4 902430-301039', N'CR', 8, 40625.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (68, 32, N'570231000', N'DOWNY ANTI BACTERI REFILL 1L', N'4902430358583', N'CR', 8, 21400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (69, 32, N'570131000', N'DOWNY SUNRISE FRESH REFILL 1L', N'4902430121361', N'CR', 8, 20900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (70, 32, N'57063100', N'DOWNY GARDEN BLOOM REFILL 1L', N'4902430453172', N'CR', 8, 20900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (71, 32, N'57052900', N'DOWNY INNOCENCE REFILL 900ML ', N'4 902430 370943', N'CR', 8, 21100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (72, 32, N'570521600', N'DOWNY INNOCENCE REFILL 1.600ML ', N'4 902430 370929', N'CR', 8, 34300.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (73, 32, N'57043900', N'DOWNY ATTRACTION REFILL 900ML', N'4902430276498', N'CR', 8, 21000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (74, 32, N'57033900', N'DOWNY PASSION REFILL 900ML', N'4902430276443', N'CR', 8, 21000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (75, 24, N'56711275', N'AMBIPUR SPRAY HAWAIIAN 275G', N'4902430-342704', N'CR', 9, 34800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (76, 24, N'56713275', N'AMBIPUR SPRAY NEWZLND 275G', N'4902430-342728', N'CR', 9, 34800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (77, 24, N'56712275', N'AMBIPUR SPRAY TH DRG FRUIT 275', N'4902430-345156', N'CR', 9, 34800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (78, 22, N'514311015', N'TGS LOTION MST 15 ML REG SRP ID - 1', N'7702018-074891', N'CR', 10, 19025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (79, 22, N'514311015', N'TGS LOTION MOIST 15 ML REG SRP ID - 1', N'7702018-074778', N'CR', 10, 19025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (80, 1, N'3554015', N'OLAY REGENERIST EYE 15ML ', N'4902430-188951', N'CR', 12, 103575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (81, 1, N'3556100', N'OLAY REGENERIST CREAM CLEANSER 100GR ', N'4902430-208093', N'CR', 12, 33000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (82, 1, N'3105015', N'OLAY TE EYE CREAM 15GR ', N'4902430-360173', N'CR', 12, 90750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (83, 1, N'3127050', N'OLAY TE FOAMING CLEANSER 50GR ', N'4902430-359856', N'CR', 12, 20851.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (84, 1, N'3127100', N'OLAY TE FOAMING CLEANSER 100GR ', N'4902430-359832', N'CR', 12, 33325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (85, 1, N'3128050', N'OLAY TE CREAM CLEANSER 50GR ', N'4902430-359870', N'CR', 12, 20851.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (86, 1, N'3128100', N'OLAY TE CREAM CLEANSER 100GR ', N'4902430-359863', N'CR', 12, 33325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (87, 1, N'3411050', N'OLAY WHITE RADIANCE CREAM CLEANSER 50GR ', N'4902430-080767', N'CR', 12, 15000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (88, 1, N'3411100', N'OLAY WHITE RADIANCE CREAM CLEANSER 100GR ', N'4902430-080750', N'CR', 12, 25000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (89, 1, N'3422100', N'OLAY WR PURIFYING FOAMING CLEANSER 100GR ', N'4902430-199926', N'CR', 12, 40825.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (90, 1, N'3425015', N'OLAY WR EYE SERUM 15GR ', N'4902430-200189', N'CR', 12, 98175.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (91, 1, N'3426015', N'OLAY WR SPOT CORRECTOR 15GR ', N'4902430-200202', N'CR', 12, 98175.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (92, 1, N'3175050', N'OLAY NATURAL WHITE UV BLOCKER 50ML ', N'4902430-375245', N'CR', 12, 66475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (93, 1, N'3176050', N'OLAY NATURAL WHITE CLEANSER 50G ', N'4902430-216159', N'CR', 12, 15225.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (94, 4, N'11091300', N'HERBAL SHP HELLO HYDRATION 300ML ', N'4902430-163736', N'CR', 12, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (95, 5, N'11101300', N'HE SHP DANGEROUSLY STRAIGHT 300ML ', N'4902430-163781', N'CR', 12, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (96, 5, N'11102300', N'HE COND DANGEROUSLY STRAIGHT 300ML ', N'4902430-163798', N'CR', 12, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (97, 5, N'11151300', N'HE ESS SHP SMOOTH LOVIN 300ML ', N'4902430-268301', N'CR', 12, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (98, 5, N'11152300', N'HE ESS COND SMOOTH LOVIN 300ML ', N'4902430-268325', N'CR', 12, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (99, 7, N'2221350', N'H&S SHP MENTHOL 350ML', N'4902430-411776', N'CR', 12, 30225.0000)
GO
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (100, 7, N'2271350', N'H&S SHP ANTI HAIRFALL 350ML', N'4902430-430715', N'CR', 12, 30225.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (101, 9, N'5741340', N'PAN SHP HAIR FALL CONTROL 340ML ', N'4902430-400992', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (102, 9, N'5743335', N'PAN COND HAIR FALL CONTROL 335ML ', N'4902430-401036', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (103, 9, N'5711340', N'PAN SHP ANTI DANDRUFF 340ML ', N'4902430-401012', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (104, 9, N'5751340', N'PAN SHP TOTAL CARE 340ML ', N'4902430-400619', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (105, 9, N'5753335', N'PAN COND TOTAL CARE 335ML ', N'4902430-399432', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (106, 9, N'57841340', N'PAN SHP NC Smoothness Life 340ML', N'4902430-451536', N'CR', 12, 29825.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (107, 9, N'57843335', N'PAN COND NC Smoothness Life 335ML', N'4902430-451543', N'CR', 12, 29850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (108, 9, N'57831340', N'PAN SHP NC Fullness & Life 340ML', N'4902430-401005', N'CR', 12, 29825.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (109, 9, N'57833335', N'PAN COND NC Fullness & Life 335ML', N'4902430-401043', N'CR', 12, 29850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (110, 9, N'5721340', N'PAN SHP SMOOTH SILKY 340ML ', N'4902430-399357', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (111, 9, N'5723335', N'PAN COND SMOOTH SILKY 335ML ', N'4902430-401029', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (112, 9, N'5731340', N'PAN SHP LONG BLACK 340ML ', N'4902430-414937', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (113, 9, N'5761340', N'PAN SHP NOURISHED SHINE 340ML ', N'4902430-399388', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (114, 9, N'5763335', N'PAN COND NOURISHED SHINE 335ML ', N'4902430-399487', N'CR', 12, 29775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (115, 13, N'6219320', N'RJC SHP  RICH  320', N'4 902430450355', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (116, 11, N'6035150', N'REJOICE TRT RICH CREAMBATH 150ML ', N'4 902430-300094', N'CR', 12, 22625.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (117, 13, N'6211320', N'RJC SHP  SOFT&SMOOTH  320', N'4 902430-450362', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (118, 13, N'6213320', N'RJC SHP  FRUITY AD  320', N'4 902430450409', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (119, 13, N'6215320', N'RJC SHP  ANTI HAIR FALL  320', N'4 902430473231', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (120, 13, N'6216320', N'RJC SHP  ANTI FRIZZ 320', N'4 902430450393', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (121, 13, N'6217320', N'RJC SHP  3 IN 1 320', N'4 902430450379', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (122, 13, N'6218320', N'RJC SHP MANAGEBLE BLACK 320 ML', N'4 902430450386', N'CR', 12, 24475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (123, 14, N'46717050', N'WELLA KSTN CREAM 2/8+6% BLUE BLACK 50ML ', N'4056800614509', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (124, 14, N'46713050', N'WELLA CREAM 6/35+9% E.BROWN 50ML ', N'8850400857931', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (125, 14, N'46701050', N'WELLA CREAM 2/0+6% BLACK 50ML ', N'4056800856565', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (126, 14, N'46710050', N'WELLA CREAM 5/4+9% CHESTNUT 50ML ', N'8850400812367', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (127, 14, N'46705050', N'WELLA CREAM 4/5+6% MAHOGANY 50ML ', N'8850400450453', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (128, 14, N'46717050', N'WELLA CREAM 2/8+6% BLUE BLACK 50ML ', N'4056800614509', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (129, 15, N'46722050', N'KWELLA CREAM 7/73+9% MOCHA 50ML ', N'4056800615162', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (130, 14, N'46704050', N'WELLA CREAM 3/4+6% CHESTNUT 50ML ', N'8850400450347', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (131, 14, N'46706050', N'WELLA CREAM 4/6+6% BURGUNDY 50ML ', N'8850400450460', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (132, 14, N'46707050', N'WELLA CREAM 4/66+6% BURGUND.RED 50ML ', N'8850400812350', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (133, 14, N'46708050', N'WELLA CREAM 4/77+6% S.BROWN 50ML ', N'8850400857917', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (134, 14, N'46712050', N'WELLA CREAM 5/66+9% AUBERGINE 50ML ', N'8850400450569', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (135, 14, N'46709050', N'WELLA CREAM 5/37+6% SL.BROWN 50ML ', N'8850400857921', N'CR', 12, 48600.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (136, 14, N'46714050', N'WELLA CREAM 6/45+9% COPPER 50ML ', N'8850400450644', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (137, 14, N'46703050', N'WELLA CREAM 4/0+6% MED.BROWN 50ML ', N'8850400450408', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (138, 14, N'46720050', N'WELLA CREAM 6/73+9% MAROON 50ML ', N'4056800615100', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (139, 14, N'46719050', N'WELLA CREAM 6/55+9% MAHOGANY 50ML ', N'4056800615087', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (140, 14, N'46715050', N'WELLA CREAM 9/3+9% G.BLONDE 50ML ', N'8850400812459', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (141, 14, N'46711050', N'WELLA CREAM 5/5+9% MAHOGANY 50ML ', N'8850400450552', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (142, 14, N'46702050', N'WELLA CREAM 3/0+6% D.BROWN 50ML ', N'8850400450309', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (143, 14, N'46718050', N'WELLA CREAM 6/4+9% D.COPPER 50ML ', N'4056800614547', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (144, 14, N'46721050', N'WELLA CREAM 7/3+9% HAZELNUT 50ML ', N'4056800615124', N'CR', 12, 48575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (145, 18, N'519621004', N'MACH3 TURBO SEN CRT 4+SP AERO GL 70G I', N'4902430479936', N'CR', 12, 100450.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (146, 24, N'56064007', N'AMBIPUR CAR PINK BLOSSOM ST-C3 7 ML', N'9556076-011910', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (147, 24, N'56004007', N'AMBIPUR CAR PINK BLOSSOM RF-C3 7 ML', N'9556076-011927', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (148, 24, N'56071008', N'AMBIPUR CAR FRSH N COOL ST-C3 8ML', N'9556076-008330', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (149, 24, N'56074008', N'AMBIPUR CAR FRSH N COOL RF-C3 8ML', N'9556076-008347', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (150, 24, N'56041008', N'AMBIPUR CAR LAVENDER SPA ST-C3 8ML', N'9556076-007838', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (151, 24, N'56044008', N'AMBIPUR CAR LAVENDER SPA RF-C3 8ML', N'9556076-007845', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (152, 24, N'56061008', N'AMBIPUR CAR V.BOUQUET ST-C3 8ML', N'9556076-000303', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (153, 24, N'56064008', N'AMBIPUR CAR V.BOUQUET RF-C3 8ML', N'9556076-001256', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (154, 24, N'56081008', N'AMBIPUR CAR FRSH N LGHT ST-C3 8ML', N'9556076-007234', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (155, 24, N'56084008', N'AMBIPUR CAR FRSH N LGHT RF-C3 8ML', N'9556076-007258', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (156, 24, N'56091008', N'AMBIPUR CAR AQUA ST-C3 8ML', N'9556076-000273', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (157, 24, N'56094008', N'AMBIPUR CAR AQUA RF-C3 8ML', N'9556076-000280', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (158, 24, N'56051008', N'AMBIPUR CAR PACIFIC AIR ST-C3 8ML', N'9556076-001256', N'CR', 12, 31500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (159, 24, N'56054008', N'AMBIPUR CAR PACIFIC AIR RF-C3 8ML', N'9556076-001270', N'CR', 12, 23850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (160, 25, N'56002300', N'AP AEROSOL GREENTEA (NCALOG) 300ML', N'9556076-003304', N'CR', 12, 12060.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (161, 25, N'56004300', N'AP AEROSOL P/FLOWER (NCALOG) 300ML', N'9556076-001447', N'CR', 12, 12060.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (162, 25, N'56003300', N'AP AEROSOL L/BREEZE (NCALOG) 300ML', N'9556076-005445', N'CR', 12, 12060.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (163, 25, N'56001300', N'AP AEROSOL CTRZ BRST (NCALOG) 300ML', N'9556076-001478', N'CR', 12, 12060.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (164, 27, N'4501014', N'PAMP NEW AB HIPPO NB 14S ', N'4902430-287876', N'CR', 12, 26925.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (165, 27, N'4502014', N'PAMP NEW AB HIPPO SMALL 14S ', N'4902430-287883', N'CR', 12, 26925.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (166, 27, N'4503012', N'PAMP NEW AB HIPPO MEDIUM 12S ', N'4 902430-287869', N'CR', 12, 26925.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (167, 27, N'4504010', N'PAMP NEW AB HIPPO LARGE 10S ', N'4 902430-287852', N'CR', 12, 26925.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (168, 27, N'4601109', N'PAMP ORANGE PANTS M9 ', N'4 902430-301350', N'CR', 12, 16425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (169, 27, N'4601208', N'PAMP ORANGE PANTS L8 ', N'4 902430-301367', N'CR', 12, 16425.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (170, 27, N'4602412', N'PAMP ORANGE TAPED S12 ', N'4 902430-305143', N'CR', 12, 16050.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (171, 27, N'4602424', N'PAMP ORANGE TAPED S24 ', N'4 902430-305228', N'CR', 12, 32126.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (172, 27, N'4602122', N'PAMP ORANGE TAPED M22 ', N'4 902430-305075', N'CR', 12, 32126.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (173, 27, N'4602220', N'PAMP ORANGE TAPED L20 ', N'4 902430-305051', N'CR', 12, 32126.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (174, 29, N'13011045', N'SECRET APDO CRM PLAT SHWR FRESH 45G ', N'20800713429', N'CR', 12, 21625.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (175, 30, N'13021045', N'OLD SPICE APDO CRM PURE SPORT 45G ', N'20800718332', N'CR', 12, 21625.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (176, 30, N'13022045', N'OLD SPICE APDO CRM FRESH 45G ', N'20800306355', N'CR', 12, 21625.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (177, 31, N'7021020', N'WHISPER REGULAR FLOW WING 20S ', N'4 902430-044264', N'CR', 12, 18250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (178, 31, N'7051016', N'WHISPER ULTRA HEAVY WING 16S ', N'4 015400-046776', N'CR', 12, 20975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (179, 32, N'570211000', N'DOWNY ANTI BACTERI BOTOL 1L', N'4902430358590', N'CR', 12, 26400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (180, 32, N'570111000', N'DOWNY SUNRISE FRESH BOTOL 1L', N'4902430966245', N'CR', 12, 25400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (181, 32, N'57061100', N'DOWNY GARDEN BLOOM BOTOL 1L', N'4902430452755', N'CR', 12, 25400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (182, 32, N'57051900', N'DOWNY INNOCENCE BOTTLE 900ML ', N'4 902430 371018', N'CR', 12, 25325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (183, 32, N'570511800', N'DOWNY INNOCENCE BOTTLE 1.800 ML ', N'4 902430 370936', N'CR', 12, 46100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (184, 32, N'57041900', N'DOWNY ATTRACTION BOTOL 900ML', N'4902430276504', N'CR', 12, 25325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (185, 32, N'57031900', N'DOWNY PASSION BOTOL 900ML', N'4902430276337', N'CR', 12, 25325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (186, 31, N'7041010', N'WHISPER ULTRA REGULAR WING  10S ', N'4 902430-495905', N'CR', 16, 10975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (187, 31, N'7041020', N'WHISPER ULTRA REGULAR WING  20S ', N'4 902430-492416', N'CR', 16, 21075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (188, 31, N'7051008', N'WHISPER ULTRA HEAVY WING 8S ', N'4 015400-046769', N'CR', 16, 10975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (189, 31, N'7111008', N'WHISPER COTTON CLEAN REG FLOW-WING 8s', N'4 902430-349864', N'CR', 16, 6525.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (190, 31, N'7121008', N'WHISPER COTTON CLEAN REG NON-WING 8s', N'4 902430-349918', N'CR', 16, 5325.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (191, 31, N'7121001', N'WHISPER COTTON CLEAN REG FLOW 20s', N'4 902430-349925', N'CR', 16, 12601.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (192, 32, N'57021400', N'DOWNY ANTI BACTERI BOTOL 400ML', N'4902430302487', N'CR', 20, 11525.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (193, 32, N'57011400', N'DOWNY SUNRISE FRESH BOTOL 400ML', N'4902430963251', N'CR', 20, 10875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (194, 32, N'57061400', N'DOWNY GARDEN BLOOM BOTOL 400ML', N'4902430453189', N'CR', 20, 10875.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (195, 32, N'57051370', N'DOWNY INNOCENCE BOTOL 370ML', N'4902430371025', N'CR', 20, 10900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (196, 32, N'57041370', N'DOWNY ATTRACTION BOTOL 370ML', N'4902430276474', N'CR', 20, 10900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (197, 32, N'57031370', N'DOWNY PASSION BOTOL 370ML', N'4902430276429', N'CR', 20, 10900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (198, 1, N'3061050', N'OLAY WHITE RADIANCE INTENSIVE CREAM 50GR ', N'4902430-232227', N'CR', 24, 53900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (199, 1, N'3062075', N'OLAY WHITE RADIANCE LOTION 75ML ', N'4902430-232241', N'CR', 24, 43350.0000)
GO
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (200, 1, N'3061100', N'OLAY WHITE RADIANCE CREAM 100GR ', N'4902430-080750', N'CR', 24, 103150.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (201, 1, N'3063050', N'OLAY WHITE RADIANCE NIGHT 50GR ', N'4902430-989992', N'CR', 24, 58500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (202, 1, N'3171040', N'OLAY NATURAL WHITE LIGHT 40G ', N'4902430-284158', N'CR', 24, 23200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (203, 1, N'3173050', N'OLAY NATURAL WHITE DAY CREAM 50G ', N'4902430-374248', N'CR', 24, 42125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (204, 1, N'3172050', N'OLAY NATURAL WHITE NIGHT CREAM 50G ', N'4902430-374293', N'CR', 24, 42125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (205, 4, N'11091160', N'HERBAL SHP HELLO HYDRATION 160ML ', N'4902430-163910', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (206, 5, N'11111160', N'HE SHP NO FLAKIN WAY 160ML ', N'4902430-163989', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (207, 5, N'11101160', N'HE SHP DANGEROUSLY STRAIGHT 160ML ', N'4902430-163866', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (208, 5, N'11102160', N'HE COND DANGEROUSLY STRAIGHT 160ML ', N'4902430-163873', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (209, 5, N'11131160', N'HE SHP COLOUR MY SHINY 160ML ', N'4902430-163958', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (210, 5, N'11132160', N'HE COND COLOUR MY SHINY 160ML ', N'4902430-163965', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (211, 5, N'11151160', N'HE ESS SHP SMOOTH LOVIN 160ML ', N'4902430-268318', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (212, 5, N'11152160', N'HE ESS COND SMOOTH LOVIN 160ML ', N'4902430-268332', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (213, 5, N'11121160', N'HE SHP BREAKS OVER 160ML ', N'4902430-163897', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (214, 5, N'11122160', N'HE COND BREAKS OVER 160ML ', N'4902430-163880', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (215, 5, N'11141160', N'HE ESS SHP LONG TERM RELATIONSHIP 160ML ', N'4902430-219983', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (216, 5, N'11142160', N'HE ESS COND LONG TERM RELATIONSHIP 160ML ', N'4902430-219976', N'CR', 24, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (217, 7, N'2221180', N'H&S SHP COOL MENTHOL 180ML', N'4902430-102247', N'CR', 24, 15850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (218, 7, N'2231180', N'H&S AD SHP CLEAN & BALANCE 180ML ', N'4902430-102254', N'CR', 24, 15850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (219, 7, N'2211180', N'H&S SHP SMOOTH SILKY 180ML', N'4902430-102230', N'CR', 24, 15850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (220, 7, N'2271180', N'H&S SHP ANTI HAIRFALL 180ML', N'4902430-142083', N'CR', 24, 15850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (221, 7, N'2311180', N'H&S SHP ITCHY SCALP CARE 180 ML', N'4902430-482783', N'CR', 24, 15850.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (222, 9, N'5741170', N'PAN SHP HAIR FALL CONTROL 170ML ', N'4902430-400886', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (223, 9, N'5743165', N'PAN COND HAIR FALL CONTROL 165ML ', N'4902430-40978', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (224, 9, N'5711170', N'PAN SHP ANTI DANDRUFF 170ML ', N'4902430-400947', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (225, 9, N'5751170', N'PAN SHP TOTAL CARE 170ML ', N'4902430-400602', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (226, 9, N'5753165', N'PAN COND TOTAL CARE 165ML ', N'4902430-399425', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (227, 9, N'57821170', N'PAN SHP NC Smoothness Life  170ML', N'4902430-451512', N'CR', 24, 18101.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (228, 9, N'57843165', N'PAN COND NC Smoothness Life 165ML', N'4902430-451529', N'CR', 24, 18101.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (229, 9, N'57844135', N'PAN TRT JAR NC Smoothness Life 135ML', N'4902430-452335', N'CR', 24, 25775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (230, 9, N'57811170', N'PAN SHP NC Fullness & Life 170ML', N'4902430-400909', N'CR', 24, 18101.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (231, 9, N'57833165', N'PAN COND NC Fullness & Life 165ML', N'4902430-400985', N'CR', 24, 18101.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (232, 9, N'57834135', N'PAN TRT JAR NC Fullness & Life 135ML', N'4902430-390781', N'CR', 24, 25775.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (233, 9, N'5721170', N'PAN SHP SMOOTH SILKY 170ML ', N'4902430-400879', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (234, 9, N'5723165', N'PAN COND SMOOTH SILKY 165ML ', N'4902430-400961', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (235, 9, N'5731170', N'PAN SHP LONG BLACK 170ML ', N'4902430-418300', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (236, 9, N'5761170', N'PAN SHP NOURISHED SHINE 170ML ', N'4902430-399371', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (237, 9, N'5763165', N'PAN COND NOURISHED SHINE 165 ML ', N'4902430-399470', N'CR', 24, 16750.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (238, 12, N'6219170', N'REJ SHP RICH 170ML', N'4 902430-428316', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (239, 12, N'6229170', N'REJ COND RICH 170ML', N'4 902430-249799', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (240, 12, N'6211170', N'REJ SHP SOFT AND SMOOTH 170ML', N'4 902430-432269', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (241, 12, N'6221170', N'REJ COND SOFT AND SMOOTH 170ML', N'4 902430-249782', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (242, 12, N'6213170', N'REJ SHP FRUITY AD 170ML', N'4 902430-428439', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (243, 12, N'6215170', N'REJ SHP ANTI HARI FALL 170ML', N'4 902430-428415', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (244, 12, N'6225170', N'REJ COND ANTI HAIR FALL 170ML', N'4 902430-249737', N'CR', 24, 13701.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (245, 12, N'6216170', N'REJ SHP ANTI FRIZZ 170ML', N'4 902430-429368', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (246, 12, N'6226170', N'REJ COND ANTI FRIZZ 170ML', N'4 902430-252621', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (247, 12, N'6217170', N'REJ SHP 3 IN 1 170ML', N'4 902430-429375', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (248, 12, N'6227170', N'REJ COND 3 IN 1 170ML', N'4 902430-273695', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (249, 12, N'6218170', N'REJ SHP MANAGEABLE BLACK 170ML', N'4 902430-428422', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (250, 12, N'6228170', N'REJ COND MANAGEABLE BLACK 170ML', N'4 902430-428347', N'CR', 24, 14075.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (251, 11, N'6182100', N'REJOICE TRT COMBING CREAM 100ML ', N'4 902430-279161', N'CR', 24, 13725.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (252, 27, N'4602110', N'PAMP ORANGE TAPED M10 ', N'4 902430-305129', N'CR', 24, 16050.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (253, 27, N'4602209', N'PAMP ORANGE TAPED L9 ', N'4 902430-305105', N'CR', 24, 16050.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (254, 29, N'13011014', N'SECRET APDO CRM PLAT SHWR FRESH 14G ', N'20800712705', N'CR', 24, 10225.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (255, 30, N'13022014', N'OLD SPICE APDO CRM FRESH 14G ', N'20800306348', N'CR', 24, 10225.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (256, 30, N'13021014', N'OLD SPICE APDO CRM PURE SPORT 14G ', N'20800718349', N'CR', 24, 10225.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (257, 31, N'7021010', N'WHISPER REGULAR FLOW WINGS 10S ', N'4 902430-044257', N'CR', 24, 9576.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (258, 31, N'7031008', N'WHISPER HEAVY FLOW & OVERNIGHT 8S ', N'4 902430-044233', N'CR', 24, 9576.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (259, 32, N'57023400', N'DOWNY ANTI BACTERI REFILL 400ML', N'4902430-302500', N'CR', 24, 10000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (260, 32, N'57013400', N'DOWNY SUNRISE FRESH REFILL 400ML', N'4902430-972260', N'CR', 24, 9400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (261, 32, N'57063400', N'DOWNY GARDEN BLOOM REFILL 400ML', N'4902430-452724', N'CR', 24, 9400.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (262, 32, N'57053370', N'DOWNY INNOCENCE REFILL 370ML', N'4902430-370950', N'CR', 24, 9800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (263, 32, N'57033370', N'DOWNY ATTRACTION REFILL 370ML', N'4902430-276481', N'CR', 24, 9800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (264, 32, N'57043370', N'DOWNY PASSION REFILL 370ML', N'4902430-276436', N'CR', 24, 9800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (265, 6, N'2221005', N'HS SHP COOL MENTHOL 5ML', N'4902430-963350', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (266, 7, N'2231005', N'H&S SHP CLEAN BALANCED 5ML', N'4902430-963343', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (267, 7, N'2211005', N'H&S SHP SMOOTH SILKY 5ML', N'4902430-963367', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (268, 7, N'2271005', N'H&S SHP ANTI HAIRFALL 5ML ', N'4902430-142410', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (269, 7, N'2311005', N'H&S SHP ITCHY SCALP CARE 5 ML', N'4902430-477185', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (270, 9, N'5741005', N'PAN SHP HAIR FALL CONTROL 5ML ', N'4902430-416542', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (271, 9, N'5743005', N'PAN COND HAIRFALL CONTROL 5ML ', N'4902430-415729', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (272, 9, N'5751005', N'PAN SHP TOTAL CARE 5ML ', N'4902430-415873', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (273, 9, N'5753005', N'PAN COND TOTAL CARE 5ML ', N'4902430-415743', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (274, 9, N'5721005', N'PAN SHP SMOOTH SILKY 5ML ', N'4902430-41536', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (275, 9, N'5723005', N'PAN COND SMOOTH SILKY 5ML ', N'4902430-415736', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (276, 9, N'5731005', N'PAN SHP LONG BLACK 5ML ', N'4902430-415958', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (277, 9, N'5733005', N'PAN COND LONG BLACK 5ML ', N'4902430-415750', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (278, 9, N'5761005', N'PAN SHP NOURISHED SHINE 5ML ', N'4902430-264860', N'CR', 40, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (279, 11, N'6219006', N'REJOICE RICH SHP 6 ML', N'4 902430-428460', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (280, 11, N'6033005', N'REJOICE COND RICH 5ML ', N'4 902430-252607', N'CR', 40, 3950.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (281, 12, N'6211006', N'REJ SHP SOFT AND SMOOTH 6 ML', N'4 902430-432603', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (282, 11, N'6213006', N'REJOICE SHP FRUITY AD 6 ML ', N'4 902430-458788', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (283, 11, N'6215006', N'REJOICE SHP ANTI HAIRFALL 6 ML ', N'4 902430-468930', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (284, 11, N'6216006', N'REJOICE SHP ANTI FRIZZ 6 ML ', N'4 902430-458764', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (285, 11, N'6217004', N'REJOICE SHP 3 IN 1 4ML', N'4 902430-428514', N'CR', 40, 2475.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (286, 13, N'6191006', N'RJC SHP MANAGEBLE BLACK 6 ML ', N'4 902430-458757', N'CR', 40, 4200.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (287, 11, N'6182004', N'REJOICE TREATMENT COMBING CREAM 4ML ', N'4 902430-279307', N'CR', 40, 3950.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (288, 1, N'3130018', N'OLAY AGE PROTEC CREAM 18G', N'4902430-332163', N'CR', 48, 16675.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (289, 1, N'3140030', N'OLAY AGE PROTEC LOTION 30G', N'4902430-332170', N'CR', 48, 25025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (290, 1, N'3062030', N'OLAY WHITE RADIANCE LOTION 30ML ', N'4902430-232265', N'CR', 48, 18500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (291, 1, N'3171020', N'OLAY NATURAL WHITE LIGHT 20GR ', N'4902430-284134', N'CR', 48, 10925.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (292, 1, N'3174030', N'OLAY NATURAL WHITE LOTION 30ML ', N'4902430-374279', N'CR', 48, 18525.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (293, 5, N'11092160', N'HE COND HELLO HYDRATION 160ML ', N'4902430-163927', N'CR', 48, 16250.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (294, 5, N'11092300', N'HE COND HELLO HYDRATION 300ML ', N'4902430-163743', N'CR', 48, 30025.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (295, 7, N'2221075', N'H&S SHP COOL MENTHOL 75ML', N'4902430-414296', N'CR', 48, 6975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (296, 7, N'2231075', N'H&S SHP CLEAN BALANCED 75ML', N'4902430-414289', N'CR', 48, 6975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (297, 7, N'2211075', N'H&S SHP SMOOTH SILKY 75ML', N'4902430-433167', N'CR', 48, 6975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (298, 7, N'2271075', N'H&S SHP ANTI HAIRFALL 75ML', N'4902430-433082', N'CR', 48, 6975.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (299, 7, N'2311075', N'H&S SHP ITCHY SCALP CARE 75 ML', N'4902430-477161', N'CR', 48, 6975.0000)
GO
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (300, 9, N'5741070', N'PAN SHP HAIR FALL CONTROL 70ML ', N'4903430-401142', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (301, 9, N'5743075', N'PAN COND HAIR FALL CONTROL 75ML ', N'4902430-400664', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (302, 9, N'5711070', N'PAN SHP ANTI DANDRUFF 70ML ', N'4902430-401173', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (303, 9, N'5751070', N'PAN SHP TOTAL CARE 70ML ', N'4902430-399333', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (304, 9, N'5753075', N'PAN COND TOTAL CARE 75ML ', N'4902430-399418', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (305, 9, N'57841070', N'PAN SHP NC Smoothness Life 70ML', N'4902430-451475', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (306, 9, N'57843075', N'PAN COND NC Smoothness Life 75ML', N'4902430-451499', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (307, 9, N'57831070', N'PAN SHP NC Fullness & Life 70ML', N'4902430-401159', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (308, 9, N'57833075', N'PAN COND NC Fullness & Life 75ML', N'4902430-400862', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (309, 9, N'5721070', N'PAN SHP SMOOTH SILKY 70ML ', N'4902430-399531', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (310, 9, N'5723075', N'PAN COND SMOOTH SILKY 75ML ', N'4902430-400640', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (311, 9, N'5731070', N'PAN SHP LONG BLACK 70ML ', N'4902430-419017', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (312, 9, N'5733075', N'PAN COND LONG BLACK 75ML ', N'4902430-414944', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (313, 9, N'5761070', N'PAN SHP NOURISHED SHINE 70ML ', N'4902430-399364', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (314, 9, N'5763075', N'PAN COND NOURISHED SHINE 75ML ', N'4902430-399456', N'CR', 48, 6125.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (315, 12, N'6219070', N'REJ SHP RICH 70ML', N'4 902430-429405', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (316, 12, N'6229070', N'REJ COND RICH 70ML', N'4 902430-428408', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (317, 12, N'6211070', N'REJ SHP SOFT AND SMOOTH 70ML', N'4 902430-432245', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (318, 12, N'6221070', N'REJ COND SOFT AND SMOOTH 70ML', N'4 902430-432252', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (319, 12, N'6213070', N'REJ SHP FRUITY AD 70ML', N'4 902430-428378', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (320, 12, N'6215070', N'REJ SHP ANTI HAIR FALL 70ML', N'4 902430-428361', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (321, 12, N'6225070', N'REJ COND ANTI HAIR FALL 70ML', N'4 902430-428392', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (322, 12, N'6216070', N'REJ SHP ANTI FRIZZ 70ML', N'4 902430-428354', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (323, 12, N'6217070', N'REJ SHP 3 IN 1 70ML', N'4 902430-429399', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (324, 12, N'6227070', N'REJ COND 3 IN 1 70ML', N'4 902430-', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (325, 12, N'6218070', N'REJ SHP MANAGEABLE BLACK 70ML', N'4 902430-428385', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (326, 12, N'6228070', N'REJ COND MANAGEABLE BLACK 70ML', N'4 902430-428118', N'CR', 48, 5100.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (327, 11, N'6182040', N'REJOICE TRT COMBING CREAM 40ML ', N'4 902430-279062', N'CR', 48, 6001.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (328, 32, N'57023200', N'DOWNY ANTI BACTERI REFILL 200ML', N'4902430-302494', N'CR', 48, 5300.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (329, 32, N'57013200', N'DOWNY SUNRISE FRESH REFILL 200ML', N'4902430-302449', N'CR', 48, 5000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (330, 32, N'57063200', N'DOWNY GARDEN BLOOM REFILL 200ML', N'4902430-452786', N'CR', 48, 5000.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (331, 36, N'21021100', N'FORMULA 44 DEWASA 100ml ', N'4 987176-006905', N'CR', 48, 12575.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (332, 3, N'8113135', N'SAFEGUARD GREEN 135G ', N'4902430-935593', N'CR', 72, 4800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (333, 3, N'8111135', N'SAFEGUARD WHITE 135G ', N'4902430-934800', N'CR', 72, 4800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (334, 3, N'8112135', N'SAFEGUARD PINK 135G ', N'4902430-935616', N'CR', 72, 4800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (335, 3, N'8114135', N'SAFEGUARD MENTHOL 135G ', N'4902430-935647', N'CR', 72, 4800.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (336, 36, N'21113054', N'FORMULA 44 ANAK STRAWBERRY 54ML ', N'4 987176-006943', N'CR', 72, 9825.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (337, 36, N'21121054', N'FORMULA 44 DEWASA 54ML ', N'4 987176-006882', N'CR', 72, 9825.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (338, 36, N'21031054', N'FORMULA 44 CGCD SYRUP DT 54ml ', N'4 987176-006929', N'CR', 72, 8900.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (339, 9, N'5711005', N'PAN SHP ANTI DANDRUFF 5ML ', N'4902430-416023', N'CR', 80, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (340, 2, N'8031090', N'CAMAY CHIC BLACK 90gr ', N'4902430-284288', N'CR', 96, 2725.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (341, 2, N'8011090', N'CAMAY NATURAL WHITE 90GR ', N'4902430-284295', N'CR', 96, 2725.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (342, 2, N'8021090', N'CAMAY CLASSIC PINK 90GR ', N'4902430-284646', N'CR', 96, 2725.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (343, 3, N'8111090', N'SAFEGUARD WHITE 90G ', N'4902430-495073', N'CR', 96, 3500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (344, 3, N'8112090', N'SAFEGUARD PINK 90G ', N'4902430-495141', N'CR', 96, 3500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (345, 3, N'8113090', N'SAFEGUARD GREEN 90G ', N'4902430-495103', N'CR', 96, 3500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (346, 3, N'8114090', N'SAFEGUARD MENTHOL 90G ', N'4902430-073899', N'CR', 96, 3500.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (347, 36, N'21113027', N'FORMULA 44 ANAK STRAWBERRY 27ML ', N'4 987176-006411', N'CR', 108, 5375.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (348, 36, N'21121027', N'FORMULA 44 DEWASA 27ml ', N'4 987176-002679', N'CR', 108, 5375.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (349, 36, N'21031027', N'FORMULA 44 CGCD SYRUP DT 27ml ', N'4 987176-002730', N'CR', 108, 4450.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (350, 27, N'4601101', N'PAMP ORANGE PANTS M1 ', N'4 902430-300995', N'CR', 120, 1700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (351, 27, N'4601201', N'PAMP ORANGE PANTS L1 ', N'4 902430-301008', N'CR', 120, 1700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (352, 37, N'24011050', N'VICKS VAPORUB 50gr ', N'4 987176-000552', N'CR', 144, 17350.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (353, 37, N'22011006', N'VICKS INHALER 6S ', N'4 902430-393126', N'CR', 288, 8700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (354, 37, N'24011025', N'VICKS VAPORUB 25GR ', N'4 987176 008718', N'CR', 288, 9700.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (355, 9, N'57841005', N'PAN SHP NC Smoothness Life 5ML', N'4902430-451451', N'CR', 480, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (356, 9, N'57843005', N'PAN COND NC Smoothness Life 5ML', N'4902430-451086', N'CR', 480, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (357, 9, N'57831005', N'PAN SHP NC Fullness & Life 5ML', N'4902430-415934', N'CR', 480, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (358, 9, N'57833005', N'PAN COND NC Fullness & Life 5ML', N'4902430-415767', N'CR', 480, 4657.0000)
INSERT [dbo].[Product] ([Id], [BrandId], [Code], [Name], [Barcode], [Unit], [ItemPerUnit], [PricePerUnit]) VALUES (359, 37, N'24011010', N'VICKS VAPORUB 10gr ', N'4 987176-600554', N'CR', 864, 4800.0000)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[ProductDiscounts] ON 

INSERT [dbo].[ProductDiscounts] ([Id], [ProductId], [DiscountA], [DiscountB], [Quantity]) VALUES (1, 1, 2, NULL, NULL)
INSERT [dbo].[ProductDiscounts] ([Id], [ProductId], [DiscountA], [DiscountB], [Quantity]) VALUES (2, 13, 3, 2, 3)
SET IDENTITY_INSERT [dbo].[ProductDiscounts] OFF
SET IDENTITY_INSERT [dbo].[Sale] ON 

INSERT [dbo].[Sale] ([Id], [SaleDate], [StoreCode], [SellerCode]) VALUES (2, CAST(0x0000A1B400FF6090 AS DateTime), N'T00000001', N'SLX001')
INSERT [dbo].[Sale] ([Id], [SaleDate], [StoreCode], [SellerCode]) VALUES (10003, CAST(0x0000A1BB00000000 AS DateTime), N'T00000001', N'SLX001')
INSERT [dbo].[Sale] ([Id], [SaleDate], [StoreCode], [SellerCode]) VALUES (10004, CAST(0x0000A1BC00000000 AS DateTime), N'G00000001', N'SLX001')
INSERT [dbo].[Sale] ([Id], [SaleDate], [StoreCode], [SellerCode]) VALUES (10005, CAST(0x0000A1BB00B47350 AS DateTime), N'G00000001', N'SLX001')
INSERT [dbo].[Sale] ([Id], [SaleDate], [StoreCode], [SellerCode]) VALUES (20005, CAST(0x0000A1BD00000000 AS DateTime), N'T00000001', N'SLX001')
SET IDENTITY_INSERT [dbo].[Sale] OFF
SET IDENTITY_INSERT [dbo].[SaleDetails] ON 

INSERT [dbo].[SaleDetails] ([Id], [SaleId], [ProductId], [DiscountA], [DiscountB], [DiscountSales], [GrossPrice], [Quantity]) VALUES (10007, 2, 1, 2, 0, 2.75, 155425.0000, 2)
INSERT [dbo].[SaleDetails] ([Id], [SaleId], [ProductId], [DiscountA], [DiscountB], [DiscountSales], [GrossPrice], [Quantity]) VALUES (10008, 2, 13, 3, 2, 2.75, 103575.0000, 7)
INSERT [dbo].[SaleDetails] ([Id], [SaleId], [ProductId], [DiscountA], [DiscountB], [DiscountSales], [GrossPrice], [Quantity]) VALUES (10009, 10005, 1, 2, 0, 2.75, 155425.0000, 7)
INSERT [dbo].[SaleDetails] ([Id], [SaleId], [ProductId], [DiscountA], [DiscountB], [DiscountSales], [GrossPrice], [Quantity]) VALUES (10010, 10005, 13, 3, 2, 2.75, 103575.0000, 3)
SET IDENTITY_INSERT [dbo].[SaleDetails] OFF
INSERT [dbo].[Seller] ([Code], [Name], [Address], [Telephone]) VALUES (N'SLX001', N'Seller 1', N'Jl.jalan', N'0983489348')
INSERT [dbo].[Seller] ([Code], [Name], [Address], [Telephone]) VALUES (N'SLX002', N'Seller 2', N'Jl.Jalan medan', N'08646734664')
INSERT [dbo].[Stores] ([Code], [Name], [Address], [Telephone], [SellerCode]) VALUES (N'COBA', N'fery', N'coba', N'98347578', N'SLX001')
INSERT [dbo].[Stores] ([Code], [Name], [Address], [Telephone], [SellerCode]) VALUES (N'G00000001', N'gj', N'kfjgkf', N'jkdjkd', N'SLX001')
INSERT [dbo].[Stores] ([Code], [Name], [Address], [Telephone], [SellerCode]) VALUES (N'T00000001', N'Test', N'jln', N'lsfsf', N'SLX001')
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([Id], [Name], [Address], [PhoneNumber]) VALUES (1, N'Suplier satu', N'jl.jalan', N'09834589858')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
ALTER TABLE [dbo].[SaleDetails] ADD  CONSTRAINT [DF_SaleDetail_Discount]  DEFAULT ((1)) FOR [DiscountA]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Categori] FOREIGN KEY([BrandId])
REFERENCES [dbo].[Brand] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Categori]
GO
ALTER TABLE [dbo].[ProductDiscounts]  WITH CHECK ADD  CONSTRAINT [FK_ProductDiscounts_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[ProductDiscounts] CHECK CONSTRAINT [FK_ProductDiscounts_Product]
GO
ALTER TABLE [dbo].[Purchase]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[Purchase] CHECK CONSTRAINT [FK_Purchase_Supplier]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetail_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [FK_PurchaseDetail_Product]
GO
ALTER TABLE [dbo].[PurchaseDetail]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseDetail_Purchase] FOREIGN KEY([PurchaseId])
REFERENCES [dbo].[Purchase] ([Id])
GO
ALTER TABLE [dbo].[PurchaseDetail] CHECK CONSTRAINT [FK_PurchaseDetail_Purchase]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sale_Seller1] FOREIGN KEY([SellerCode])
REFERENCES [dbo].[Seller] ([Code])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sale_Seller1]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sale_Stores] FOREIGN KEY([StoreCode])
REFERENCES [dbo].[Stores] ([Code])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sale_Stores]
GO
ALTER TABLE [dbo].[SaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_SaleDetail_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[SaleDetails] CHECK CONSTRAINT [FK_SaleDetail_Product]
GO
ALTER TABLE [dbo].[SaleDetails]  WITH CHECK ADD  CONSTRAINT [FK_SaleDetail_Sale] FOREIGN KEY([SaleId])
REFERENCES [dbo].[Sale] ([Id])
GO
ALTER TABLE [dbo].[SaleDetails] CHECK CONSTRAINT [FK_SaleDetail_Sale]
GO
ALTER TABLE [dbo].[Stores]  WITH CHECK ADD  CONSTRAINT [FK_Stores_Seller] FOREIGN KEY([SellerCode])
REFERENCES [dbo].[Seller] ([Code])
GO
ALTER TABLE [dbo].[Stores] CHECK CONSTRAINT [FK_Stores_Seller]
GO
USE [master]
GO
ALTER DATABASE [pos] SET  READ_WRITE 
GO
