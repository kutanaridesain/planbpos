﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Suplier : UserControl
    {
        public Suplier()
        {
            InitializeComponent();
            supplierTableAdapter.Fill(posDataSet.Supplier);
        }

        private void supplierBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            supplierBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(posDataSet);
            MessageBox.Show("Proses berhasil!");
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            ActiveControl = nameTextEdit;
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            supplierTableAdapter.Update(posDataSet.Supplier);
        }
    }
}
