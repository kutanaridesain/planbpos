﻿namespace trgpos.Fragments
{
    partial class Sales
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label poNumberLabel2;
            System.Windows.Forms.Label grossPriceLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label poNumberLabel;
            System.Windows.Forms.Label saleDateLabel;
            System.Windows.Forms.Label storeCodeLabel;
            System.Windows.Forms.Label sellerCodeLabel;
            System.Windows.Forms.Label discountALabel1;
            System.Windows.Forms.Label discountBLabel;
            System.Windows.Forms.Label discountSalesLabel;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sales));
            this.posDataSet = new trgpos.posDataSet();
            this.saleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.saleTableAdapter = new trgpos.posDataSetTableAdapters.SaleTableAdapter();
            this.tableAdapterManager = new trgpos.posDataSetTableAdapters.TableAdapterManager();
            this.sellerTableAdapter = new trgpos.posDataSetTableAdapters.SellerTableAdapter();
            this.storesTableAdapter = new trgpos.posDataSetTableAdapters.StoresTableAdapter();
            this.saleDetailsTableAdapter = new trgpos.posDataSetTableAdapters.SaleDetailsTableAdapter();
            this.saleBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saleBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.poToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.storesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sellerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.sellerCodeComboBox = new System.Windows.Forms.ComboBox();
            this.storeCodeComboBox = new System.Windows.Forms.ComboBox();
            this.saleDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.poNumberTextBox = new System.Windows.Forms.TextBox();
            this.saleGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellerCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.TxtItemQuantity = new System.Windows.Forms.TextBox();
            this.TxtUnitQuantity = new System.Windows.Forms.TextBox();
            this.TxtDiscountSales = new System.Windows.Forms.TextBox();
            this.saleDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtDiscountB = new System.Windows.Forms.TextBox();
            this.TxtDiscountA = new System.Windows.Forms.TextBox();
            this.LblTotal = new System.Windows.Forms.Label();
            this.CbProductName = new System.Windows.Forms.ComboBox();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TxtQuantity = new DevExpress.XtraEditors.SpinEdit();
            this.TxtGrossPrice = new DevExpress.XtraEditors.SpinEdit();
            this.CbProduct = new System.Windows.Forms.ComboBox();
            this.TxtPoNumber = new System.Windows.Forms.TextBox();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorSaveItem1 = new System.Windows.Forms.ToolStripButton();
            this.PrintButton = new System.Windows.Forms.ToolStripButton();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.idTextBox1 = new System.Windows.Forms.TextBox();
            this.saleDetailsGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountSales = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tableAdapterManagerSaleDetail = new trgpos.posDataSetTableAdapters.TableAdapterManager();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.productTableAdapter = new trgpos.posDataSetTableAdapters.ProductTableAdapter();
            this.productDiscountsTableAdapter = new trgpos.posDataSetTableAdapters.ProductDiscountsTableAdapter();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.salesViewReportTableAdapter = new trgpos.posDataSetTableAdapters.SalesViewReportTableAdapter();
            poNumberLabel2 = new System.Windows.Forms.Label();
            grossPriceLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            poNumberLabel = new System.Windows.Forms.Label();
            saleDateLabel = new System.Windows.Forms.Label();
            storeCodeLabel = new System.Windows.Forms.Label();
            sellerCodeLabel = new System.Windows.Forms.Label();
            discountALabel1 = new System.Windows.Forms.Label();
            discountBLabel = new System.Windows.Forms.Label();
            discountSalesLabel = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleBindingNavigator)).BeginInit();
            this.saleBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.storesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saleGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saleDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrossPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saleDetailsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // poNumberLabel2
            // 
            poNumberLabel2.AutoSize = true;
            poNumberLabel2.Location = new System.Drawing.Point(24, 39);
            poNumberLabel2.Name = "poNumberLabel2";
            poNumberLabel2.Size = new System.Drawing.Size(65, 13);
            poNumberLabel2.TabIndex = 1;
            poNumberLabel2.Text = "PO Number:";
            // 
            // grossPriceLabel
            // 
            grossPriceLabel.AutoSize = true;
            grossPriceLabel.Location = new System.Drawing.Point(539, 63);
            grossPriceLabel.Name = "grossPriceLabel";
            grossPriceLabel.Size = new System.Drawing.Size(64, 13);
            grossPriceLabel.TabIndex = 11;
            grossPriceLabel.Text = "Gross Price:";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(539, 89);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(26, 13);
            quantityLabel.TabIndex = 13;
            quantityLabel.Text = "Unit";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(24, 67);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(47, 13);
            productNameLabel.TabIndex = 15;
            productNameLabel.Text = "Product:";
            // 
            // poNumberLabel
            // 
            poNumberLabel.AutoSize = true;
            poNumberLabel.Location = new System.Drawing.Point(8, 43);
            poNumberLabel.Name = "poNumberLabel";
            poNumberLabel.Size = new System.Drawing.Size(65, 13);
            poNumberLabel.TabIndex = 1;
            poNumberLabel.Text = "PO Number:";
            // 
            // saleDateLabel
            // 
            saleDateLabel.AutoSize = true;
            saleDateLabel.Location = new System.Drawing.Point(7, 68);
            saleDateLabel.Name = "saleDateLabel";
            saleDateLabel.Size = new System.Drawing.Size(57, 13);
            saleDateLabel.TabIndex = 3;
            saleDateLabel.Text = "Sale Date:";
            // 
            // storeCodeLabel
            // 
            storeCodeLabel.AutoSize = true;
            storeCodeLabel.Location = new System.Drawing.Point(7, 95);
            storeCodeLabel.Name = "storeCodeLabel";
            storeCodeLabel.Size = new System.Drawing.Size(35, 13);
            storeCodeLabel.TabIndex = 5;
            storeCodeLabel.Text = "Store:";
            // 
            // sellerCodeLabel
            // 
            sellerCodeLabel.AutoSize = true;
            sellerCodeLabel.Location = new System.Drawing.Point(7, 122);
            sellerCodeLabel.Name = "sellerCodeLabel";
            sellerCodeLabel.Size = new System.Drawing.Size(36, 13);
            sellerCodeLabel.TabIndex = 7;
            sellerCodeLabel.Text = "Seller:";
            // 
            // discountALabel1
            // 
            discountALabel1.AutoSize = true;
            discountALabel1.Location = new System.Drawing.Point(24, 89);
            discountALabel1.Name = "discountALabel1";
            discountALabel1.Size = new System.Drawing.Size(62, 13);
            discountALabel1.TabIndex = 20;
            discountALabel1.Text = "Discount A:";
            // 
            // discountBLabel
            // 
            discountBLabel.AutoSize = true;
            discountBLabel.Location = new System.Drawing.Point(24, 115);
            discountBLabel.Name = "discountBLabel";
            discountBLabel.Size = new System.Drawing.Size(62, 13);
            discountBLabel.TabIndex = 21;
            discountBLabel.Text = "Discount B:";
            // 
            // discountSalesLabel
            // 
            discountSalesLabel.AutoSize = true;
            discountSalesLabel.Location = new System.Drawing.Point(539, 35);
            discountSalesLabel.Name = "discountSalesLabel";
            discountSalesLabel.Size = new System.Drawing.Size(81, 13);
            discountSalesLabel.TabIndex = 22;
            discountSalesLabel.Text = "Discount Sales:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(539, 116);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(39, 13);
            label2.TabIndex = 26;
            label2.Text = "Pieces";
            // 
            // posDataSet
            // 
            this.posDataSet.DataSetName = "posDataSet";
            this.posDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // saleBindingSource
            // 
            this.saleBindingSource.DataMember = "Sale";
            this.saleBindingSource.DataSource = this.posDataSet;
            // 
            // saleTableAdapter
            // 
            this.saleTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BrandTableAdapter = null;
            this.tableAdapterManager.ProductDiscountsTableAdapter = null;
            this.tableAdapterManager.ProductTableAdapter = null;
            this.tableAdapterManager.PurchaseDetailTableAdapter = null;
            this.tableAdapterManager.PurchaseTableAdapter = null;
            this.tableAdapterManager.SaleDetailsTableAdapter = null;
            this.tableAdapterManager.SaleTableAdapter = this.saleTableAdapter;
            this.tableAdapterManager.SellerTableAdapter = this.sellerTableAdapter;
            this.tableAdapterManager.SpreadDetailsTableAdapter = null;
            this.tableAdapterManager.SpreadTableAdapter = null;
            this.tableAdapterManager.StoresTableAdapter = this.storesTableAdapter;
            this.tableAdapterManager.SupplierTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = trgpos.posDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserTableAdapter = null;
            // 
            // sellerTableAdapter
            // 
            this.sellerTableAdapter.ClearBeforeFill = true;
            // 
            // storesTableAdapter
            // 
            this.storesTableAdapter.ClearBeforeFill = true;
            // 
            // saleDetailsTableAdapter
            // 
            this.saleDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // saleBindingNavigator
            // 
            this.saleBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.saleBindingNavigator.BindingSource = this.saleBindingSource;
            this.saleBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.saleBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.saleBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.saleBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.saleBindingNavigatorSaveItem,
            this.toolStripLabel1,
            this.poToolStripTextBox,
            this.toolStripButton1,
            this.toolStripButton3});
            this.saleBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.saleBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.saleBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.saleBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.saleBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.saleBindingNavigator.Name = "saleBindingNavigator";
            this.saleBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.saleBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.saleBindingNavigator.Size = new System.Drawing.Size(527, 25);
            this.saleBindingNavigator.TabIndex = 0;
            this.saleBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 22);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // saleBindingNavigatorSaveItem
            // 
            this.saleBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saleBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("saleBindingNavigatorSaveItem.Image")));
            this.saleBindingNavigatorSaveItem.Name = "saleBindingNavigatorSaveItem";
            this.saleBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.saleBindingNavigatorSaveItem.Text = "Save Data";
            this.saleBindingNavigatorSaveItem.Click += new System.EventHandler(this.saleBindingNavigatorSaveItem_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(47, 22);
            this.toolStripLabel1.Text = "Cari PO";
            this.toolStripLabel1.Visible = false;
            // 
            // poToolStripTextBox
            // 
            this.poToolStripTextBox.Name = "poToolStripTextBox";
            this.poToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            this.poToolStripTextBox.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Visible = false;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // storesBindingSource
            // 
            this.storesBindingSource.DataMember = "Stores";
            this.storesBindingSource.DataSource = this.posDataSet;
            // 
            // sellerBindingSource
            // 
            this.sellerBindingSource.DataMember = "Seller";
            this.sellerBindingSource.DataSource = this.posDataSet;
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 30);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Padding = new System.Windows.Forms.Padding(5);
            this.splitContainerControl.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(1273, 475);
            this.splitContainerControl.SplitterPosition = 527;
            this.splitContainerControl.TabIndex = 11;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(sellerCodeLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.sellerCodeComboBox);
            this.splitContainerControl2.Panel1.Controls.Add(storeCodeLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.storeCodeComboBox);
            this.splitContainerControl2.Panel1.Controls.Add(saleDateLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.saleDateDateTimePicker);
            this.splitContainerControl2.Panel1.Controls.Add(poNumberLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.poNumberTextBox);
            this.splitContainerControl2.Panel1.Controls.Add(this.saleBindingNavigator);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.saleGridControl);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(527, 461);
            this.splitContainerControl2.SplitterPosition = 152;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // sellerCodeComboBox
            // 
            this.sellerCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.saleBindingSource, "SellerCode", true));
            this.sellerCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleBindingSource, "SellerName", true));
            this.sellerCodeComboBox.DataSource = this.sellerBindingSource;
            this.sellerCodeComboBox.DisplayMember = "Name";
            this.sellerCodeComboBox.FormattingEnabled = true;
            this.sellerCodeComboBox.Location = new System.Drawing.Point(96, 119);
            this.sellerCodeComboBox.Name = "sellerCodeComboBox";
            this.sellerCodeComboBox.Size = new System.Drawing.Size(121, 21);
            this.sellerCodeComboBox.TabIndex = 8;
            this.sellerCodeComboBox.ValueMember = "Code";
            // 
            // storeCodeComboBox
            // 
            this.storeCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.saleBindingSource, "StoreCode", true));
            this.storeCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleBindingSource, "StoreName", true));
            this.storeCodeComboBox.DataSource = this.storesBindingSource;
            this.storeCodeComboBox.DisplayMember = "Name";
            this.storeCodeComboBox.FormattingEnabled = true;
            this.storeCodeComboBox.Location = new System.Drawing.Point(96, 92);
            this.storeCodeComboBox.Name = "storeCodeComboBox";
            this.storeCodeComboBox.Size = new System.Drawing.Size(121, 21);
            this.storeCodeComboBox.TabIndex = 6;
            this.storeCodeComboBox.ValueMember = "Code";
            // 
            // saleDateDateTimePicker
            // 
            this.saleDateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.saleBindingSource, "SaleDate", true));
            this.saleDateDateTimePicker.Location = new System.Drawing.Point(96, 67);
            this.saleDateDateTimePicker.Name = "saleDateDateTimePicker";
            this.saleDateDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.saleDateDateTimePicker.TabIndex = 4;
            // 
            // poNumberTextBox
            // 
            this.poNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleBindingSource, "PoNumber", true));
            this.poNumberTextBox.Location = new System.Drawing.Point(96, 40);
            this.poNumberTextBox.Name = "poNumberTextBox";
            this.poNumberTextBox.ReadOnly = true;
            this.poNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.poNumberTextBox.TabIndex = 2;
            // 
            // saleGridControl
            // 
            this.saleGridControl.CausesValidation = false;
            this.saleGridControl.DataSource = this.saleBindingSource;
            this.saleGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saleGridControl.Location = new System.Drawing.Point(0, 0);
            this.saleGridControl.MainView = this.gridView2;
            this.saleGridControl.Margin = new System.Windows.Forms.Padding(0);
            this.saleGridControl.Name = "saleGridControl";
            this.saleGridControl.Size = new System.Drawing.Size(527, 304);
            this.saleGridControl.TabIndex = 0;
            this.saleGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView3});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoNumber,
            this.colSaleDate,
            this.colStoreName,
            this.colStoreCode,
            this.colSellerCode,
            this.colSellerName});
            this.gridView2.GridControl = this.saleGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoNumber, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView2_FocusedRowChanged);
            // 
            // colPoNumber
            // 
            this.colPoNumber.Caption = "PO Number";
            this.colPoNumber.FieldName = "PoNumber";
            this.colPoNumber.Name = "colPoNumber";
            this.colPoNumber.OptionsColumn.AllowEdit = false;
            this.colPoNumber.OptionsColumn.AllowFocus = false;
            this.colPoNumber.OptionsColumn.ReadOnly = true;
            this.colPoNumber.Visible = true;
            this.colPoNumber.VisibleIndex = 1;
            this.colPoNumber.Width = 326;
            // 
            // colSaleDate
            // 
            this.colSaleDate.Caption = "Date";
            this.colSaleDate.FieldName = "SaleDate";
            this.colSaleDate.Name = "colSaleDate";
            this.colSaleDate.OptionsColumn.AllowEdit = false;
            this.colSaleDate.OptionsColumn.AllowFocus = false;
            this.colSaleDate.OptionsColumn.ReadOnly = true;
            this.colSaleDate.Visible = true;
            this.colSaleDate.VisibleIndex = 0;
            this.colSaleDate.Width = 197;
            // 
            // colStoreName
            // 
            this.colStoreName.Caption = "Store Name";
            this.colStoreName.FieldName = "StoreName";
            this.colStoreName.Name = "colStoreName";
            this.colStoreName.OptionsColumn.AllowEdit = false;
            this.colStoreName.OptionsColumn.AllowFocus = false;
            this.colStoreName.OptionsColumn.ReadOnly = true;
            this.colStoreName.Visible = true;
            this.colStoreName.VisibleIndex = 3;
            this.colStoreName.Width = 331;
            // 
            // colStoreCode
            // 
            this.colStoreCode.Caption = "Store Code";
            this.colStoreCode.FieldName = "StoreCode";
            this.colStoreCode.Name = "colStoreCode";
            this.colStoreCode.OptionsColumn.AllowEdit = false;
            this.colStoreCode.OptionsColumn.AllowFocus = false;
            this.colStoreCode.OptionsColumn.ReadOnly = true;
            // 
            // colSellerCode
            // 
            this.colSellerCode.Caption = "Sales Code";
            this.colSellerCode.FieldName = "SellerCode";
            this.colSellerCode.Name = "colSellerCode";
            this.colSellerCode.OptionsColumn.AllowEdit = false;
            this.colSellerCode.OptionsColumn.AllowFocus = false;
            this.colSellerCode.OptionsColumn.ReadOnly = true;
            // 
            // colSellerName
            // 
            this.colSellerName.Caption = "Sales Name";
            this.colSellerName.FieldName = "SellerName";
            this.colSellerName.Name = "colSellerName";
            this.colSellerName.OptionsColumn.AllowEdit = false;
            this.colSellerName.OptionsColumn.AllowFocus = false;
            this.colSellerName.OptionsColumn.ReadOnly = true;
            this.colSellerName.Visible = true;
            this.colSellerName.VisibleIndex = 2;
            this.colSellerName.Width = 326;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.saleGridControl;
            this.gridView3.Name = "gridView3";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(label2);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtItemQuantity);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtUnitQuantity);
            this.splitContainerControl1.Panel1.Controls.Add(discountSalesLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtDiscountSales);
            this.splitContainerControl1.Panel1.Controls.Add(discountBLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtDiscountB);
            this.splitContainerControl1.Panel1.Controls.Add(discountALabel1);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtDiscountA);
            this.splitContainerControl1.Panel1.Controls.Add(this.LblTotal);
            this.splitContainerControl1.Panel1.Controls.Add(this.CbProductName);
            this.splitContainerControl1.Panel1.Controls.Add(productNameLabel);
            this.splitContainerControl1.Panel1.Controls.Add(quantityLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtQuantity);
            this.splitContainerControl1.Panel1.Controls.Add(grossPriceLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtGrossPrice);
            this.splitContainerControl1.Panel1.Controls.Add(this.CbProduct);
            this.splitContainerControl1.Panel1.Controls.Add(poNumberLabel2);
            this.splitContainerControl1.Panel1.Controls.Add(this.TxtPoNumber);
            this.splitContainerControl1.Panel1.Controls.Add(this.bindingNavigator1);
            this.splitContainerControl1.Panel1.Controls.Add(this.idTextBox);
            this.splitContainerControl1.Panel1.Controls.Add(this.idTextBox1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.saleDetailsGridControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(727, 461);
            this.splitContainerControl1.SplitterPosition = 153;
            this.splitContainerControl1.TabIndex = 11;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // TxtItemQuantity
            // 
            this.TxtItemQuantity.Location = new System.Drawing.Point(627, 112);
            this.TxtItemQuantity.Name = "TxtItemQuantity";
            this.TxtItemQuantity.Size = new System.Drawing.Size(100, 20);
            this.TxtItemQuantity.TabIndex = 25;
            this.TxtItemQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtUnitQuantity
            // 
            this.TxtUnitQuantity.Location = new System.Drawing.Point(627, 86);
            this.TxtUnitQuantity.Name = "TxtUnitQuantity";
            this.TxtUnitQuantity.Size = new System.Drawing.Size(100, 20);
            this.TxtUnitQuantity.TabIndex = 24;
            this.TxtUnitQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtDiscountSales
            // 
            this.TxtDiscountSales.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "DiscountSales", true));
            this.TxtDiscountSales.Enabled = false;
            this.TxtDiscountSales.Location = new System.Drawing.Point(626, 32);
            this.TxtDiscountSales.Name = "TxtDiscountSales";
            this.TxtDiscountSales.Size = new System.Drawing.Size(100, 20);
            this.TxtDiscountSales.TabIndex = 23;
            this.TxtDiscountSales.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // saleDetailsBindingSource
            // 
            this.saleDetailsBindingSource.DataMember = "SaleDetails";
            this.saleDetailsBindingSource.DataSource = this.posDataSet;
            // 
            // TxtDiscountB
            // 
            this.TxtDiscountB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "DiscountB", true));
            this.TxtDiscountB.Enabled = false;
            this.TxtDiscountB.Location = new System.Drawing.Point(92, 112);
            this.TxtDiscountB.Name = "TxtDiscountB";
            this.TxtDiscountB.Size = new System.Drawing.Size(100, 20);
            this.TxtDiscountB.TabIndex = 22;
            this.TxtDiscountB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TxtDiscountA
            // 
            this.TxtDiscountA.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "DiscountA", true));
            this.TxtDiscountA.Enabled = false;
            this.TxtDiscountA.Location = new System.Drawing.Point(92, 86);
            this.TxtDiscountA.Name = "TxtDiscountA";
            this.TxtDiscountA.Size = new System.Drawing.Size(100, 20);
            this.TxtDiscountA.TabIndex = 21;
            this.TxtDiscountA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LblTotal
            // 
            this.LblTotal.AutoSize = true;
            this.LblTotal.Location = new System.Drawing.Point(207, 116);
            this.LblTotal.Name = "LblTotal";
            this.LblTotal.Size = new System.Drawing.Size(37, 13);
            this.LblTotal.TabIndex = 18;
            this.LblTotal.Text = "[Total]";
            // 
            // CbProductName
            // 
            this.CbProductName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CbProductName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CbProductName.CausesValidation = false;
            this.CbProductName.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.saleDetailsBindingSource, "ProductId", true));
            this.CbProductName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "CodeName", true));
            this.CbProductName.DataSource = this.productBindingSource;
            this.CbProductName.DisplayMember = "CodeName";
            this.CbProductName.Enabled = false;
            this.CbProductName.FormattingEnabled = true;
            this.CbProductName.Location = new System.Drawing.Point(92, 60);
            this.CbProductName.Name = "CbProductName";
            this.CbProductName.Size = new System.Drawing.Size(425, 21);
            this.CbProductName.TabIndex = 17;
            this.CbProductName.ValueMember = "Id";
            this.CbProductName.SelectionChangeCommitted += new System.EventHandler(this.CbProductName_SelectionChangeCommitted);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataMember = "Product";
            this.productBindingSource.DataSource = this.posDataSet;
            // 
            // TxtQuantity
            // 
            this.TxtQuantity.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.saleDetailsBindingSource, "Quantity", true));
            this.TxtQuantity.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TxtQuantity.Location = new System.Drawing.Point(626, 86);
            this.TxtQuantity.Name = "TxtQuantity";
            this.TxtQuantity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TxtQuantity.Size = new System.Drawing.Size(100, 20);
            this.TxtQuantity.TabIndex = 14;
            this.TxtQuantity.EditValueChanged += new System.EventHandler(this.TxtQuantity_EditValueChanged);
            // 
            // TxtGrossPrice
            // 
            this.TxtGrossPrice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.saleDetailsBindingSource, "GrossPrice", true));
            this.TxtGrossPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TxtGrossPrice.Location = new System.Drawing.Point(626, 60);
            this.TxtGrossPrice.Name = "TxtGrossPrice";
            this.TxtGrossPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TxtGrossPrice.Size = new System.Drawing.Size(100, 20);
            this.TxtGrossPrice.TabIndex = 12;
            this.TxtGrossPrice.EditValueChanged += new System.EventHandler(this.TxtGrossPrice_EditValueChanged);
            // 
            // CbProduct
            // 
            this.CbProduct.CausesValidation = false;
            this.CbProduct.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "ProductId", true));
            this.CbProduct.DataSource = this.productBindingSource;
            this.CbProduct.DisplayMember = "Id";
            this.CbProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbProduct.FormattingEnabled = true;
            this.CbProduct.Location = new System.Drawing.Point(92, 60);
            this.CbProduct.Name = "CbProduct";
            this.CbProduct.Size = new System.Drawing.Size(176, 21);
            this.CbProduct.TabIndex = 4;
            // 
            // TxtPoNumber
            // 
            this.TxtPoNumber.CausesValidation = false;
            this.TxtPoNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "PoNumber", true));
            this.TxtPoNumber.Enabled = false;
            this.TxtPoNumber.Location = new System.Drawing.Point(93, 32);
            this.TxtPoNumber.Name = "TxtPoNumber";
            this.TxtPoNumber.Size = new System.Drawing.Size(100, 20);
            this.TxtPoNumber.TabIndex = 2;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.bindingNavigator1.BindingSource = this.saleDetailsBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem1;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1,
            this.bindingNavigatorSaveItem1,
            this.PrintButton});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem1;
            this.bindingNavigator1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.bindingNavigator1.Size = new System.Drawing.Size(727, 25);
            this.bindingNavigator1.TabIndex = 0;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem1.Text = "Add new";
            this.bindingNavigatorAddNewItem1.Click += new System.EventHandler(this.bindingNavigatorAddNewItem1_Click);
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem1.Text = "of {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem1.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 22);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorSaveItem1
            // 
            this.bindingNavigatorSaveItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorSaveItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorSaveItem1.Image")));
            this.bindingNavigatorSaveItem1.Name = "bindingNavigatorSaveItem1";
            this.bindingNavigatorSaveItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorSaveItem1.Text = "Save Data";
            this.bindingNavigatorSaveItem1.Click += new System.EventHandler(this.bindingNavigatorSaveItem1_Click);
            // 
            // PrintButton
            // 
            this.PrintButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PrintButton.Image = ((System.Drawing.Image)(resources.GetObject("PrintButton.Image")));
            this.PrintButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(23, 22);
            this.PrintButton.Text = "Print";
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "Id", true));
            this.idTextBox.Location = new System.Drawing.Point(93, 32);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(100, 20);
            this.idTextBox.TabIndex = 20;
            // 
            // idTextBox1
            // 
            this.idTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.saleDetailsBindingSource, "Id", true));
            this.idTextBox1.Location = new System.Drawing.Point(93, 32);
            this.idTextBox1.Name = "idTextBox1";
            this.idTextBox1.Size = new System.Drawing.Size(100, 20);
            this.idTextBox1.TabIndex = 27;
            // 
            // saleDetailsGridControl
            // 
            this.saleDetailsGridControl.CausesValidation = false;
            this.saleDetailsGridControl.DataSource = this.saleDetailsBindingSource;
            this.saleDetailsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saleDetailsGridControl.Location = new System.Drawing.Point(0, 0);
            this.saleDetailsGridControl.MainView = this.gridView1;
            this.saleDetailsGridControl.Margin = new System.Windows.Forms.Padding(0);
            this.saleDetailsGridControl.Name = "saleDetailsGridControl";
            this.saleDetailsGridControl.Size = new System.Drawing.Size(727, 303);
            this.saleDetailsGridControl.TabIndex = 0;
            this.saleDetailsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.saleDetailsGridControl.Load += new System.EventHandler(this.saleDetailsGridControl_Load);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPoNumber1,
            this.colProductId,
            this.colProductName,
            this.colDiscountA,
            this.colDiscountB,
            this.colDiscountSales,
            this.colGrossPrice,
            this.colQuantity});
            this.gridView1.GridControl = this.saleDetailsGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colProductName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colPoNumber1
            // 
            this.colPoNumber1.FieldName = "PoNumber";
            this.colPoNumber1.Name = "colPoNumber1";
            this.colPoNumber1.OptionsColumn.AllowEdit = false;
            this.colPoNumber1.OptionsColumn.AllowFocus = false;
            this.colPoNumber1.OptionsColumn.ReadOnly = true;
            // 
            // colProductId
            // 
            this.colProductId.FieldName = "ProductId";
            this.colProductId.Name = "colProductId";
            this.colProductId.OptionsColumn.AllowEdit = false;
            this.colProductId.OptionsColumn.AllowFocus = false;
            this.colProductId.OptionsColumn.ReadOnly = true;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Product";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowFocus = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 0;
            this.colProductName.Width = 464;
            // 
            // colDiscountA
            // 
            this.colDiscountA.FieldName = "DiscountA";
            this.colDiscountA.Name = "colDiscountA";
            this.colDiscountA.OptionsColumn.AllowEdit = false;
            this.colDiscountA.OptionsColumn.AllowFocus = false;
            this.colDiscountA.OptionsColumn.ReadOnly = true;
            this.colDiscountA.Visible = true;
            this.colDiscountA.VisibleIndex = 1;
            this.colDiscountA.Width = 142;
            // 
            // colDiscountB
            // 
            this.colDiscountB.FieldName = "DiscountB";
            this.colDiscountB.Name = "colDiscountB";
            this.colDiscountB.OptionsColumn.AllowEdit = false;
            this.colDiscountB.OptionsColumn.AllowFocus = false;
            this.colDiscountB.OptionsColumn.ReadOnly = true;
            this.colDiscountB.Visible = true;
            this.colDiscountB.VisibleIndex = 2;
            this.colDiscountB.Width = 142;
            // 
            // colDiscountSales
            // 
            this.colDiscountSales.FieldName = "DiscountSales";
            this.colDiscountSales.Name = "colDiscountSales";
            this.colDiscountSales.OptionsColumn.AllowEdit = false;
            this.colDiscountSales.OptionsColumn.AllowFocus = false;
            this.colDiscountSales.OptionsColumn.ReadOnly = true;
            this.colDiscountSales.Visible = true;
            this.colDiscountSales.VisibleIndex = 3;
            this.colDiscountSales.Width = 142;
            // 
            // colGrossPrice
            // 
            this.colGrossPrice.DisplayFormat.FormatString = "Rp.{0:0,0.00},-";
            this.colGrossPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colGrossPrice.FieldName = "GrossPrice";
            this.colGrossPrice.Name = "colGrossPrice";
            this.colGrossPrice.OptionsColumn.AllowEdit = false;
            this.colGrossPrice.OptionsColumn.AllowFocus = false;
            this.colGrossPrice.OptionsColumn.ReadOnly = true;
            this.colGrossPrice.Visible = true;
            this.colGrossPrice.VisibleIndex = 4;
            this.colGrossPrice.Width = 142;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowFocus = false;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 5;
            this.colQuantity.Width = 148;
            // 
            // tableAdapterManagerSaleDetail
            // 
            this.tableAdapterManagerSaleDetail.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManagerSaleDetail.BrandTableAdapter = null;
            this.tableAdapterManagerSaleDetail.ProductDiscountsTableAdapter = null;
            this.tableAdapterManagerSaleDetail.ProductTableAdapter = null;
            this.tableAdapterManagerSaleDetail.PurchaseDetailTableAdapter = null;
            this.tableAdapterManagerSaleDetail.PurchaseTableAdapter = null;
            this.tableAdapterManagerSaleDetail.SaleDetailsTableAdapter = this.saleDetailsTableAdapter;
            this.tableAdapterManagerSaleDetail.SaleTableAdapter = null;
            this.tableAdapterManagerSaleDetail.SellerTableAdapter = null;
            this.tableAdapterManagerSaleDetail.SpreadDetailsTableAdapter = null;
            this.tableAdapterManagerSaleDetail.SpreadTableAdapter = null;
            this.tableAdapterManagerSaleDetail.StoresTableAdapter = null;
            this.tableAdapterManagerSaleDetail.SupplierTableAdapter = null;
            this.tableAdapterManagerSaleDetail.UpdateOrder = trgpos.posDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManagerSaleDetail.UserTableAdapter = null;
            // 
            // productTableAdapter
            // 
            this.productTableAdapter.ClearBeforeFill = true;
            // 
            // productDiscountsTableAdapter
            // 
            this.productDiscountsTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(110)))), ((int)(((byte)(210)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1273, 30);
            this.panel1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(9, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Transaction / Sale";
            // 
            // salesViewReportTableAdapter
            // 
            this.salesViewReportTableAdapter.ClearBeforeFill = true;
            // 
            // Sales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl);
            this.Controls.Add(this.panel1);
            this.Name = "Sales";
            this.Size = new System.Drawing.Size(1273, 505);
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saleBindingNavigator)).EndInit();
            this.saleBindingNavigator.ResumeLayout(false);
            this.saleBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.storesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saleGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.saleDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrossPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.saleDetailsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private posDataSet posDataSet;
        private System.Windows.Forms.BindingSource saleBindingSource;
        private posDataSetTableAdapters.SaleTableAdapter saleTableAdapter;
        private posDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator saleBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton saleBindingNavigatorSaveItem;
        private posDataSetTableAdapters.SaleDetailsTableAdapter saleDetailsTableAdapter;
        private System.Windows.Forms.BindingSource storesBindingSource;
        private posDataSetTableAdapters.StoresTableAdapter storesTableAdapter;
        private System.Windows.Forms.BindingSource sellerBindingSource;
        private posDataSetTableAdapters.SellerTableAdapter sellerTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.BindingSource saleDetailsBindingSource;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private DevExpress.XtraGrid.GridControl saleDetailsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colPoNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colProductId;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountA;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountB;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountSales;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraEditors.SpinEdit TxtQuantity;
        private DevExpress.XtraEditors.SpinEdit TxtGrossPrice;
        private System.Windows.Forms.ComboBox CbProduct;
        private System.Windows.Forms.TextBox TxtPoNumber;
        private System.Windows.Forms.ToolStripButton bindingNavigatorSaveItem1;
        private posDataSetTableAdapters.TableAdapterManager tableAdapterManagerSaleDetail;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox poToolStripTextBox;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource productBindingSource;
        private posDataSetTableAdapters.ProductTableAdapter productTableAdapter;
        private posDataSetTableAdapters.ProductDiscountsTableAdapter productDiscountsTableAdapter;
        private System.Windows.Forms.ComboBox CbProductName;
        private System.Windows.Forms.Label LblTotal;
        private System.Windows.Forms.ToolStripButton PrintButton;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraGrid.GridControl saleGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreName;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSellerCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSellerName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.ComboBox sellerCodeComboBox;
        private System.Windows.Forms.ComboBox storeCodeComboBox;
        private System.Windows.Forms.DateTimePicker saleDateDateTimePicker;
        private System.Windows.Forms.TextBox poNumberTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox TxtDiscountA;
        private System.Windows.Forms.TextBox TxtDiscountSales;
        private System.Windows.Forms.TextBox TxtDiscountB;
        private System.Windows.Forms.TextBox TxtItemQuantity;
        private System.Windows.Forms.TextBox TxtUnitQuantity;
        private System.Windows.Forms.TextBox idTextBox1;
        private posDataSetTableAdapters.SalesViewReportTableAdapter salesViewReportTableAdapter;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
    }
}
