﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Products : UserControl
    {
        public Products()
        {
            InitializeComponent();
        }

        private void Products_Load(object sender, EventArgs e)
        {
            productTableAdapter.FillBy(posDataSet.Product);
            brandTableAdapter.Fill(posDataSet.Brand);
        }

        private void bindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            productBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(posDataSet);
            if(String.IsNullOrEmpty (TxtSearch.Text )){
                productTableAdapter.FillBy(posDataSet.Product);
            }else{
                String cleanText = TxtSearch.Text.Trim();
                productTableAdapter.FillByTerm(posDataSet.Product, cleanText);
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            brandIdComboBox.Text = brandNameComboBox.SelectedValue.ToString();
            stockTextEdit.Text = "0";
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            tableAdapterManager.UpdateAll(posDataSet);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            TextBox txtbox = (TextBox)sender;
            String cleanText = txtbox.Text.Trim();
            productTableAdapter.FillByTerm(posDataSet.Product, cleanText);
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            TxtSearch.Text = String.Empty;
            productTableAdapter.FillBy(posDataSet.Product);
        }
    }
}
