﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Seller : UserControl
    {
        private const string pattern = "SLM{0}";
        public Seller()
        {
            InitializeComponent();
        }

        private void sellerBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {

                if (String.IsNullOrEmpty(telephoneTextEdit.Text))
                {
                    telephoneTextEdit.Text = " ";
                }
                Validate();
                sellerBindingSource.EndEdit();
                tableAdapterManager.UpdateAll(posDataSet);
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
                }
        }

        private void Seller_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtSearch.Text))
            {
                sellerTableAdapter.Fill(posDataSet.Seller);
            }
            else
            {
                String cleanText = TxtSearch.Text.Trim();
                sellerTableAdapter.FillByTerm(posDataSet.Seller, cleanText);
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            var latestCode = sellerTableAdapter.GetLatestCode().Remove(0, 3);
            var nrCount = int.Parse(latestCode) + 1;
            var str = nrCount.ToString("000");
            codeTextBox.Text = String.Format(pattern, str);
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            try
            {
                tableAdapterManager.UpdateAll(posDataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void telephoneTextEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                sellerBindingNavigatorSaveItem_Click(sender, e);
            }
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            TxtSearch.Text = String.Empty;
            sellerTableAdapter.Fill(posDataSet.Seller);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            TextBox txtbox = (TextBox)sender;
            String cleanText = txtbox.Text.Trim();
            sellerTableAdapter.FillByTerm(posDataSet.Seller, cleanText);
        }
    }
}
