﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using System.Drawing;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;

namespace trgpos.Fragments
{
    public partial class Spreading : UserControl
    {
        public Spreading()
        {
            InitializeComponent();
            this.productTableAdapter.FillByCustom(this.posDataSet.Product);
            this.sellerTableAdapter.Fill(this.posDataSet.Seller);
            this.spreadTableAdapter.Fill(this.posDataSet.Spread);

            CbProductName_SelectionChangeCommitted(CbProductName, null);
        }

        private void spreadDetailsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.spreadDetailsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.posDataSet);

        }

        private void spreadBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.spreadBindingSource.EndEdit();
            //this.tableAdapterManager.UpdateAll(this.posDataSet);
            this.spreadTableAdapter.Update(this.posDataSet.Spread);

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string spreadNr = dt.ToString("yMdHms");
            spreadNumberTextBox.Text = String.Format("SP{0}", spreadNr);
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            this.spreadDetailsTableAdapter.FillBySpreadCode(this.posDataSet.SpreadDetails, spreadNumberTextBox.Text);
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (gridView1.IsRowSelected(i))
                {

                    string spreadCode = String.Empty;
                    if (gridView1.GetRowCellValue(i, "SpreadNumber") != null)
                    {
                        spreadCode = gridView1.GetRowCellValue(i, "SpreadNumber").ToString();
                    }

                    TxtSpreadCode.Text = spreadCode;
                    break;
                }
            }
        
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            TxtSpreadCode.Text = spreadNumberTextBox.Text;
        
            CbProductName.Enabled = true;
            qtySoldSpinEdit.Value = 0;
            qtyOutSpinEdit.Value = 0;
        }

        private void CbProductName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            qtySoldSpinEdit.Value = 0;
            qtyOutSpinEdit.Value = 0;

            ComboBox selectedProduct = (ComboBox)sender;
            DataRowView selectedDataRow = (DataRowView)selectedProduct.SelectedItem;
            if (selectedDataRow != null)
            {

                long id = 0;
                long.TryParse(selectedDataRow["Id"].ToString(), out id);

                if (gridView2.RowCount > 1)
                {
                    for (int i = 0; i < gridView2.RowCount; i++)
                    {
                        if (gridView2.GetRowCellValue(i, "ProductId") == null)
                            continue;

                        int productId = 0;
                        int.TryParse(gridView2.GetRowCellValue(i, "ProductId").ToString(), out productId);
                        if (id == productId)
                        {
                            this.spreadDetailsTableAdapter.FillBySpreadCode(this.posDataSet.SpreadDetails, spreadNumberTextBox.Text);
                            gridView2.FocusedRowHandle = i;
                        }
                    }
                }
            }
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
           // int id = 0;
           // int.TryParse(idTextBox1.Text, out id);

            try
            {
                this.Validate();

                int quantityOut = 0;
                if (!String.IsNullOrEmpty(qtyOutSpinEdit.Text))
                {
                    int.TryParse(qtyOutSpinEdit.Value.ToString(), out quantityOut);
                }

                int quantitySold = 0;
                if (!String.IsNullOrEmpty(qtySoldSpinEdit.Text))
                {
                    int.TryParse(qtySoldSpinEdit.Value.ToString(), out quantitySold);
                }

                if (quantityOut < quantitySold)
                {
                    MessageBox.Show("Quantity Sold is not valid.");
                    return;
                }

                DataRowView selectedDataRow = (DataRowView)CbProductName.SelectedItem;

                int selectedIndex = 0;
                selectedIndex = CbProductName.SelectedIndex;
                /*if (selectedDataRow != null)
                {
                    int itemPerUnit = 0;
                    int.TryParse(selectedDataRow["ItemPerUnit"].ToString(), out itemPerUnit);
                    if (itemPerUnit > 0)
                    {
                        if (unitQuantity > 0)
                        {
                            unitQuantity = unitQuantity * itemPerUnit;
                        }
                    }


                }

                TxtQuantity.Text = (unitQuantity + itemQuantity).ToString();

                int txtQuantity;
                int.TryParse(TxtQuantity.Value.ToString(), out txtQuantity);

                if (txtQuantity <= 0)
                {
                    MessageBox.Show("The Quantity must be greather than 0.");
                    return;
                }
                */
                this.spreadDetailsBindingSource.EndEdit();

                
                this.spreadDetailsTableAdapter.Update(this.posDataSet);
                //ShowTotal();
                this.productTableAdapter.FillByCustom(this.posDataSet.Product);

                CbProductName.SelectedIndex = selectedIndex;
                this.spreadDetailsTableAdapter.FillBySpreadCode(this.posDataSet.SpreadDetails, spreadNumberTextBox.Text);


                CbProductName.Enabled = false;

                MessageBox.Show("Data saved successfully.");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            try
            {
                BrickGraphics gr = printingSystem.Graph;
                BrickStringFormat bsf = new BrickStringFormat(StringAlignment.Near, StringAlignment.Center);
                gr.StringFormat = bsf;
                gr.BorderColor = SystemColors.ControlDark;

                // Declare bricks.
                TextBrick textbrick;
                //CheckBoxBrick checkbrick;
                //Brick brick;
                PageInfoBrick pageinfobr;

                printingSystem.PageSettings.Landscape = false;
                printingSystem.Begin();

                gr.Modifier = BrickModifier.Detail;
                gr.BeginUnionRect();

                int y = 5;
                int x = 5;

                gr.Font = new Font("Arial", 13, FontStyle.Bold);
                textbrick = gr.DrawString(Properties.Settings.Default.NamaPerusahaan.ToString().ToUpper(), Color.Black, new RectangleF(x, y, 400, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


                y += 25;
                gr.Font = new Font("Arial", 11, FontStyle.Regular);
                textbrick = gr.DrawString(String.Format("LAPORANG STOCK SPREADING {0:dd-MM-yyyy}", spreadDateDateEdit.EditValue), Color.Black, new RectangleF(x, y, 400, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

                y += 25;
                gr.Font = new Font("Arial", 9, FontStyle.Regular);
                textbrick = gr.DrawString("KODE SPREADING : " + spreadNumberTextBox.Text, Color.Black, new RectangleF(x, y, 250, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

                textbrick = gr.DrawString("NAMA : " + sellerCodeComboBox.Text, Color.Black, new RectangleF(x + 275, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

                gr.Font = new Font("Arial", 7, FontStyle.Regular);
                int[] topHeaderWidth = { 40, 90, 370, 75, 75, 80 };
                string[] topHeaderTitle = { "No.", "Kode", "Nama Barang", 
                                       "Qty Masuk", "Qty Keluar", "Saldo Akhir" };

                y += 25;
                int topHeaderX = 0;
                for (int i = 0; i < topHeaderWidth.Length; i++)
                {
                    textbrick = gr.DrawString(topHeaderTitle[i], Color.Black, new RectangleF(topHeaderX, y, topHeaderWidth[i], 20), BorderSide.All);
                    textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                    textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                    topHeaderX += topHeaderWidth[i];
                }

                //get the data
                DataTable dtSaleDetail = spreadDetailsTableAdapter.GetDataBySpreadCode(spreadNumberTextBox.Text);

                if (dtSaleDetail.Rows.Count > 0)
                {
                    gr.Font = new Font("Arial", 7, FontStyle.Regular);
                    int index = 1;

                    foreach (DataRow row in dtSaleDetail.Rows)
                    {
                        y += 20;
                        int itemJ = 0;

                        long id = 0;
                        if (row["ProductId"] != null)
                        {
                            long.TryParse(row["ProductId"].ToString(), out id);
                        }
                        if (id > 0)
                        {
                            DataTable dtProduct = productTableAdapter.GetDataById(id);
                            if (dtProduct != null)
                            {
                                foreach (DataRow rowProduct in dtProduct.Rows)
                                {
                                    int quantityOut = 0;
                                    int.TryParse(row["QtyOut"].ToString(), out quantityOut);

                                    int quantitySold = 0;
                                    int.TryParse(row["QtySold"].ToString(), out quantitySold);

                                    int qty = quantityOut - quantitySold;

                                    for (int j = 0; j < topHeaderWidth.Length; j++)
                                    {
                                        string text = string.Empty;
                                        switch (j)
                                        {
                                            case 0:
                                                text = "  " + (index).ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;
                                            case 1:
                                                text = rowProduct["Code"].ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;
                                            case 2:
                                                text = row["ProductName"].ToString();//string.Format("{0:0.0}", Math.Truncate(grossPrice * 10) / 10);//rowProduct["Name"].ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;
                                            case 3:
                                                text = quantityOut.ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;
                                            case 4:
                                                text = quantitySold.ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                                itemJ += topHeaderWidth[j];
                                                break;
                                            case 5:
                                                text = qty.ToString();
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;

                                            default:
                                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, topHeaderWidth[j], 20), BorderSide.All);
                                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                                itemJ += topHeaderWidth[j];
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                y += 80;

                textbrick = gr.DrawString("Diterima", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

                textbrick = gr.DrawString("Diserahkan", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

                textbrick = gr.DrawString("Dibuat", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;


                y += 70;

                textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

                textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

                textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

                gr.Font = new Font("Arial", 8, FontStyle.Regular);
                // Finish the creation of a non-separable group of bricks.
                gr.EndUnionRect();

                // Create a MarginalHeader section.
                gr.Modifier = BrickModifier.MarginalHeader;
                RectangleF r = RectangleF.Empty;
                r.Height = 40;
                gr.BackColor = Color.White;



                // Display the DevExpress text string.
                //SizeF sz = gr.MeasureString("trigniter");

                // Display the PageInfoBrick containing date-time information. Date-time information is displayed
                // in the left part of the MarginalHeader section using the FullDateTimePattern.
                pageinfobr = gr.DrawPageInfo(PageInfo.DateTime, "Tanggal Cetak : {0:dd-MM-yyyy}", Color.Black, r, BorderSide.None);
                pageinfobr.Alignment = BrickAlignment.Near;

                // Display the PageInfoBrick containing the page number among total pages. The page number
                // is displayed in the right part of the MarginalHeader section.
                pageinfobr = gr.DrawPageInfo(PageInfo.NumberOfTotal, "Halaman {0} dari {1}", Color.Black, r,
                     BorderSide.None);
                pageinfobr.Alignment = BrickAlignment.Far;

                printingSystem.PageSettings.LeftMargin = 40;
                printingSystem.PageSettings.RightMargin = 0;

                printingSystem.End();
                printingSystem.PreviewFormEx.Show();
                //printingSystem.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
