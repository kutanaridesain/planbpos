﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace trgpos.Fragments
{
    public partial class SalesReport : UserControl
    {
        public SalesReport()
        {
            InitializeComponent();

            DtpStartDate.Value = DateTime.Now.AddDays(-7);
            DtpEndDate.Value = DateTime.Now;

            this.salesViewReportTableAdapter.FillByStartEndDate(
               this.posDataSet.SalesViewReport, DateTime.Now.AddDays(-7).Date, DateTime.Now.Date);
        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            this.salesViewReportTableAdapter.FillByStartEndDate(
               this.posDataSet.SalesViewReport, DtpStartDate.Value.Date,  DtpEndDate.Value.Date);
        }

        private void BtnExport_Click(object sender, EventArgs e)
        {
            gridView1.ExportToXlsx("SalesReport.xlsx");

           // Application xcel = new Microsoft.Office.Interop.Excel.Application(;
            System.Diagnostics.Process.Start("SalesReport.xlsx");

        }


    }
}
