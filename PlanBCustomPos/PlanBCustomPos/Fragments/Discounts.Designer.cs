﻿namespace trgpos.Fragments
{
    partial class Discounts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label productIdLabel;
            System.Windows.Forms.Label discountALabel;
            System.Windows.Forms.Label discountBLabel;
            System.Windows.Forms.Label quantityLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Discounts));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.codeNameComboBox = new System.Windows.Forms.ComboBox();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.posDataSet = new trgpos.posDataSet();
            this.quantitySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.productDiscountsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.discountBSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.discountASpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.productIdComboBox = new System.Windows.Forms.ComboBox();
            this.productDiscountsGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colDiscountA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.productDiscountsTableAdapter = new trgpos.posDataSetTableAdapters.ProductDiscountsTableAdapter();
            this.tableAdapterManager = new trgpos.posDataSetTableAdapters.TableAdapterManager();
            this.productDiscountsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.productDiscountsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.productTableAdapter = new trgpos.posDataSetTableAdapters.ProductTableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LblSearch = new System.Windows.Forms.Label();
            this.BtnClean = new System.Windows.Forms.Button();
            this.TxtSearch = new System.Windows.Forms.TextBox();
            productIdLabel = new System.Windows.Forms.Label();
            discountALabel = new System.Windows.Forms.Label();
            discountBLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantitySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountASpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsBindingNavigator)).BeginInit();
            this.productDiscountsBindingNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // productIdLabel
            // 
            productIdLabel.AutoSize = true;
            productIdLabel.Location = new System.Drawing.Point(17, 12);
            productIdLabel.Name = "productIdLabel";
            productIdLabel.Size = new System.Drawing.Size(54, 13);
            productIdLabel.TabIndex = 0;
            productIdLabel.Text = "Produk * :";
            // 
            // discountALabel
            // 
            discountALabel.AutoSize = true;
            discountALabel.Location = new System.Drawing.Point(17, 39);
            discountALabel.Name = "discountALabel";
            discountALabel.Size = new System.Drawing.Size(63, 13);
            discountALabel.TabIndex = 2;
            discountALabel.Text = "Diskon A * :";
            // 
            // discountBLabel
            // 
            discountBLabel.AutoSize = true;
            discountBLabel.Location = new System.Drawing.Point(17, 65);
            discountBLabel.Name = "discountBLabel";
            discountBLabel.Size = new System.Drawing.Size(56, 13);
            discountBLabel.TabIndex = 4;
            discountBLabel.Text = "Diskon B :";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(16, 91);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(57, 13);
            quantityLabel.TabIndex = 6;
            quantityLabel.Text = "Kuantitas :";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 55);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panel2);
            this.splitContainerControl1.Panel1.Controls.Add(this.codeNameComboBox);
            this.splitContainerControl1.Panel1.Controls.Add(quantityLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.quantitySpinEdit);
            this.splitContainerControl1.Panel1.Controls.Add(discountBLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.discountBSpinEdit);
            this.splitContainerControl1.Panel1.Controls.Add(discountALabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.discountASpinEdit);
            this.splitContainerControl1.Panel1.Controls.Add(productIdLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.productIdComboBox);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.productDiscountsGridControl);
            this.splitContainerControl1.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(642, 483);
            this.splitContainerControl1.SplitterPosition = 112;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // codeNameComboBox
            // 
            this.codeNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.codeNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.codeNameComboBox.CausesValidation = false;
            this.codeNameComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "CodeName", true));
            this.codeNameComboBox.DataSource = this.productBindingSource;
            this.codeNameComboBox.DisplayMember = "CodeName";
            this.codeNameComboBox.FormattingEnabled = true;
            this.codeNameComboBox.Location = new System.Drawing.Point(108, 9);
            this.codeNameComboBox.Name = "codeNameComboBox";
            this.codeNameComboBox.Size = new System.Drawing.Size(399, 21);
            this.codeNameComboBox.TabIndex = 10;
            this.codeNameComboBox.ValueMember = "Id";
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataMember = "Product";
            this.productBindingSource.DataSource = this.posDataSet;
            // 
            // posDataSet
            // 
            this.posDataSet.DataSetName = "posDataSet";
            this.posDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // quantitySpinEdit
            // 
            this.quantitySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productDiscountsBindingSource, "Quantity", true));
            this.quantitySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.quantitySpinEdit.Location = new System.Drawing.Point(108, 88);
            this.quantitySpinEdit.Name = "quantitySpinEdit";
            this.quantitySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.quantitySpinEdit.Size = new System.Drawing.Size(100, 20);
            this.quantitySpinEdit.TabIndex = 7;
            // 
            // productDiscountsBindingSource
            // 
            this.productDiscountsBindingSource.DataMember = "ProductDiscounts";
            this.productDiscountsBindingSource.DataSource = this.posDataSet;
            // 
            // discountBSpinEdit
            // 
            this.discountBSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productDiscountsBindingSource, "DiscountB", true));
            this.discountBSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.discountBSpinEdit.Location = new System.Drawing.Point(108, 62);
            this.discountBSpinEdit.Name = "discountBSpinEdit";
            this.discountBSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.discountBSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.discountBSpinEdit.TabIndex = 5;
            // 
            // discountASpinEdit
            // 
            this.discountASpinEdit.CausesValidation = false;
            this.discountASpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.productDiscountsBindingSource, "DiscountA", true));
            this.discountASpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.discountASpinEdit.Location = new System.Drawing.Point(108, 36);
            this.discountASpinEdit.Name = "discountASpinEdit";
            this.discountASpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.discountASpinEdit.Size = new System.Drawing.Size(100, 20);
            this.discountASpinEdit.TabIndex = 3;
            // 
            // productIdComboBox
            // 
            this.productIdComboBox.CausesValidation = false;
            this.productIdComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productDiscountsBindingSource, "ProductId", true));
            this.productIdComboBox.DataSource = this.productBindingSource;
            this.productIdComboBox.DisplayMember = "Id";
            this.productIdComboBox.FormattingEnabled = true;
            this.productIdComboBox.Location = new System.Drawing.Point(108, 9);
            this.productIdComboBox.Name = "productIdComboBox";
            this.productIdComboBox.Size = new System.Drawing.Size(82, 21);
            this.productIdComboBox.TabIndex = 1;
            this.productIdComboBox.ValueMember = "Id";
            // 
            // productDiscountsGridControl
            // 
            this.productDiscountsGridControl.DataSource = this.productDiscountsBindingSource;
            this.productDiscountsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productDiscountsGridControl.Location = new System.Drawing.Point(5, 5);
            this.productDiscountsGridControl.MainView = this.gridView1;
            this.productDiscountsGridControl.Name = "productDiscountsGridControl";
            this.productDiscountsGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.productDiscountsGridControl.Size = new System.Drawing.Size(632, 356);
            this.productDiscountsGridControl.TabIndex = 0;
            this.productDiscountsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colProductId,
            this.colDiscountA,
            this.colDiscountB,
            this.colQuantity,
            this.colProductName});
            this.gridView1.GridControl = this.productDiscountsGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colProductId, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colProductId
            // 
            this.colProductId.Caption = "Nama Produk";
            this.colProductId.ColumnEdit = this.repositoryItemComboBox1;
            this.colProductId.FieldName = "ProductId";
            this.colProductId.Name = "colProductId";
            this.colProductId.OptionsColumn.AllowEdit = false;
            this.colProductId.OptionsColumn.AllowFocus = false;
            this.colProductId.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // colDiscountA
            // 
            this.colDiscountA.Caption = "Diskon A (%)";
            this.colDiscountA.FieldName = "DiscountA";
            this.colDiscountA.Name = "colDiscountA";
            this.colDiscountA.OptionsColumn.AllowEdit = false;
            this.colDiscountA.OptionsColumn.AllowFocus = false;
            this.colDiscountA.OptionsColumn.ReadOnly = true;
            this.colDiscountA.Visible = true;
            this.colDiscountA.VisibleIndex = 1;
            // 
            // colDiscountB
            // 
            this.colDiscountB.Caption = "Diskon B (%)";
            this.colDiscountB.FieldName = "DiscountB";
            this.colDiscountB.Name = "colDiscountB";
            this.colDiscountB.OptionsColumn.AllowEdit = false;
            this.colDiscountB.OptionsColumn.AllowFocus = false;
            this.colDiscountB.OptionsColumn.ReadOnly = true;
            this.colDiscountB.Visible = true;
            this.colDiscountB.VisibleIndex = 2;
            // 
            // colQuantity
            // 
            this.colQuantity.Caption = "Kuantitas minimum untuk diskon B";
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowFocus = false;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 3;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Nama Produk";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowFocus = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 0;
            // 
            // productDiscountsTableAdapter
            // 
            this.productDiscountsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BrandTableAdapter = null;
            this.tableAdapterManager.ProductDiscountsTableAdapter = this.productDiscountsTableAdapter;
            this.tableAdapterManager.ProductTableAdapter = null;
            this.tableAdapterManager.PurchaseDetailTableAdapter = null;
            this.tableAdapterManager.PurchaseTableAdapter = null;
            this.tableAdapterManager.SaleDetailsTableAdapter = null;
            this.tableAdapterManager.SaleTableAdapter = null;
            this.tableAdapterManager.SellerTableAdapter = null;
            this.tableAdapterManager.SpreadDetailsTableAdapter = null;
            this.tableAdapterManager.SpreadTableAdapter = null;
            this.tableAdapterManager.StoresTableAdapter = null;
            this.tableAdapterManager.SupplierTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = trgpos.posDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserTableAdapter = null;
            // 
            // productDiscountsBindingNavigator
            // 
            this.productDiscountsBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.productDiscountsBindingNavigator.BindingSource = this.productDiscountsBindingSource;
            this.productDiscountsBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.productDiscountsBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.productDiscountsBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.productDiscountsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.productDiscountsBindingNavigatorSaveItem});
            this.productDiscountsBindingNavigator.Location = new System.Drawing.Point(0, 30);
            this.productDiscountsBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.productDiscountsBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.productDiscountsBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.productDiscountsBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.productDiscountsBindingNavigator.Name = "productDiscountsBindingNavigator";
            this.productDiscountsBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.productDiscountsBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.productDiscountsBindingNavigator.Size = new System.Drawing.Size(642, 25);
            this.productDiscountsBindingNavigator.TabIndex = 1;
            this.productDiscountsBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 22);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // productDiscountsBindingNavigatorSaveItem
            // 
            this.productDiscountsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.productDiscountsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("productDiscountsBindingNavigatorSaveItem.Image")));
            this.productDiscountsBindingNavigatorSaveItem.Name = "productDiscountsBindingNavigatorSaveItem";
            this.productDiscountsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.productDiscountsBindingNavigatorSaveItem.Text = "Save Data";
            this.productDiscountsBindingNavigatorSaveItem.Click += new System.EventHandler(this.productDiscountsBindingNavigatorSaveItem_Click);
            // 
            // productTableAdapter
            // 
            this.productTableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(110)))), ((int)(((byte)(210)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(642, 30);
            this.panel1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Manage Data / Discounts";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LblSearch);
            this.panel2.Controls.Add(this.BtnClean);
            this.panel2.Controls.Add(this.TxtSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(348, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(294, 112);
            this.panel2.TabIndex = 18;
            // 
            // LblSearch
            // 
            this.LblSearch.AutoSize = true;
            this.LblSearch.Location = new System.Drawing.Point(3, 7);
            this.LblSearch.Name = "LblSearch";
            this.LblSearch.Size = new System.Drawing.Size(90, 13);
            this.LblSearch.TabIndex = 15;
            this.LblSearch.Text = "Search  By Name";
            // 
            // BtnClean
            // 
            this.BtnClean.Location = new System.Drawing.Point(269, 4);
            this.BtnClean.Name = "BtnClean";
            this.BtnClean.Size = new System.Drawing.Size(22, 23);
            this.BtnClean.TabIndex = 16;
            this.BtnClean.Text = "X";
            this.BtnClean.UseVisualStyleBackColor = true;
            this.BtnClean.Click += new System.EventHandler(this.BtnClean_Click);
            // 
            // TxtSearch
            // 
            this.TxtSearch.Location = new System.Drawing.Point(96, 4);
            this.TxtSearch.Name = "TxtSearch";
            this.TxtSearch.Size = new System.Drawing.Size(167, 20);
            this.TxtSearch.TabIndex = 14;
            this.TxtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            // 
            // Discounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.productDiscountsBindingNavigator);
            this.Controls.Add(this.panel1);
            this.Name = "Discounts";
            this.Size = new System.Drawing.Size(642, 538);
            this.Load += new System.EventHandler(this.Discounts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quantitySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountBSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountASpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productDiscountsBindingNavigator)).EndInit();
            this.productDiscountsBindingNavigator.ResumeLayout(false);
            this.productDiscountsBindingNavigator.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private posDataSet posDataSet;
        private System.Windows.Forms.BindingSource productDiscountsBindingSource;
        private posDataSetTableAdapters.ProductDiscountsTableAdapter productDiscountsTableAdapter;
        private posDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator productDiscountsBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton productDiscountsBindingNavigatorSaveItem;
        private DevExpress.XtraGrid.GridControl productDiscountsGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colProductId;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountA;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountB;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraEditors.SpinEdit quantitySpinEdit;
        private DevExpress.XtraEditors.SpinEdit discountBSpinEdit;
        private DevExpress.XtraEditors.SpinEdit discountASpinEdit;
        private System.Windows.Forms.ComboBox productIdComboBox;
        private System.Windows.Forms.BindingSource productBindingSource;
        private posDataSetTableAdapters.ProductTableAdapter productTableAdapter;
        private System.Windows.Forms.BindingSource fKProductDiscountsProductBindingSource;
        private System.Windows.Forms.BindingSource productBindingSource1;
        private System.Windows.Forms.BindingSource productBindingSource2;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private System.Windows.Forms.ComboBox codeNameComboBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label LblSearch;
        private System.Windows.Forms.Button BtnClean;
        private System.Windows.Forms.TextBox TxtSearch;
    }
}
