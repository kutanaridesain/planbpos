﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Brands : UserControl
    {
        public Brands()
        {
            InitializeComponent();
        }

        private void brandBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(nameTextEdit.Text))
            {
                MessageBox.Show("The Name is required!");
                return;
            }
            Validate();
            brandBindingSource.EndEdit();
                tableAdapterManager.UpdateAll(posDataSet);
            if (String.IsNullOrEmpty(TxtSearch.Text))
            {
                brandTableAdapter.Fill(posDataSet.Brand);
            }
            else
            {
                String cleanText = TxtSearch.Text.Trim();
                brandTableAdapter.FillByTerm(posDataSet.Brand, cleanText);
            }
        }

        private void Brands_Load(object sender, EventArgs e)
        {
            brandTableAdapter.Fill(posDataSet.Brand);
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            nameTextEdit.Text = string.Empty;
            ActiveControl = nameTextEdit;
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            tableAdapterManager.UpdateAll(posDataSet);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            TextBox txtbox = (TextBox)sender;
            String cleanText = txtbox.Text.Trim();
            brandTableAdapter.FillByTerm(posDataSet.Brand, cleanText);
           
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            TxtSearch.Text = String.Empty;
            brandTableAdapter.Fill(posDataSet.Brand);
        }
    }
}
