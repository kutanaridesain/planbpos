﻿namespace trgpos.Fragments
{
    partial class SalesReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesReport));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.LblSalesReportTitle = new System.Windows.Forms.Label();
            this.LblStartDate = new System.Windows.Forms.Label();
            this.LblEndDate = new System.Windows.Forms.Label();
            this.DtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.DtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.posDataSet = new trgpos.posDataSet();
            this.salesViewReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.salesViewReportTableAdapter = new trgpos.posDataSetTableAdapters.SalesViewReportTableAdapter();
            this.tableAdapterManager = new trgpos.posDataSetTableAdapters.TableAdapterManager();
            this.salesViewReportBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.salesViewReportBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.salesViewReportGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSaleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellerCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStoreName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProduct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBrand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscountSales = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BtnPreview = new System.Windows.Forms.Button();
            this.BtnExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportBindingNavigator)).BeginInit();
            this.salesViewReportBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.BtnExport);
            this.splitContainerControl1.Panel1.Controls.Add(this.BtnPreview);
            this.splitContainerControl1.Panel1.Controls.Add(this.DtpEndDate);
            this.splitContainerControl1.Panel1.Controls.Add(this.DtpStartDate);
            this.splitContainerControl1.Panel1.Controls.Add(this.LblEndDate);
            this.splitContainerControl1.Panel1.Controls.Add(this.LblStartDate);
            this.splitContainerControl1.Panel1.Controls.Add(this.LblSalesReportTitle);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.salesViewReportGridControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(889, 539);
            this.splitContainerControl1.SplitterPosition = 131;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // LblSalesReportTitle
            // 
            this.LblSalesReportTitle.AutoSize = true;
            this.LblSalesReportTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSalesReportTitle.Location = new System.Drawing.Point(3, 42);
            this.LblSalesReportTitle.Name = "LblSalesReportTitle";
            this.LblSalesReportTitle.Size = new System.Drawing.Size(240, 31);
            this.LblSalesReportTitle.TabIndex = 0;
            this.LblSalesReportTitle.Text = "Laporan Penjualan";
            // 
            // LblStartDate
            // 
            this.LblStartDate.AutoSize = true;
            this.LblStartDate.Location = new System.Drawing.Point(50, 89);
            this.LblStartDate.Name = "LblStartDate";
            this.LblStartDate.Size = new System.Drawing.Size(68, 13);
            this.LblStartDate.TabIndex = 1;
            this.LblStartDate.Text = "Dari Tanggal";
            // 
            // LblEndDate
            // 
            this.LblEndDate.AutoSize = true;
            this.LblEndDate.Location = new System.Drawing.Point(414, 89);
            this.LblEndDate.Name = "LblEndDate";
            this.LblEndDate.Size = new System.Drawing.Size(84, 13);
            this.LblEndDate.TabIndex = 2;
            this.LblEndDate.Text = "Sampai Tanggal";
            // 
            // DtpStartDate
            // 
            this.DtpStartDate.Location = new System.Drawing.Point(125, 89);
            this.DtpStartDate.Name = "DtpStartDate";
            this.DtpStartDate.Size = new System.Drawing.Size(200, 20);
            this.DtpStartDate.TabIndex = 3;
            // 
            // DtpEndDate
            // 
            this.DtpEndDate.Location = new System.Drawing.Point(504, 89);
            this.DtpEndDate.Name = "DtpEndDate";
            this.DtpEndDate.Size = new System.Drawing.Size(200, 20);
            this.DtpEndDate.TabIndex = 4;
            // 
            // posDataSet
            // 
            this.posDataSet.DataSetName = "posDataSet";
            this.posDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // salesViewReportBindingSource
            // 
            this.salesViewReportBindingSource.DataMember = "SalesViewReport";
            this.salesViewReportBindingSource.DataSource = this.posDataSet;
            // 
            // salesViewReportTableAdapter
            // 
            this.salesViewReportTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BrandTableAdapter = null;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.ProductDiscountsTableAdapter = null;
            this.tableAdapterManager.ProductTableAdapter = null;
            this.tableAdapterManager.PurchaseDetailTableAdapter = null;
            this.tableAdapterManager.PurchaseTableAdapter = null;
            this.tableAdapterManager.SaleDetailsTableAdapter = null;
            this.tableAdapterManager.SaleTableAdapter = null;
            this.tableAdapterManager.SellerTableAdapter = null;
            this.tableAdapterManager.SpreadDetailsTableAdapter = null;
            this.tableAdapterManager.SpreadTableAdapter = null;
            this.tableAdapterManager.StoresTableAdapter = null;
            this.tableAdapterManager.SupplierTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = trgpos.posDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserTableAdapter = null;
            // 
            // salesViewReportBindingNavigator
            // 
            this.salesViewReportBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.salesViewReportBindingNavigator.BindingSource = this.salesViewReportBindingSource;
            this.salesViewReportBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.salesViewReportBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.salesViewReportBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.salesViewReportBindingNavigatorSaveItem});
            this.salesViewReportBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.salesViewReportBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.salesViewReportBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.salesViewReportBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.salesViewReportBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.salesViewReportBindingNavigator.Name = "salesViewReportBindingNavigator";
            this.salesViewReportBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.salesViewReportBindingNavigator.Size = new System.Drawing.Size(889, 25);
            this.salesViewReportBindingNavigator.TabIndex = 1;
            this.salesViewReportBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // salesViewReportBindingNavigatorSaveItem
            // 
            this.salesViewReportBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.salesViewReportBindingNavigatorSaveItem.Enabled = false;
            this.salesViewReportBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("salesViewReportBindingNavigatorSaveItem.Image")));
            this.salesViewReportBindingNavigatorSaveItem.Name = "salesViewReportBindingNavigatorSaveItem";
            this.salesViewReportBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.salesViewReportBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // salesViewReportGridControl
            // 
            this.salesViewReportGridControl.DataSource = this.salesViewReportBindingSource;
            this.salesViewReportGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.salesViewReportGridControl.Location = new System.Drawing.Point(0, 0);
            this.salesViewReportGridControl.MainView = this.gridView1;
            this.salesViewReportGridControl.Name = "salesViewReportGridControl";
            this.salesViewReportGridControl.Size = new System.Drawing.Size(889, 403);
            this.salesViewReportGridControl.TabIndex = 0;
            this.salesViewReportGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSaleDate,
            this.colSellerCode,
            this.colSellerName,
            this.colStoreCode,
            this.colStoreName,
            this.colPoNumber,
            this.colProductCode,
            this.colProduct,
            this.colBrand,
            this.colDiscountA,
            this.colDiscountB,
            this.colDiscountSales,
            this.colGrossPrice,
            this.colQuantity});
            this.gridView1.GridControl = this.salesViewReportGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colSaleDate
            // 
            this.colSaleDate.FieldName = "SaleDate";
            this.colSaleDate.Name = "colSaleDate";
            this.colSaleDate.Visible = true;
            this.colSaleDate.VisibleIndex = 0;
            // 
            // colSellerCode
            // 
            this.colSellerCode.FieldName = "SellerCode";
            this.colSellerCode.Name = "colSellerCode";
            this.colSellerCode.Visible = true;
            this.colSellerCode.VisibleIndex = 1;
            // 
            // colSellerName
            // 
            this.colSellerName.FieldName = "SellerName";
            this.colSellerName.Name = "colSellerName";
            this.colSellerName.OptionsColumn.ReadOnly = true;
            this.colSellerName.Visible = true;
            this.colSellerName.VisibleIndex = 2;
            // 
            // colStoreCode
            // 
            this.colStoreCode.FieldName = "StoreCode";
            this.colStoreCode.Name = "colStoreCode";
            this.colStoreCode.Visible = true;
            this.colStoreCode.VisibleIndex = 3;
            // 
            // colStoreName
            // 
            this.colStoreName.FieldName = "StoreName";
            this.colStoreName.Name = "colStoreName";
            this.colStoreName.OptionsColumn.ReadOnly = true;
            this.colStoreName.Visible = true;
            this.colStoreName.VisibleIndex = 4;
            // 
            // colPoNumber
            // 
            this.colPoNumber.FieldName = "PoNumber";
            this.colPoNumber.Name = "colPoNumber";
            this.colPoNumber.Visible = true;
            this.colPoNumber.VisibleIndex = 5;
            // 
            // colProductCode
            // 
            this.colProductCode.FieldName = "ProductCode";
            this.colProductCode.Name = "colProductCode";
            this.colProductCode.OptionsColumn.ReadOnly = true;
            this.colProductCode.Visible = true;
            this.colProductCode.VisibleIndex = 6;
            // 
            // colProduct
            // 
            this.colProduct.FieldName = "Product";
            this.colProduct.Name = "colProduct";
            this.colProduct.OptionsColumn.ReadOnly = true;
            this.colProduct.Visible = true;
            this.colProduct.VisibleIndex = 7;
            // 
            // colBrand
            // 
            this.colBrand.FieldName = "Brand";
            this.colBrand.Name = "colBrand";
            this.colBrand.OptionsColumn.ReadOnly = true;
            this.colBrand.Visible = true;
            this.colBrand.VisibleIndex = 8;
            // 
            // colDiscountA
            // 
            this.colDiscountA.FieldName = "DiscountA";
            this.colDiscountA.Name = "colDiscountA";
            this.colDiscountA.Visible = true;
            this.colDiscountA.VisibleIndex = 9;
            // 
            // colDiscountB
            // 
            this.colDiscountB.FieldName = "DiscountB";
            this.colDiscountB.Name = "colDiscountB";
            this.colDiscountB.Visible = true;
            this.colDiscountB.VisibleIndex = 10;
            // 
            // colDiscountSales
            // 
            this.colDiscountSales.FieldName = "DiscountSales";
            this.colDiscountSales.Name = "colDiscountSales";
            this.colDiscountSales.Visible = true;
            this.colDiscountSales.VisibleIndex = 11;
            // 
            // colGrossPrice
            // 
            this.colGrossPrice.FieldName = "GrossPrice";
            this.colGrossPrice.Name = "colGrossPrice";
            this.colGrossPrice.Visible = true;
            this.colGrossPrice.VisibleIndex = 12;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 13;
            // 
            // BtnPreview
            // 
            this.BtnPreview.Location = new System.Drawing.Point(722, 85);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(75, 23);
            this.BtnPreview.TabIndex = 5;
            this.BtnPreview.Text = "Preview";
            this.BtnPreview.UseVisualStyleBackColor = true;
            this.BtnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // BtnExport
            // 
            this.BtnExport.Location = new System.Drawing.Point(803, 86);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(107, 23);
            this.BtnExport.TabIndex = 6;
            this.BtnExport.Text = "Export To Excel";
            this.BtnExport.UseVisualStyleBackColor = true;
            this.BtnExport.Click += new System.EventHandler(this.BtnExport_Click);
            // 
            // SalesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.salesViewReportBindingNavigator);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "SalesReport";
            this.Size = new System.Drawing.Size(889, 539);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportBindingNavigator)).EndInit();
            this.salesViewReportBindingNavigator.ResumeLayout(false);
            this.salesViewReportBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.salesViewReportGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.Label LblSalesReportTitle;
        private System.Windows.Forms.DateTimePicker DtpEndDate;
        private System.Windows.Forms.DateTimePicker DtpStartDate;
        private System.Windows.Forms.Label LblEndDate;
        private System.Windows.Forms.Label LblStartDate;
        private DevExpress.XtraGrid.GridControl salesViewReportGridControl;
        private System.Windows.Forms.BindingSource salesViewReportBindingSource;
        private posDataSet posDataSet;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellerCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSellerName;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colStoreName;
        private DevExpress.XtraGrid.Columns.GridColumn colPoNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colProductCode;
        private DevExpress.XtraGrid.Columns.GridColumn colProduct;
        private DevExpress.XtraGrid.Columns.GridColumn colBrand;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountA;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountB;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscountSales;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private posDataSetTableAdapters.SalesViewReportTableAdapter salesViewReportTableAdapter;
        private posDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator salesViewReportBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton salesViewReportBindingNavigatorSaveItem;
        private System.Windows.Forms.Button BtnExport;
        private System.Windows.Forms.Button BtnPreview;
    }
}
