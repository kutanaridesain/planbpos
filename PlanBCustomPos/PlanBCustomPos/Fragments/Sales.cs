﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;

namespace trgpos.Fragments
{
    public partial class Sales : UserControl
    {
        private int minQuantityForDiscountB;
        private double discountB;
        int selectedStock = 0;
        private Boolean isSpreading = false;
        
        public Sales()
        {
            InitializeComponent();
            minQuantityForDiscountB = 0;
            discountB = 0;

            LblTotal.Text = "0";
            this.storesTableAdapter.Fill(this.posDataSet.Stores);
            this.sellerTableAdapter.Fill(this.posDataSet.Seller);
            this.saleTableAdapter.FillBy(this.posDataSet.Sale);
            this.productTableAdapter.FillByCustom(this.posDataSet.Product);

            CbProductName_SelectionChangeCommitted(CbProduct, null);

        }

        private void saleBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.saleBindingSource.EndEdit();
                this.saleTableAdapter.Update(this.posDataSet);
               
                MessageBox.Show("Data saved successfully.");
            }catch(Exception ex){
                MessageBox.Show("Error " + ex.Message);
            }

        }


        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            DateTime theDate = DateTime.Now;
            string poNumber = theDate.ToString("yyMMddHms");
            poNumberTextBox.Text = poNumber;
            saleDateDateTimePicker.Value = DateTime.Now;
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.saleTableAdapter.FillBy(this.posDataSet.Sale);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void bindingNavigatorSaveItem1_Click(object sender, EventArgs e)
        {
            int id = 0;
            int.TryParse(idTextBox1.Text, out id);
                     
            try
            {
                this.Validate();
                
                int unitQuantity = 0;
                int.TryParse(TxtUnitQuantity.Text, out unitQuantity);

                int itemQuantity = 0;
                int.TryParse(TxtItemQuantity.Text, out itemQuantity);

                DataRowView selectedDataRow = (DataRowView)CbProduct.SelectedItem;

                int selectedIndex = 0;
                selectedIndex = CbProduct.SelectedIndex;
                if (selectedDataRow != null)
                {
                    int itemPerUnit = 0;
                    int.TryParse(selectedDataRow["ItemPerUnit"].ToString(), out itemPerUnit);
                    if (itemPerUnit > 0)
                    {
                        if (unitQuantity > 0)
                        {
                            unitQuantity = unitQuantity * itemPerUnit;  
                        }
                    }

                   
                }

                TxtQuantity.Text = (unitQuantity + itemQuantity).ToString();

                int txtQuantity;
                int.TryParse(TxtQuantity.Value.ToString(), out txtQuantity);

                if (txtQuantity <= 0)
                {
                    MessageBox.Show("The Quantity must be greather than 0.");
                    return;
                }
        
                this.saleDetailsBindingSource.EndEdit();

                
                this.saleDetailsTableAdapter.Update(this.posDataSet);
                ShowTotal();
                this.productTableAdapter.FillByCustom(this.posDataSet.Product);
               
                CbProduct.SelectedIndex = selectedIndex;
                this.saleDetailsTableAdapter.FillBy(this.posDataSet.SaleDetails, poNumberTextBox.Text);


                CbProductName.Enabled = false;

                MessageBox.Show("Data saved successfully.");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

            //if (this.posDataSet.SaleDetails != null)
            //{
                this.saleDetailsTableAdapter.FillBy(this.posDataSet.SaleDetails, poNumberTextBox.Text);
            //}

            for (int i = 0; i < gridView2.RowCount; i++)
            {
                if (gridView2.IsRowSelected(i))
                {

                    string poNumber = String.Empty;
                    string storeCode = String.Empty;
                    isSpreading = false;
                    if (gridView2.GetRowCellValue(i, "PoNumber") != null)
                    {
                        poNumber = gridView2.GetRowCellValue(i, "PoNumber").ToString();
                        storeCode = gridView2.GetRowCellValue(i, "StoreCode").ToString();

                        //is spreading??
                        if (storesTableAdapter.IsSpreading(storeCode)!= null)
                        {
                            isSpreading = true;
                        }
                    }

                    
                    TxtPoNumber.Text = poNumber;
                    break;
                }
            }

            ShowTotal();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.saleTableAdapter.SearhByPo(this.posDataSet.Sale, poToolStripTextBox.Text);
        }

        private void SetUnitItemQuantity(int quantity, int itemPerUnit)
        {
            if (quantity > 0)
            {
                int unitQuantity = quantity / itemPerUnit;
                int itemQuantity = quantity % itemPerUnit;
                if (unitQuantity >= 0)
                {
                    TxtUnitQuantity.Text = unitQuantity.ToString();
                }

                if (itemQuantity >= 0)
                {
                    TxtItemQuantity.Text = itemQuantity.ToString();
                }
            }

        }

        //private void CbProduct_SelectedValueChanged(object sender, EventArgs e)
        private void CbProductName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //bindingNavigatorAddNewItem1.PerformClick();
            TxtDiscountA.Text = "0";
            TxtDiscountB.Text = "0";
            TxtDiscountSales.Text = "0";
            TxtUnitQuantity.Text = "0";
            TxtItemQuantity.Text = "0";
            
            ComboBox selectedProduct = (ComboBox)sender;
            DataRowView selectedProductDataRow = (DataRowView)selectedProduct.SelectedItem;

            if (selectedProductDataRow != null)
            {
                
                long id = 0;
                long.TryParse(selectedProductDataRow["Id"].ToString(), out id);

                if (gridView1.RowCount > 1)
                {
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        if (gridView1.GetRowCellValue(i, "ProductId") == null)
                            continue;

                        int productId = 0;
                        int.TryParse(gridView1.GetRowCellValue(i, "ProductId").ToString(), out productId);
                        if (id == productId)
                        {
                            this.saleDetailsTableAdapter.FillBy(this.posDataSet.SaleDetails, poNumberTextBox.Text);
                            gridView1.FocusedRowHandle = i;
                        }
                    }
                }

                int itemPerUnit = 0;
                int.TryParse(selectedProductDataRow["ItemPerUnit"].ToString(), out itemPerUnit);
                double price = 0;
                double.TryParse(selectedProductDataRow["PricePerUnit"].ToString(), out price);

                if(selectedProductDataRow["Stock"] != DBNull.Value ){
                    int.TryParse(selectedProductDataRow["Stock"].ToString(), out selectedStock);
                }

                TxtGrossPrice.Text = String.Format("{0:0,0.00}", price);// (price).ToString();

                //check quantity
                int quantity = 0;
                int.TryParse(TxtQuantity.Text, out quantity);


                SetUnitItemQuantity(quantity, itemPerUnit);

                posDataSet.ProductDiscountsDataTable pd = this.productDiscountsTableAdapter.GetDataByProductId(id);
                
                if (pd != null)
                {
                    minQuantityForDiscountB = 0;

                    if (pd.Count > 0)
                    {
                        TxtDiscountA.Text = pd[0].DiscountA.ToString();

                        if (pd[0].IsQuantityNull() == false)
                        {
                            int.TryParse(pd[0].Quantity.ToString(), out minQuantityForDiscountB);
                        }


                        discountB = 0;
                        if (pd[0].IsDiscountBNull() == false && isSpreading == false)
                        {
                            double.TryParse(pd[0].DiscountB.ToString(), out discountB);
                        }

                        if (quantity >= minQuantityForDiscountB)
                        {
                            TxtDiscountB.Text = discountB.ToString();
                        }
                    }
                }
            }

        }

        private void bindingNavigatorAddNewItem1_Click(object sender, EventArgs e)
        {
            TxtPoNumber.Text = poNumberTextBox.Text;
            TxtDiscountA.Text = "0";
            TxtDiscountB.Text = "0";
            TxtDiscountSales.Text = "0";
            TxtUnitQuantity.Text = "0";
            TxtItemQuantity.Text = "0";
            CbProductName.Enabled = true;
        }

        private void TxtQuantity_EditValueChanged(object sender, EventArgs e)
        {
            int quantity = 0;

            int.TryParse(TxtQuantity.Value.ToString(), out quantity);
            if (quantity < 0)
                quantity = 0;

            //if (minQuantityForDiscountB > 0 && discountB > 0)
            if (minQuantityForDiscountB > 0 )
            {
                if (quantity >= minQuantityForDiscountB)
                {
                    TxtDiscountB.Text = discountB.ToString();
                }
                else
                {
                    TxtDiscountB.Text = "0";
                }
            }
            else
            {
                TxtDiscountB.Text = "0";
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("abc");
            ShowTotal();
        }

        private void saleDetailsGridControl_Load(object sender, EventArgs e)
        {
            //int milliseconds = 3000;
            //System.Threading.Thread.Sleep(milliseconds);
            ShowTotal();
        }

        private void ShowTotal(){
            double total = 0;
            for (int i=0; i < gridView1.RowCount; i++)
            {
                
                if (gridView1.GetRowCellValue(i, "GrossPrice") != null)
                {
                    double t = 0;
                    double.TryParse(gridView1.GetRowCellValue(i, "GrossPrice").ToString (), out t);
                    int q = 0;
                    int.TryParse(gridView1.GetRowCellValue(i, "Quantity").ToString(), out q);
                    if (q == 0)
                    {
                        q = 1;
                    }

                    int qq = 0;
                    int id = 0;
                    int.TryParse(gridView1.GetRowCellValue(i, "ProductId").ToString(), out id);
                    DataTable dtProduct = productTableAdapter.GetDataById(id);
                    if (dtProduct != null)
                    {
                        foreach (DataRow rowProduct in dtProduct.Rows)
                        {
                            int.TryParse(rowProduct["ItemPerUnit"].ToString(), out qq);
                        }
                    }

                    if (qq < 1)
                    {
                        qq = 1;
                    }

                    //NOTE: TIDAK ADA SATUAN BESAR UNTUK SEMENTARA
                    //MAKA qq diset = 1;
                    qq = 1;

                    total += t * q * qq ;
                }
            }

            double discountSales = 0;
            if (total > Properties.Settings.Default.MinForDiscountC && isSpreading == false)
            {
                discountSales = Properties.Settings.Default.DiscountC;
            }

            this.saleDetailsTableAdapter.UpdateDiscountSales(discountSales, poNumberTextBox.Text);
            LblTotal.Text = "Total Penjualan : Rp. " + String.Format("{0:0,0.0}",total);

     
            if (!String.IsNullOrEmpty(TxtQuantity.Text))
            {
                int quantity = 0;
                int.TryParse(TxtQuantity.Text, out quantity);
                if (quantity > 0)
                {
                    DataRowView selectedDataRow = (DataRowView)CbProduct.SelectedItem;
                    if (selectedDataRow != null)
                    {
                        int itemPerUnit = 0;
                        int.TryParse(selectedDataRow["ItemPerUnit"].ToString(), out itemPerUnit);
                        if (itemPerUnit > 0)
                        {
                            SetUnitItemQuantity(quantity, itemPerUnit);
                        }
                    }
                }
            }
        }

        private void PrintButton_Click(object sender, EventArgs e)
        {
            BrickGraphics gr = printingSystem.Graph;
            BrickStringFormat bsf = new BrickStringFormat(StringAlignment.Near, StringAlignment.Center);
            gr.StringFormat = bsf;
            gr.BorderColor = SystemColors.ControlDark;

            // Declare bricks.
            TextBrick textbrick;
            
            //CheckBoxBrick checkbrick;
            //Brick brick;
            PageInfoBrick pageinfobr;

            printingSystem.PageSettings.Landscape = false;
            printingSystem.Begin();

            gr.Modifier = BrickModifier.Detail;
            gr.BeginUnionRect();

            int y = 5;
            int x = 5;
            gr.Font = new Font("Arial", 13, FontStyle.Bold);
            textbrick = gr.DrawString("Faktur Penjualan", Color.Black, new RectangleF(x, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 25;
            gr.Font = new Font("Arial", 11, FontStyle.Regular);
            textbrick = gr.DrawString("Dicetak oleh : " + Properties.Settings.Default.DicetakOleh, Color.Black, new RectangleF(x, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            y += 25;

            gr.Font = new Font("Arial", 8, FontStyle.Regular);
            textbrick = gr.DrawString(Properties.Settings.Default.NamaPerusahaan, Color.Black, new RectangleF(x, y, 600, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


            y += 20;
            textbrick = gr.DrawString(Properties.Settings.Default.Alamat, Color.Black, new RectangleF(x, y, 600, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString("Telp : " + Properties.Settings.Default.Telepon, Color.Black, new RectangleF(x + 300, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


            y += 20;
            textbrick = gr.DrawString("NPWP : " + Properties.Settings.Default.NPWP, Color.Black, new RectangleF(x, y, 600, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString("Pengukuhan: " + Properties.Settings.Default.Pengukuhan, Color.Black, new RectangleF(x + 300, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 30;
            string storeName = string.Empty;
            string storeLocation = string.Empty;
            DataRowView  drvStore = (DataRowView)storeCodeComboBox.SelectedItem;
            if (drvStore != null)
            {
                storeName = drvStore["Name"].ToString();
                storeLocation = drvStore["Address"].ToString();
            }
            textbrick = gr.DrawString("Nama Toko ", Color.Black, new RectangleF(x, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(": " + storeName, Color.Black, new RectangleF(x + 75, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString("NO PO", Color.Black, new RectangleF(x + 300, y, 300, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(": " + poNumberTextBox.Text, Color.Black, new RectangleF(x + 350, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 15;
            string salesName = string.Empty;
            DataRowView drvSales = (DataRowView)sellerCodeComboBox.SelectedItem;
            if (drvStore != null)
            {
                salesName = drvSales["Name"].ToString();
            }
            textbrick = gr.DrawString("Tanggal", Color.Black, new RectangleF(x, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString(": " + saleDateDateTimePicker.Value.ToShortDateString (), Color.Black, new RectangleF(x + 75, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString("Sales", Color.Black, new RectangleF(x + 300, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString(": " + salesName, Color.Black, new RectangleF(x + 350, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 15;
            textbrick = gr.DrawString("Lokasi", Color.Black, new RectangleF(x, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString(": " + storeLocation, Color.Black, new RectangleF(x + 75, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString("Dikirim", Color.Black, new RectangleF(x + 300, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString(": Gideon Baru", Color.Black, new RectangleF(x + 350, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 20;
            //brick = gr.DrawRect(new RectangleF(5, y, 620, 5), BorderSide.Bottom, Color.Transparent, Color.Black);

            //top header
            gr.Font = new Font("Arial", 7, FontStyle.Regular);
            int[] topHeaderWidth = { 40, 70, 250, 90, 75, 110, 105 };
            string[] topHeaderTitle = { "No.", "Kode", "Nama Barang", 
                                       "Harga", "Jumlah SB/SK", "Potongan A/B/C", 
                                       "Total" };
            y += 20;
            int topHeaderX = 0;
            for (int i = 0; i < topHeaderWidth.Length; i++)
            {
                textbrick = gr.DrawString(topHeaderTitle[i], Color.Black, new RectangleF(topHeaderX, y, topHeaderWidth[i], 20), BorderSide.All);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                topHeaderX += topHeaderWidth[i];
            }

            //bottom header
            int[] bottomHeaderWidth = { 40, 70, 250, 90, 40, 35, 40, 35, 35, 105 };
            string[] bottomHeaderTitle = { "", "", "", "", 
                                             "S B","S K", "Disk A", "Disk B", "Disk C", 
                                       "" };
            y += 20;
            int bottomHeaderX = 0;
            for (int i = 0; i < bottomHeaderWidth.Length; i++)
            {
                textbrick = gr.DrawString(bottomHeaderTitle[i], Color.Black, new RectangleF(bottomHeaderX, y, bottomHeaderWidth[i], 20), BorderSide.All);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                bottomHeaderX += bottomHeaderWidth[i];
            }

            //get the data
            DataTable dtSaleDetail = saleDetailsTableAdapter.GetDataByPoNumber(poNumberTextBox.Text);

            double totalGrossPrice = 0;
            double totalDiscountA = 0;
            double totalDiscountB = 0;
            double totalDiscountSales = 0;

            if (dtSaleDetail.Rows.Count > 0)
            {
                gr.Font = new Font("Arial", 7, FontStyle.Regular);
                int index = 1;
                //for (int i = 0; i < 40; i++)
                //{
                foreach (DataRow row in dtSaleDetail.Rows)
                {
                    y += 20;
                    int itemJ = 0;

                    long id = 0;
                    if (row["ProductId"] != null)
                    {
                        long.TryParse(row["ProductId"].ToString(), out id);
                    }
                    if (id > 0)
                    {
                        DataTable dtProduct = productTableAdapter.GetDataById(id);
                        if (dtProduct != null)
                        {
                            foreach (DataRow rowProduct in dtProduct.Rows)
                            {
                                int itemQuantity = 0;
                                int.TryParse(rowProduct["ItemPerUnit"].ToString(), out itemQuantity);

                                int unitQuantity = 0;
                                int.TryParse(row["Quantity"].ToString(), out unitQuantity);

                                double grossPrice = 0;
                                double.TryParse(row["GrossPrice"].ToString(), out grossPrice);

                                double discountA = 0;
                                double.TryParse(row["DiscountA"].ToString(), out discountA);
                                if (discountA > 0)
                                {
                                    totalDiscountA += (discountA / 100) * (grossPrice * unitQuantity);
                                }

                                double discountB = 0;
                                double.TryParse(row["DiscountB"].ToString(), out discountB);
                                if (discountB > 0)
                                {
                                    totalDiscountB += (discountB / 100) * (grossPrice * unitQuantity);
                                }

                                double discountSales = 0;
                                double.TryParse(row["DiscountSales"].ToString(), out discountSales);
                                totalDiscountSales = discountSales;

                                double subTotal = (unitQuantity > 0 ? unitQuantity : 0) * grossPrice;

                                totalGrossPrice += subTotal;

                                int quantityA = unitQuantity / itemQuantity;
                                int quantityB = unitQuantity % itemQuantity;

                                for (int j = 0; j < bottomHeaderWidth.Length; j++)
                                {
                                    string text = string.Empty;
                                    switch (j)
                                    {
                                        case 0:
                                            text = "  " + (index).ToString();
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            itemJ += bottomHeaderWidth[j];
                                            break;
                                        case 1:
                                            text = rowProduct["Code"].ToString();
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 2:
                                            text = row["ProductName"].ToString();//string.Format("{0:0.0}", Math.Truncate(grossPrice * 10) / 10);//rowProduct["Name"].ToString();
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 3:
                                            text = grossPrice.ToString();//String.Format("{0:0,0.0}", row["GrossPrice"]);
                                            text = String.Format("{0:0,0.0}", grossPrice);
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 4:
                                            text = quantityA.ToString();
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 5:
                                            text = quantityB.ToString();
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 6:
                                            text = string.Format("{0:0.0}", Math.Truncate(discountA * 10) / 10);
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 7:
                                            text = string.Format("{0:0.0}", Math.Truncate(discountB * 10) / 10);
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 8:
                                            text = string.Format("{0:0.0}", Math.Truncate(discountSales * 10) / 10);
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        case 9:
                                            text = String.Format("{0:0,0.0}", subTotal);
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                        default:
                                            textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, bottomHeaderWidth[j], 20), BorderSide.All);
                                            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                                            textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                            textbrick.Padding = 3;
                                            itemJ += bottomHeaderWidth[j];
                                            textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                            break;
                                    }
                                }
                            }
                        }
                        // }

                        index += 1;
                    }
                }
            }

            //TOTAL
            y += 50;
            int xTotal = 510;
            textbrick = gr.DrawString("JUMLAH GROSS :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(String.Format("{0:0,0.0}", totalGrossPrice), Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;

            y += 15;
            double dDiscountA = Math.Truncate(totalDiscountA * 10) / 10;
            textbrick = gr.DrawString("JUMLAH DISKON A :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(string.Format("{0:0.0}", Math.Truncate(totalDiscountA * 10) / 10), Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;

            y += 15;
            double dDiscountB = Math.Truncate(totalDiscountB * 10) / 10;
            textbrick = gr.DrawString("JUMLAH DISKON B :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(string.Format("{0:0.0}", Math.Truncate(totalDiscountB * 10) / 10), Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;

            y += 15;
            double dDiscountC = 0;
            if (totalDiscountSales > 0)
            {
                dDiscountC = totalDiscountSales/100*totalGrossPrice;// Math.Truncate(totalDiscountSales * 10) / 10;
                //dDiscountC = Math.Truncate(dDiscountC * 10) / 10;
            }
            textbrick = gr.DrawString("JUMLAH DISKON C :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            textbrick = gr.DrawString(string.Format("{0:0.0}", Math.Truncate(dDiscountC * 10) / 10), Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;


            y += 15;
            textbrick = gr.DrawString("DISKON :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            double td = (totalDiscountA + totalDiscountB + dDiscountC);
            textbrick = gr.DrawString(String.Format("{0:0,0.0}", td),
                Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;

            y += 15;
            textbrick = gr.DrawString("JUMLAH :", Color.Black, new RectangleF(xTotal, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            double t = totalGrossPrice - td;
            textbrick = gr.DrawString(String.Format("{0:0,0.0}", t),
                Color.Black, new RectangleF(xTotal + 110, y, 100, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Far;


            y += 80;

            textbrick = gr.DrawString("Penerima", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("Admin", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("Hormat Kami", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;


            y += 70;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            // Finish the creation of a non-separable group of bricks.
            gr.EndUnionRect();

            // Create a MarginalHeader section.
            gr.Modifier = BrickModifier.MarginalHeader;
            RectangleF r = RectangleF.Empty;
            r.Height = 40;
            gr.BackColor = Color.White;

            // Display the DevExpress text string.
            //SizeF sz = gr.MeasureString("trigniter");

            // Display the PageInfoBrick containing date-time information. Date-time information is displayed
            // in the left part of the MarginalHeader section using the FullDateTimePattern.
            pageinfobr = gr.DrawPageInfo(PageInfo.DateTime, "Tanggal Cetak: {0:dd-MM-yyyy}", Color.Black, r, BorderSide.None);
            pageinfobr.Alignment = BrickAlignment.Near;

            // Display the PageInfoBrick containing the page number among total pages. The page number
            // is displayed in the right part of the MarginalHeader section.
            pageinfobr = gr.DrawPageInfo(PageInfo.NumberOfTotal, "Halaman {0} dari {1}", Color.Black, r,
                 BorderSide.None);
            pageinfobr.Alignment = BrickAlignment.Far;

            printingSystem.PageSettings.LeftMargin = 40;
            printingSystem.PageSettings.RightMargin = 0;

            printingSystem.End();
            printingSystem.PreviewFormEx.Show();
            //printingSystem.Print();
        }

        private void TxtGrossPrice_EditValueChanged(object sender, EventArgs e)
        {
            double val = 0;
            double.TryParse (TxtGrossPrice.Text, out val);
            TxtGrossPrice.Text = String.Format("{0:0,0.0}", val);
        }
        
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            BrickGraphics gr = printingSystem.Graph;
            BrickStringFormat bsf = new BrickStringFormat(StringAlignment.Near, StringAlignment.Center);
            gr.StringFormat = bsf;
            gr.BorderColor = SystemColors.ControlDark;

            // Declare bricks.
            TextBrick textbrick;
            //CheckBoxBrick checkbrick;
            //Brick brick;
            PageInfoBrick pageinfobr;

            printingSystem.PageSettings.Landscape = false;
            printingSystem.Begin();

            gr.Modifier = BrickModifier.Detail;
            gr.BeginUnionRect();

            int y = 5;
            int x = 5;
            gr.Font = new Font("Arial", 13, FontStyle.Bold);
            textbrick = gr.DrawString("Dokumen Pengiriman Barang", Color.Black, new RectangleF(x, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


            y += 25;

            gr.Font = new Font("Arial", 8, FontStyle.Regular);
            textbrick = gr.DrawString(Properties.Settings.Default.NamaPerusahaan, Color.Black, new RectangleF(x, y, 600, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


            y += 20;
            textbrick = gr.DrawString(Properties.Settings.Default.Alamat, Color.Black, new RectangleF(x, y, 600, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString("Telp : " + Properties.Settings.Default.Telepon, Color.Black, new RectangleF(x + 200, y, 400, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;


            textbrick = gr.DrawString("Tanggal", Color.Black, new RectangleF(x + 450, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            textbrick = gr.DrawString(": " + saleDateDateTimePicker.Value.ToShortDateString (), Color.Black, new RectangleF(x + 500, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;

            y += 20;

            //top header
            gr.Font = new Font("Arial", 7, FontStyle.Regular);
            int[] topHeaderWidth = { 40, 110, 480, 110 };
            string[] topHeaderTitle = { "No.", "Kode", "Nama Barang", "Jumlah" };

            y += 20;
            int topHeaderX = 0;
            for (int i = 0; i < topHeaderWidth.Length; i++)
            {
                textbrick = gr.DrawString(topHeaderTitle[i], Color.Black, new RectangleF(topHeaderX, y, topHeaderWidth[i], 20), BorderSide.All);
                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                topHeaderX += topHeaderWidth[i];
            }

            //content
            int[] contentWidth = { 40, 110, 480, 110 };

            DataTable dtSaleDetail = salesViewReportTableAdapter.GetDataSummaryByDate(saleDateDateTimePicker.Value, saleDateDateTimePicker.Value);


            if (dtSaleDetail.Rows.Count > 0)
            {
                gr.Font = new Font("Arial", 7, FontStyle.Regular);
                int index = 1;
                foreach (DataRow row in dtSaleDetail.Rows)
                {
                    y += 20;
                    int itemJ = 0;
                    for (int j = 0; j < contentWidth.Length; j++)
                    {
                        string text = string.Empty;
                        switch (j)
                        {
                            case 0:
                                text = "  " + (index).ToString();
                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, contentWidth[j], 20), BorderSide.All);
                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                textbrick.Padding = 3;
                                itemJ += contentWidth[j];
                                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                break;
                            case 1:
                                text = row["ProductCode"].ToString();
                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, contentWidth[j], 20), BorderSide.All);
                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                textbrick.Padding = 3;
                                itemJ += contentWidth[j];
                                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                break;
                            case 2:
                                text = row["Product"].ToString();
                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, contentWidth[j], 20), BorderSide.All);
                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Near);
                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                textbrick.Padding = 3;
                                itemJ += contentWidth[j];
                                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                break;
                            case 3:
                                text = row["Quantity"].ToString();
                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, contentWidth[j], 20), BorderSide.All);
                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Far);
                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                textbrick.Padding = 3;
                                itemJ += contentWidth[j];
                                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                break;
                            default:
                                textbrick = gr.DrawString(text, Color.Black, new RectangleF(itemJ, y, contentWidth[j], 20), BorderSide.All);
                                textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
                                textbrick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
                                textbrick.Padding = 3;
                                itemJ += contentWidth[j];
                                textbrick.BorderDashStyle = BorderDashStyle.Dot;
                                break;
                        }
                    }
                    index += 1;
                }
            }



            y += 80;

            textbrick = gr.DrawString("Penerima", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("Admin", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("Hormat Kami", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;


            y += 70;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(0, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(250, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            textbrick = gr.DrawString("(............................................)", Color.Black, new RectangleF(500, y, 200, 20), BorderSide.None);
            textbrick.StringFormat = textbrick.StringFormat.ChangeAlignment(StringAlignment.Center);
            textbrick.HorzAlignment = DevExpress.Utils.HorzAlignment.Center;

            // Finish the creation of a non-separable group of bricks.
            gr.EndUnionRect();

            // Create a MarginalHeader section.
            gr.Modifier = BrickModifier.MarginalHeader;
            RectangleF r = RectangleF.Empty;
            r.Height = 40;
            gr.BackColor = Color.White;

            // Display the DevExpress text string.
            //SizeF sz = gr.MeasureString("trigniter");

            // Display the PageInfoBrick containing date-time information. Date-time information is displayed
            // in the left part of the MarginalHeader section using the FullDateTimePattern.
            pageinfobr = gr.DrawPageInfo(PageInfo.DateTime, "Tanggal Cetak: {0:dd-MM-yyyy}", Color.Black, r, BorderSide.None);
            pageinfobr.Alignment = BrickAlignment.Near;

            // Display the PageInfoBrick containing the page number among total pages. The page number
            // is displayed in the right part of the MarginalHeader section.
            pageinfobr = gr.DrawPageInfo(PageInfo.NumberOfTotal, "Halaman {0} dari {1}", Color.Black, r,
                 BorderSide.None);
            pageinfobr.Alignment = BrickAlignment.Far;

            printingSystem.PageSettings.LeftMargin = 40;
            printingSystem.PageSettings.RightMargin = 0;

            printingSystem.End();
            printingSystem.PreviewFormEx.Show();
            //printingSystem.Print();
        }

        /*private void CbProductName_SelectionChangeCommitted(object sender, EventArgs e)
        {

        }*/

    }
}