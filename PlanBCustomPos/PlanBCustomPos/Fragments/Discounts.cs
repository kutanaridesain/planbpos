﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Discounts : UserControl
    {
        public Discounts()
        {
            InitializeComponent();
        }

        private void productDiscountsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            Validate();
            productDiscountsBindingSource.EndEdit();
            tableAdapterManager.UpdateAll(posDataSet);

            if (String.IsNullOrEmpty(TxtSearch.Text))
            {
                productDiscountsTableAdapter.FillBy(posDataSet.ProductDiscounts);
            }
            else
            {
                String cleanText = TxtSearch.Text.Trim();
                productDiscountsTableAdapter.FillByTerm(posDataSet.ProductDiscounts, cleanText);
            }
        }

        private void Discounts_Load(object sender, EventArgs e)
        {
            productDiscountsTableAdapter.FillBy(posDataSet.ProductDiscounts);
            productTableAdapter.FillByCustom(posDataSet.Product);
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                productDiscountsTableAdapter.FillBy(posDataSet.ProductDiscounts);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            productIdComboBox.Text = codeNameComboBox.SelectedValue.ToString();
            discountASpinEdit.Text = "0";
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            tableAdapterManager.UpdateAll(posDataSet);
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            TextBox txtbox = (TextBox)sender;
            String cleanText = txtbox.Text.Trim();
            productDiscountsTableAdapter.FillByTerm(posDataSet.ProductDiscounts, cleanText);
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            TxtSearch.Text = String.Empty;
            productDiscountsTableAdapter.FillBy(posDataSet.ProductDiscounts);
        }
    }
}
