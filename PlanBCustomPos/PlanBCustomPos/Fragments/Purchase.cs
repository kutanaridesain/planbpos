﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using DevExpress.XtraEditors;

namespace trgpos.Fragments
{
    public partial class Purchase : DevExpress.XtraEditors.XtraUserControl
    {
        public Purchase()
        {
            InitializeComponent();
            supplierTableAdapter.Fill(posDataSet.Supplier);

            productTableAdapter.FillByCustom(posDataSet.Product);
            
            purchaseTableAdapter.Fill(posDataSet.Purchase);
            //purchaseDetailTableAdapter.Fill(posDataSet.PurchaseDetail);

            productIdComboBox_SelectionChangeCommitted(productIdComboBox, null);
        }

        private void purchaseBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.purchaseBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.posDataSet);
                //this.purchaseTableAdapter.Update(this.posDataSet.Purchase);
                MessageBox.Show("Data saved successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            idSpinEdit.Text = "-1";
            supplierIdComboBox.SelectedValue = 0;
            codeTextBox.Text = DateTime.Now.ToString("yyMMddHms");
            purchaseDateDateTimePicker.Value = DateTime.Now;
            /*decimal MyInt = 100;
            string MyString = MyInt.ToString("C", CultureInfo.InvariantCulture);
            MessageBox.Show(MyString);*/
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //this.saleDetailsTableAdapter.FillBy(this.posDataSet.SaleDetails, poNumberTextBox.Text);
            long purchaseId = 0;
            long.TryParse(idSpinEdit.Text, out purchaseId);
            this.purchaseDetailTableAdapter.FillById(this.posDataSet.PurchaseDetail, purchaseId);
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            idSpinEdit1.Text = "-1";
            idSpinEdit1.Value = -1;
            purchaseIdTextBox.Text = idSpinEdit.Text;
            pricePerItemTextBox.Text = "0";
            productIdComboBox.Enabled = true;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();

                int selectedIndex = 0;
                selectedIndex = productIdComboBox.SelectedIndex;

                this.purchaseDetailBindingSource.EndEdit();
                //this.tableAdapterManager.UpdateAll(this.posDataSet);
                this.purchaseDetailTableAdapter.Update(this.posDataSet.PurchaseDetail);

                this.productTableAdapter.FillByCustom(this.posDataSet.Product);

                productIdComboBox.SelectedIndex = selectedIndex;

                long purchaseId = 0;
                long.TryParse(idSpinEdit.Text, out purchaseId);
                this.purchaseDetailTableAdapter.FillById(this.posDataSet.PurchaseDetail, purchaseId);


                productIdComboBox.Enabled = false;
                MessageBox.Show("Data saved successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void productIdComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            quantityInUnitSpinEdit.Value = 0;

            System.Windows.Forms.ComboBox selectedProduct = (System.Windows.Forms.ComboBox)sender;
            DataRowView selectedDataRow = (DataRowView)selectedProduct.SelectedItem;
            if (selectedDataRow != null)
            {

                long id = 0;
                long.TryParse(selectedDataRow["Id"].ToString(), out id);

                if (gridView2.RowCount > 1)
                {
                    for (int i = 0; i < gridView2.RowCount; i++)
                    {
                        if (gridView2.GetRowCellValue(i, "ProductId") == null)
                            continue;

                        int productId = 0;
                        int.TryParse(gridView2.GetRowCellValue(i, "ProductId").ToString(), out productId);
                        if (id == productId)
                        {
                            //this.purchaseDetailsTableAdapter.FillBySpreadCode(this.posDataSet.SpreadDetails, spreadNumberTextBox.Text);
                            long purchaseId = 0;
                            long.TryParse(idSpinEdit.Text, out purchaseId);
                            this.purchaseDetailTableAdapter.FillById(this.posDataSet.PurchaseDetail, purchaseId);
                            gridView2.FocusedRowHandle = i;
                        }
                    }
                }
            }
        }
    }
}
