﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace trgpos.Fragments
{
    public partial class Store : UserControl
    {
        private const string patern = "{0}10100{1}";
        public Store()
        {
            InitializeComponent();
        }

        private void Store_Load(object sender, EventArgs e)
        {
            sellerTableAdapter.Fill(posDataSet.Seller);
            if (String.IsNullOrEmpty(TxtSearch.Text))
            {
                storesTableAdapter.Fill(posDataSet.Stores);
            }
            else
            {
                String cleanText = TxtSearch.Text.Trim();
                storesTableAdapter.FillByTerm(posDataSet.Stores, cleanText);
            }
        }

        private void storesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (codeTextBox.Text == "XXXXXXXXXX")
                {
                    var nrCode = "0";
                    var latestCode = storesTableAdapter.GetLatestCode(nameTextEdit.Text.Remove(1).ToUpper());
                    if (!String.IsNullOrEmpty(latestCode))
                    {
                        nrCode = latestCode.Remove(0, 6);
                    }
                    var firstLetter = nameTextEdit.Text.Remove(1).ToUpper();
                    var nrCount = int.Parse(nrCode);

                    var newCode = String.Format(patern, firstLetter, (nrCount + 1).ToString("0000"));

                    codeTextBox.Text = newCode;
                }

                if (String.IsNullOrEmpty(telephoneTextEdit.Text))
                {
                    telephoneTextEdit.Text = " ";
                }

                Validate();
                storesBindingSource.EndEdit();
                storesTableAdapter.Update(posDataSet.Stores);
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            codeTextBox.Text = "XXXXXXXXXX";
            ActiveControl = nameTextEdit;
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            TextBox txtbox = (TextBox)sender;
            String cleanText = txtbox.Text.Trim();
            storesTableAdapter.FillByTerm(posDataSet.Stores, cleanText);
        }

        private void BtnClean_Click(object sender, EventArgs e)
        {
            TxtSearch.Text = String.Empty;
            storesTableAdapter.Fill(posDataSet.Stores);
        }
    }
}
