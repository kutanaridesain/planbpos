﻿namespace trgpos.Fragments
{
    partial class Spreading
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label qtyOutLabel;
            System.Windows.Forms.Label qtySoldLabel;
            System.Windows.Forms.Label sellerCodeLabel;
            System.Windows.Forms.Label productIdLabel;
            System.Windows.Forms.Label spreadNumberLabel;
            System.Windows.Forms.Label spreadDateLabel;
            System.Windows.Forms.Label spreadNumberLabel2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Spreading));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.spreadBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.spreadBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.posDataSet = new trgpos.posDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.spreadBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.spreadDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.spreadNumberTextBox = new System.Windows.Forms.TextBox();
            this.sellerCodeComboBox = new System.Windows.Forms.ComboBox();
            this.sellerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spreadGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpreadNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpreadDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellerCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.spreadDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.TxtSpreadCode = new System.Windows.Forms.TextBox();
            this.CbProductName = new System.Windows.Forms.ComboBox();
            this.productBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.qtySoldSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.qtyOutSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.spreadDetailsGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpreadNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQtyOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQtySold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spreadTableAdapter = new trgpos.posDataSetTableAdapters.SpreadTableAdapter();
            this.tableAdapterManager = new trgpos.posDataSetTableAdapters.TableAdapterManager();
            this.spreadDetailsTableAdapter = new trgpos.posDataSetTableAdapters.SpreadDetailsTableAdapter();
            this.productTableAdapter = new trgpos.posDataSetTableAdapters.ProductTableAdapter();
            this.sellerTableAdapter = new trgpos.posDataSetTableAdapters.SellerTableAdapter();
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            qtyOutLabel = new System.Windows.Forms.Label();
            qtySoldLabel = new System.Windows.Forms.Label();
            sellerCodeLabel = new System.Windows.Forms.Label();
            productIdLabel = new System.Windows.Forms.Label();
            spreadNumberLabel = new System.Windows.Forms.Label();
            spreadDateLabel = new System.Windows.Forms.Label();
            spreadNumberLabel2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadBindingNavigator)).BeginInit();
            this.spreadBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDateDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtySoldSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtyOutSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDetailsGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).BeginInit();
            this.SuspendLayout();
            // 
            // qtyOutLabel
            // 
            qtyOutLabel.AutoSize = true;
            qtyOutLabel.Location = new System.Drawing.Point(14, 95);
            qtyOutLabel.Name = "qtyOutLabel";
            qtyOutLabel.Size = new System.Drawing.Size(69, 13);
            qtyOutLabel.TabIndex = 4;
            qtyOutLabel.Text = "Quantity Out:";
            // 
            // qtySoldLabel
            // 
            qtySoldLabel.AutoSize = true;
            qtySoldLabel.Location = new System.Drawing.Point(14, 121);
            qtySoldLabel.Name = "qtySoldLabel";
            qtySoldLabel.Size = new System.Drawing.Size(73, 13);
            qtySoldLabel.TabIndex = 6;
            qtySoldLabel.Text = "Quantity Sold:";
            // 
            // sellerCodeLabel
            // 
            sellerCodeLabel.AutoSize = true;
            sellerCodeLabel.Location = new System.Drawing.Point(15, 93);
            sellerCodeLabel.Name = "sellerCodeLabel";
            sellerCodeLabel.Size = new System.Drawing.Size(36, 13);
            sellerCodeLabel.TabIndex = 4;
            sellerCodeLabel.Text = "Seller:";
            // 
            // productIdLabel
            // 
            productIdLabel.AutoSize = true;
            productIdLabel.Location = new System.Drawing.Point(14, 68);
            productIdLabel.Name = "productIdLabel";
            productIdLabel.Size = new System.Drawing.Size(50, 13);
            productIdLabel.TabIndex = 7;
            productIdLabel.Text = "Product :";
            // 
            // spreadNumberLabel
            // 
            spreadNumberLabel.AutoSize = true;
            spreadNumberLabel.Location = new System.Drawing.Point(15, 41);
            spreadNumberLabel.Name = "spreadNumberLabel";
            spreadNumberLabel.Size = new System.Drawing.Size(35, 13);
            spreadNumberLabel.TabIndex = 7;
            spreadNumberLabel.Text = "Code:";
            // 
            // spreadDateLabel
            // 
            spreadDateLabel.AutoSize = true;
            spreadDateLabel.Location = new System.Drawing.Point(15, 67);
            spreadDateLabel.Name = "spreadDateLabel";
            spreadDateLabel.Size = new System.Drawing.Size(33, 13);
            spreadDateLabel.TabIndex = 8;
            spreadDateLabel.Text = "Date:";
            // 
            // spreadNumberLabel2
            // 
            spreadNumberLabel2.AutoSize = true;
            spreadNumberLabel2.Location = new System.Drawing.Point(14, 41);
            spreadNumberLabel2.Name = "spreadNumberLabel2";
            spreadNumberLabel2.Size = new System.Drawing.Size(72, 13);
            spreadNumberLabel2.TabIndex = 9;
            spreadNumberLabel2.Text = "Spread Code:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(110)))), ((int)(((byte)(210)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1026, 30);
            this.panel1.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Transaction / Spreading";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 30);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.spreadBindingNavigator);
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.bindingNavigator1);
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1026, 512);
            this.splitContainerControl1.SplitterPosition = 432;
            this.splitContainerControl1.TabIndex = 16;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // spreadBindingNavigator
            // 
            this.spreadBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.spreadBindingNavigator.BindingSource = this.spreadBindingSource;
            this.spreadBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.spreadBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.spreadBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.spreadBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.spreadBindingNavigatorSaveItem});
            this.spreadBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.spreadBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.spreadBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.spreadBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.spreadBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.spreadBindingNavigator.Name = "spreadBindingNavigator";
            this.spreadBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.spreadBindingNavigator.Size = new System.Drawing.Size(432, 25);
            this.spreadBindingNavigator.TabIndex = 17;
            this.spreadBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // spreadBindingSource
            // 
            this.spreadBindingSource.DataMember = "Spread";
            this.spreadBindingSource.DataSource = this.posDataSet;
            // 
            // posDataSet
            // 
            this.posDataSet.DataSetName = "posDataSet";
            this.posDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 22);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // spreadBindingNavigatorSaveItem
            // 
            this.spreadBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.spreadBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("spreadBindingNavigatorSaveItem.Image")));
            this.spreadBindingNavigatorSaveItem.Name = "spreadBindingNavigatorSaveItem";
            this.spreadBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.spreadBindingNavigatorSaveItem.Text = "Save Data";
            this.spreadBindingNavigatorSaveItem.Click += new System.EventHandler(this.spreadBindingNavigatorSaveItem_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(spreadDateLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.spreadDateDateEdit);
            this.splitContainerControl2.Panel1.Controls.Add(spreadNumberLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.spreadNumberTextBox);
            this.splitContainerControl2.Panel1.Controls.Add(sellerCodeLabel);
            this.splitContainerControl2.Panel1.Controls.Add(this.sellerCodeComboBox);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.spreadGridControl);
            this.splitContainerControl2.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(432, 512);
            this.splitContainerControl2.SplitterPosition = 154;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // spreadDateDateEdit
            // 
            this.spreadDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spreadBindingSource, "SpreadDate", true));
            this.spreadDateDateEdit.EditValue = null;
            this.spreadDateDateEdit.Location = new System.Drawing.Point(72, 64);
            this.spreadDateDateEdit.Name = "spreadDateDateEdit";
            this.spreadDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spreadDateDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spreadDateDateEdit.Size = new System.Drawing.Size(100, 20);
            this.spreadDateDateEdit.TabIndex = 9;
            // 
            // spreadNumberTextBox
            // 
            this.spreadNumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spreadBindingSource, "SpreadNumber", true));
            this.spreadNumberTextBox.Location = new System.Drawing.Point(72, 38);
            this.spreadNumberTextBox.Name = "spreadNumberTextBox";
            this.spreadNumberTextBox.ReadOnly = true;
            this.spreadNumberTextBox.Size = new System.Drawing.Size(150, 20);
            this.spreadNumberTextBox.TabIndex = 8;
            // 
            // sellerCodeComboBox
            // 
            this.sellerCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.spreadBindingSource, "SellerCode", true));
            this.sellerCodeComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spreadBindingSource, "SellerName", true));
            this.sellerCodeComboBox.DataSource = this.sellerBindingSource;
            this.sellerCodeComboBox.DisplayMember = "Name";
            this.sellerCodeComboBox.FormattingEnabled = true;
            this.sellerCodeComboBox.Location = new System.Drawing.Point(72, 90);
            this.sellerCodeComboBox.Name = "sellerCodeComboBox";
            this.sellerCodeComboBox.Size = new System.Drawing.Size(121, 21);
            this.sellerCodeComboBox.TabIndex = 5;
            this.sellerCodeComboBox.ValueMember = "Code";
            // 
            // sellerBindingSource
            // 
            this.sellerBindingSource.DataMember = "Seller";
            this.sellerBindingSource.DataSource = this.posDataSet;
            // 
            // spreadGridControl
            // 
            this.spreadGridControl.DataSource = this.spreadBindingSource;
            this.spreadGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spreadGridControl.Location = new System.Drawing.Point(5, 5);
            this.spreadGridControl.MainView = this.gridView1;
            this.spreadGridControl.Name = "spreadGridControl";
            this.spreadGridControl.Size = new System.Drawing.Size(422, 343);
            this.spreadGridControl.TabIndex = 0;
            this.spreadGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpreadNumber,
            this.colSpreadDate,
            this.colSellerCode});
            this.gridView1.GridControl = this.spreadGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            // 
            // colSpreadNumber
            // 
            this.colSpreadNumber.FieldName = "SpreadNumber";
            this.colSpreadNumber.Name = "colSpreadNumber";
            this.colSpreadNumber.OptionsColumn.AllowEdit = false;
            this.colSpreadNumber.OptionsColumn.AllowFocus = false;
            this.colSpreadNumber.OptionsColumn.ReadOnly = true;
            this.colSpreadNumber.Visible = true;
            this.colSpreadNumber.VisibleIndex = 0;
            // 
            // colSpreadDate
            // 
            this.colSpreadDate.FieldName = "SpreadDate";
            this.colSpreadDate.Name = "colSpreadDate";
            this.colSpreadDate.OptionsColumn.AllowEdit = false;
            this.colSpreadDate.OptionsColumn.AllowFocus = false;
            this.colSpreadDate.OptionsColumn.ReadOnly = true;
            this.colSpreadDate.Visible = true;
            this.colSpreadDate.VisibleIndex = 1;
            // 
            // colSellerCode
            // 
            this.colSellerCode.FieldName = "SellerCode";
            this.colSellerCode.Name = "colSellerCode";
            this.colSellerCode.OptionsColumn.AllowEdit = false;
            this.colSellerCode.OptionsColumn.AllowFocus = false;
            this.colSellerCode.OptionsColumn.ReadOnly = true;
            this.colSellerCode.Visible = true;
            this.colSellerCode.VisibleIndex = 2;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.toolStripButton1;
            this.bindingNavigator1.BindingSource = this.spreadDetailsBindingSource;
            this.bindingNavigator1.CountItem = this.toolStripLabel1;
            this.bindingNavigator1.DeleteItem = this.toolStripButton2;
            this.bindingNavigator1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton7,
            this.toolStripButton8});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
            this.bindingNavigator1.MoveFirstItem = this.toolStripButton3;
            this.bindingNavigator1.MoveLastItem = this.toolStripButton6;
            this.bindingNavigator1.MoveNextItem = this.toolStripButton5;
            this.bindingNavigator1.MovePreviousItem = this.toolStripButton4;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.toolStripTextBox1;
            this.bindingNavigator1.Size = new System.Drawing.Size(589, 25);
            this.bindingNavigator1.TabIndex = 18;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.RightToLeftAutoMirrorImage = true;
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Add new";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // spreadDetailsBindingSource
            // 
            this.spreadDetailsBindingSource.DataMember = "SpreadDetails";
            this.spreadDetailsBindingSource.DataSource = this.posDataSet;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "of {0}";
            this.toolStripLabel1.ToolTipText = "Total number of items";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.RightToLeftAutoMirrorImage = true;
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Delete";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.RightToLeftAutoMirrorImage = true;
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Move first";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.RightToLeftAutoMirrorImage = true;
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "Move previous";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.AccessibleName = "Position";
            this.toolStripTextBox1.AutoSize = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(50, 22);
            this.toolStripTextBox1.Text = "0";
            this.toolStripTextBox1.ToolTipText = "Current position";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.RightToLeftAutoMirrorImage = true;
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "Move next";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.RightToLeftAutoMirrorImage = true;
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "Move last";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "Save Data";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "toolStripButton8";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(spreadNumberLabel2);
            this.splitContainerControl3.Panel1.Controls.Add(this.TxtSpreadCode);
            this.splitContainerControl3.Panel1.Controls.Add(this.CbProductName);
            this.splitContainerControl3.Panel1.Controls.Add(productIdLabel);
            this.splitContainerControl3.Panel1.Controls.Add(qtySoldLabel);
            this.splitContainerControl3.Panel1.Controls.Add(this.qtySoldSpinEdit);
            this.splitContainerControl3.Panel1.Controls.Add(qtyOutLabel);
            this.splitContainerControl3.Panel1.Controls.Add(this.qtyOutSpinEdit);
            this.splitContainerControl3.Panel1.Controls.Add(this.idTextBox);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.spreadDetailsGridControl);
            this.splitContainerControl3.Panel2.Padding = new System.Windows.Forms.Padding(5);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(589, 512);
            this.splitContainerControl3.SplitterPosition = 155;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spreadDetailsBindingSource, "Id", true));
            this.idTextBox.Location = new System.Drawing.Point(109, 92);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(100, 20);
            this.idTextBox.TabIndex = 11;
            // 
            // TxtSpreadCode
            // 
            this.TxtSpreadCode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spreadDetailsBindingSource, "SpreadNumber", true));
            this.TxtSpreadCode.Enabled = false;
            this.TxtSpreadCode.Location = new System.Drawing.Point(110, 38);
            this.TxtSpreadCode.Name = "TxtSpreadCode";
            this.TxtSpreadCode.Size = new System.Drawing.Size(100, 20);
            this.TxtSpreadCode.TabIndex = 10;
            // 
            // CbProductName
            // 
            this.CbProductName.CausesValidation = false;
            this.CbProductName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.productBindingSource, "CodeName", true));
            this.CbProductName.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.spreadDetailsBindingSource, "ProductId", true));
            this.CbProductName.DataSource = this.productBindingSource;
            this.CbProductName.DisplayMember = "CodeName";
            this.CbProductName.Enabled = false;
            this.CbProductName.FormattingEnabled = true;
            this.CbProductName.Location = new System.Drawing.Point(110, 65);
            this.CbProductName.Name = "CbProductName";
            this.CbProductName.Size = new System.Drawing.Size(420, 21);
            this.CbProductName.TabIndex = 9;
            this.CbProductName.ValueMember = "Id";
            this.CbProductName.SelectionChangeCommitted += new System.EventHandler(this.CbProductName_SelectionChangeCommitted);
            // 
            // productBindingSource
            // 
            this.productBindingSource.DataMember = "Product";
            this.productBindingSource.DataSource = this.posDataSet;
            // 
            // qtySoldSpinEdit
            // 
            this.qtySoldSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spreadDetailsBindingSource, "QtySold", true));
            this.qtySoldSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.qtySoldSpinEdit.Location = new System.Drawing.Point(110, 118);
            this.qtySoldSpinEdit.Name = "qtySoldSpinEdit";
            this.qtySoldSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.qtySoldSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.qtySoldSpinEdit.TabIndex = 7;
            // 
            // qtyOutSpinEdit
            // 
            this.qtyOutSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spreadDetailsBindingSource, "QtyOut", true));
            this.qtyOutSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.qtyOutSpinEdit.Location = new System.Drawing.Point(110, 92);
            this.qtyOutSpinEdit.Name = "qtyOutSpinEdit";
            this.qtyOutSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.qtyOutSpinEdit.Size = new System.Drawing.Size(100, 20);
            this.qtyOutSpinEdit.TabIndex = 5;
            // 
            // spreadDetailsGridControl
            // 
            this.spreadDetailsGridControl.CausesValidation = false;
            this.spreadDetailsGridControl.DataSource = this.spreadDetailsBindingSource;
            this.spreadDetailsGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spreadDetailsGridControl.Location = new System.Drawing.Point(5, 5);
            this.spreadDetailsGridControl.MainView = this.gridView2;
            this.spreadDetailsGridControl.Name = "spreadDetailsGridControl";
            this.spreadDetailsGridControl.Size = new System.Drawing.Size(579, 342);
            this.spreadDetailsGridControl.TabIndex = 0;
            this.spreadDetailsGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colSpreadNumber1,
            this.colProductId,
            this.colProductName,
            this.colQtyOut,
            this.colQtySold});
            this.gridView2.GridControl = this.spreadDetailsGridControl;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpreadNumber1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colId
            // 
            this.colId.Caption = "ID";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colSpreadNumber1
            // 
            this.colSpreadNumber1.FieldName = "SpreadNumber";
            this.colSpreadNumber1.Name = "colSpreadNumber1";
            this.colSpreadNumber1.OptionsColumn.AllowEdit = false;
            this.colSpreadNumber1.OptionsColumn.AllowFocus = false;
            this.colSpreadNumber1.OptionsColumn.ReadOnly = true;
            // 
            // colProductId
            // 
            this.colProductId.FieldName = "ProductId";
            this.colProductId.Name = "colProductId";
            this.colProductId.OptionsColumn.AllowEdit = false;
            this.colProductId.OptionsColumn.AllowFocus = false;
            this.colProductId.OptionsColumn.ReadOnly = true;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Product";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.OptionsColumn.AllowEdit = false;
            this.colProductName.OptionsColumn.AllowFocus = false;
            this.colProductName.OptionsColumn.ReadOnly = true;
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 0;
            this.colProductName.Width = 696;
            // 
            // colQtyOut
            // 
            this.colQtyOut.FieldName = "QtyOut";
            this.colQtyOut.Name = "colQtyOut";
            this.colQtyOut.OptionsColumn.AllowEdit = false;
            this.colQtyOut.OptionsColumn.AllowFocus = false;
            this.colQtyOut.OptionsColumn.ReadOnly = true;
            this.colQtyOut.Visible = true;
            this.colQtyOut.VisibleIndex = 1;
            this.colQtyOut.Width = 238;
            // 
            // colQtySold
            // 
            this.colQtySold.FieldName = "QtySold";
            this.colQtySold.Name = "colQtySold";
            this.colQtySold.OptionsColumn.AllowEdit = false;
            this.colQtySold.OptionsColumn.AllowFocus = false;
            this.colQtySold.OptionsColumn.ReadOnly = true;
            this.colQtySold.Visible = true;
            this.colQtySold.VisibleIndex = 2;
            this.colQtySold.Width = 246;
            // 
            // spreadTableAdapter
            // 
            this.spreadTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BrandTableAdapter = null;
            this.tableAdapterManager.ProductDiscountsTableAdapter = null;
            this.tableAdapterManager.ProductTableAdapter = null;
            this.tableAdapterManager.PurchaseDetailTableAdapter = null;
            this.tableAdapterManager.PurchaseTableAdapter = null;
            this.tableAdapterManager.SaleDetailsTableAdapter = null;
            this.tableAdapterManager.SaleTableAdapter = null;
            this.tableAdapterManager.SellerTableAdapter = null;
            this.tableAdapterManager.SpreadDetailsTableAdapter = null;
            this.tableAdapterManager.SpreadTableAdapter = this.spreadTableAdapter;
            this.tableAdapterManager.StoresTableAdapter = null;
            this.tableAdapterManager.SupplierTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = trgpos.posDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserTableAdapter = null;
            // 
            // spreadDetailsTableAdapter
            // 
            this.spreadDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // productTableAdapter
            // 
            this.productTableAdapter.ClearBeforeFill = true;
            // 
            // sellerTableAdapter
            // 
            this.sellerTableAdapter.ClearBeforeFill = true;
            // 
            // Spreading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.panel1);
            this.Name = "Spreading";
            this.Size = new System.Drawing.Size(1026, 542);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spreadBindingNavigator)).EndInit();
            this.spreadBindingNavigator.ResumeLayout(false);
            this.spreadBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.posDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spreadDateDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sellerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.productBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtySoldSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qtyOutSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spreadDetailsGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private posDataSet posDataSet;
        private System.Windows.Forms.BindingSource spreadBindingSource;
        private posDataSetTableAdapters.SpreadTableAdapter spreadTableAdapter;
        private posDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator spreadBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton spreadBindingNavigatorSaveItem;
        private DevExpress.XtraGrid.GridControl spreadGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpreadNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSpreadDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellerCode;
        private DevExpress.XtraGrid.GridControl spreadDetailsGridControl;
        private System.Windows.Forms.BindingSource spreadDetailsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colSpreadNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colProductId;
        private DevExpress.XtraGrid.Columns.GridColumn colQtyOut;
        private DevExpress.XtraGrid.Columns.GridColumn colQtySold;
        private posDataSetTableAdapters.SpreadDetailsTableAdapter spreadDetailsTableAdapter;
        private DevExpress.XtraEditors.SpinEdit qtySoldSpinEdit;
        private DevExpress.XtraEditors.SpinEdit qtyOutSpinEdit;
        private System.Windows.Forms.BindingSource productBindingSource;
        private posDataSetTableAdapters.ProductTableAdapter productTableAdapter;
        private System.Windows.Forms.ComboBox sellerCodeComboBox;
        private System.Windows.Forms.BindingSource sellerBindingSource;
        private posDataSetTableAdapters.SellerTableAdapter sellerTableAdapter;
        private System.Windows.Forms.ComboBox CbProductName;
        private System.Windows.Forms.TextBox spreadNumberTextBox;
        private DevExpress.XtraEditors.DateEdit spreadDateDateEdit;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.TextBox TxtSpreadCode;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private System.Windows.Forms.TextBox idTextBox;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;


    }
}
