﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Helpers;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using trgpos.Fragments;


namespace trgpos
{
    public partial class PlanBCustomPosForm : RibbonForm
    {
        public PlanBCustomPosForm()
        {
            InitializeComponent();
            InitSkinGallery();
            InitGrid();

            this.Text = "PlanB Custom POS - " + Properties.Settings.Default.NamaPerusahaan; 
        }
        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }
       
        void InitGrid()
        {          
        }

        private void iExit_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Products());
        }

        private void PlaceControl(Control ctrl)
        {
            pnlMain.Controls.Clear();
            ctrl.Dock = DockStyle.Fill;
            pnlMain.Controls.Add(ctrl);
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Sales());
        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Brands());
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Store());
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Discounts());
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Seller());
        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Suplier());
        }

        private void barButtonItem25_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Spreading());
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Purchase());
        }

        private void barButtonItem28_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Home());
        }

        private void barButtonItem29_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new SalesReport());
        }

        private void barButtonItem30_ItemClick(object sender, ItemClickEventArgs e)
        {
            PlaceControl(new Home());
        }

    }
}