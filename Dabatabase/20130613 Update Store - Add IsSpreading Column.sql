/*
   Thursday, June 13, 20131:03:53 PM
   User: sa
   Server: HARRYNOV-ZONE\MSSQLSERVER2008
   Database: PlanBCustomPos
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Stores ADD
	IsSpreading bit NULL
GO
ALTER TABLE dbo.Stores ADD CONSTRAINT
	DF_Stores_IsSpreading DEFAULT 0 FOR IsSpreading
GO
ALTER TABLE dbo.Stores SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
